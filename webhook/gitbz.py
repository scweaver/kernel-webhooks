"""Support for gitbz queries."""
from dataclasses import dataclass
from enum import Enum
from enum import auto
import json
import re

from cki_lib import logger
from cki_lib.session import get_session

LOGGER = logger.get_logger('cki.webhook.gitbz')

GITBZ_URL = 'https://dist-git.host.prod.eng.bos.redhat.com/lookaside/gitbz-query.cgi'
GITBZ_RETRIES = 2


class Status(Enum):
    """Possible Response states."""

    APPROVED = auto()
    UNAPPROVED = auto()
    INVALID = auto()
    MISSING = auto()       # BZ details not found in gitbz response.
    TODO = auto()          # BZ has not been quiered yet.
    TEMP_FAILURE = auto()  # BZ showed a temporary failure in the gitbz response.


@dataclass(frozen=True)
class GBZResponse:
    """Gitbz response for a bug."""

    id: int  # pylint: disable=invalid-name
    status: Status
    details: str
    error: int = 0

    def __post_init__(self):
        """Process the input parameters and then log our creation."""
        self._check_status()
        self._process_details()
        LOGGER.debug('Created %s', self)

    def _check_status(self):
        """Ensure the status field is a Status object."""
        if isinstance(self.status, Status):
            return
        raise TypeError(f'self.status must be of class Status: {self}')

    def _process_details(self):
        """For Invalids try to grab error code and simplify details string."""
        if self.status is not Status.INVALID:
            return
        # like 'error: XMLRPC Fault #101 from Bugzilla: Bug #20210325 does not exist.'
        if xmlrpc_err := xmlrpc_fault(self.details):
            self.__dict__['error'] = int(xmlrpc_err[0])
            self.__dict__['details'] = xmlrpc_err[1]
        # 'error: <ProtocolError for bugzilla.redhat.com/xmlrpc.cgi: 504 Gateway Time-out>'
        elif '504 Gateway Time-out' in self.details:
            self.__dict__['error'] = 504
            self.__dict__['status'] = Status.TEMP_FAILURE
        # 'error: [Errno -3] Temporary failure in name resolution)'
        elif '[Errno -3]' in self.details:
            self.__dict__['error'] = -3
            self.__dict__['status'] = Status.TEMP_FAILURE
        # some new error??
        else:
            self.__dict__['error'] = -1
        self.__dict__['details'] = self.details.removeprefix('error: ')
        self.__dict__['details'] = self.details.strip('<>')

    def update_response(self, status, details):
        """Update with a new status/details."""
        self.__dict__['status'] = status
        self.__dict__['details'] = details
        self._check_status()
        # reset the error field.
        if status is not Status.INVALID:
            self.__dict__['error'] = 0
        self._process_details()
        LOGGER.debug('Updated %s', self)


def build_query_json(branch, bug_list):
    """Build the query json."""
    return {'package': branch.component,
            'namespace': 'rpms',
            'ref': f'refs/heads/{branch.distgit_ref}',
            'commits': [{'hexsha': 'HEAD',
                         'files': ['kernel.spec'],
                         'resolved': bug_list,
                         'related': [],
                         'reverted': [],
                         }]
            }


def get_gitbz_session():
    """Return a session for gitbz."""
    retry_args = {'status_forcelist': [500, 502, 503, 504]}
    gitbz_session = get_session('cki.webhook.gitbz', retry_args=retry_args)
    gitbz_session.allowed_methods = ['POST']
    return gitbz_session


def xmlrpc_fault(response_line):
    """Return a tuple with XMLRPC fault code/message, if any, or an empty tuple."""
    match = re.match(r'error: XMLRPC Fault #(?P<code>\d*).*: (?P<msg>.*)', response_line)
    if not match:
        return ()
    return (int(match.group('code')), match.group('msg'))


class GBZQuery:
    """An object for running queries against gitbz-query.cgi."""

    def __init__(self, branch, bug_list):
        """Set up the object."""
        self.bug_list = [GBZResponse(int(bz_id), Status.TODO, '') for bz_id in bug_list]
        self.session = get_gitbz_session()
        self.branch = branch

    def run(self):
        """Query the TODO and TEMP_FAILURE bugs from bug_list and update their state."""
        self._retries = GITBZ_RETRIES  # pylint: disable=attribute-defined-outside-init
        while (bugs_to_query := self._bugs_to_query()):
            self._mark_bugs_missing(bugs_to_query)
            LOGGER.info('Running gitbz query (retries: %d) for bugs %s on branch %s', self._retries,
                        [bug.id for bug in bugs_to_query], self.branch.name)
            self._run(bugs_to_query)
            self._retries -= 1

    def _run(self, bugs):
        """Build json and do a query."""
        query_json = build_query_json(self.branch, [bug.id for bug in bugs])
        self._do_query(query_json)

    def _do_query(self, input_json):
        """Query gitbz and process the response json."""
        LOGGER.debug('gitbz input:\n%s', json.dumps(input_json, indent=2))
        response = self.session.post(GITBZ_URL, json=input_json)
        response.raise_for_status()  # raise an error if we didn't get a nice response
        response_json = response.json()
        LOGGER.debug('gitbz output:\n%s', json.dumps(response_json, indent=2))
        self._update_bugs(response_json['logs'])

    def _update_bugs(self, json_logs):
        """Parse the response JSON and update GBZResponse objects."""
        # try to clean up the output into something more clearly parsable 🙄
        response_lines = json_logs.split('\n')
        response_lines = [line.lstrip('***').strip() for line in response_lines]

        # The response includes three possible headings: Approved, Unapproved, Invalid. Headings are
        # only present when there is a BZ in that category. So if the last heading we saw is
        # Approved then any following BZ line is Approved until we hit another heading.
        status = None
        for line in response_lines:
            if line.startswith('Approved:'):
                status = Status.APPROVED
                continue
            if line.startswith('Unapproved:'):
                status = Status.UNAPPROVED
                continue
            if line.startswith('Invalid:'):
                status = Status.INVALID
                continue
            self._parse_response_line(line, status)

    def _parse_response_line(self, line, status):
        """Update a GBZResponse object if a line refers to a BZ."""
        if match := re.match(r'^rhbz#(?P<bz_id>\d+) \((?P<details>.*)\)$', line):
            self.update_bug(match.group('bz_id'), status, match.group('details'))

    def _bugs_to_query(self):
        """Generate a list of bugs to query, only if we have retries left."""
        if self._retries < 0:
            return []
        return [bug for bug in self.bug_list if bug.status in (Status.TODO, Status.TEMP_FAILURE)]

    def _mark_bugs_missing(self, bug_list):
        """Mark the given bugs as Missing results."""
        for bug in bug_list:
            self.update_bug(bug.id, Status.MISSING, 'No results found for this BZ.')

    def update_bug(self, bz_id, status, details):
        """Update a bug object. It better exist."""
        bug = next((bug for bug in self.bug_list if bug.id == int(bz_id)), None)
        if not bug:
            raise RuntimeError(f'{bz_id} not found in bug_list: {self.bug_list}')
        bug.update_response(status, details)


def query(branch, bug_list):
    """Run a query and return a list of GBZResponse objects."""
    querier = GBZQuery(branch, bug_list)
    querier.run()
    return querier.bug_list
