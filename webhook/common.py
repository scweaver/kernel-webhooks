# pylint: disable=too-many-lines

"""Common code that can be used by all webhooks."""
import argparse
import json
import os
import pathlib
import re
from subprocess import run
import sys
from urllib import parse

import bugzilla
from cki_lib import logger
from cki_lib import metrics
from cki_lib import misc
from cki_lib.gitlab import get_instance
from cki_lib.gitlab import get_token
from cki_lib.messagequeue import Message
from cki_lib.messagequeue import MessageQueue
from cki_lib.session import get_session
from gitlab.const import MAINTAINER_ACCESS
from gitlab.exceptions import GitlabCreateError
from gitlab.exceptions import GitlabGetError
from gql import Client
from gql.transport.requests import RequestsHTTPTransport
import sentry_sdk
from yaml import safe_load

from webhook.rh_metadata import Projects
from webhook.rh_metadata import is_branch_active

from . import defs
from . import owners

LOGGER = logger.get_logger(__name__)


def get_arg_parser(webhook_prefix):
    """Intialize a commandline parser.

    Returns: argparse parser.
    """
    parser = argparse.ArgumentParser(
        description='Manual handling of merge requests')
    parser.add_argument('--merge-request',
                        help='Process given merge request URL only')
    parser.add_argument('--action', default='',
                        help='Action for the MR when using URL only')
    parser.add_argument('--json-message-file', default='',
                        help='Process a single JSON message in a file')
    parser.add_argument('--oldrev', action='store_true',
                        help='Treat this as changed MR when using URL only')
    parser.add_argument('--note',
                        help='Process a note for the given merge request')
    parser.add_argument('--disable-user-check', action='store_true',
                        help="Don't check if the API user matches the event creator.")
    parser.add_argument('--disable-inactive-branch-check', action='store_true',
                        help="Don't check if the event target branch is inactive.")
    parser.add_argument('--sentry-ca-certs', default=os.getenv('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    parser.add_argument('--rabbitmq-host', default=os.environ.get('RABBITMQ_HOST', 'localhost'))
    parser.add_argument('--rabbitmq-port', type=int,
                        default=misc.get_env_int('RABBITMQ_PORT', 5672))
    parser.add_argument('--rabbitmq-user', default=os.environ.get('RABBITMQ_USER', 'guest'))
    parser.add_argument('--rabbitmq-password',
                        default=os.environ.get('RABBITMQ_PASSWORD', 'guest'))
    parser.add_argument('--rabbitmq-exchange',
                        default=os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.webhooks'))
    parser.add_argument('--rabbitmq-routing-key',
                        default=os.environ.get(f'{webhook_prefix}_ROUTING_KEYS'),
                        help='RabbitMQ routing key. Required when processing queue.')
    parser.add_argument('--rabbitmq-queue-name',
                        default=os.environ.get(f'{webhook_prefix}_QUEUE'),
                        help='RabbitMQ queue name. Required when processing queue.')

    return parser


def parse_mr_url(url):
    """Parse the merge request URL used for manual handlers.

    Args:
        url: Full merge request URL.

    Returns:
        A tuple of (gitlab_instance, mr_object, project_path_with_namespace).
    """
    url_parts = parse.urlsplit(url)
    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    gl_instance = get_instance(instance_url)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    project_path = re.sub('/-$', '', match[1])
    gl_project = gl_instance.projects.get(project_path)
    gl_mergerequest = gl_project.mergerequests.get(int(match[2]))

    return gl_instance, gl_project, gl_mergerequest, project_path


def parse_gl_project_path(url):
    """Return the project path with namespace from the given Gitlab url."""
    url_split = url.split('/')[3:]
    namespace = []
    for part in url_split:
        if part == '-':
            break
        namespace.append(part)
    return '/'.join(namespace)


def mr_is_closed(merge_request):
    """We really don't want to run on closed MRs, add common check function."""
    return merge_request.state == "closed"


def get_mr_state_from_event(payload):
    """Return the state of the MR indicated by the event, or None."""
    return (misc.get_nested_key(payload, 'object_attributes/state') or
            misc.get_nested_key(payload, 'merge_request/state'))


def _process_gitlab_message(payload, webhooks, args, **kwargs):
    # pylint: disable=too-many-return-statements
    """Handle a gitlab message from the queue."""
    LOGGER.info('Processing message from queue: %s', json.dumps(payload, indent=None))

    object_kind = payload.get('object_kind')
    if not object_kind or object_kind not in webhooks:
        LOGGER.info('Ignoring message with missing or unhandled object_kind.')
        return False  # unit tests

    # Always ignore note events that are not related to a merge request.
    if object_kind == 'note' and \
       payload['object_attributes'].get('noteable_type') != 'MergeRequest':
        LOGGER.info('Ignoring note event for non-MR.')
        return False

    # Ignore messages related to MRs whose target branch is inactive.
    if not args.disable_inactive_branch_check and not is_event_target_branch_active(payload):
        LOGGER.info('Ignoring message with target branch that is not active.')
        return False

    # Ignore events for MRs that are in closed or merged state.
    if (state := get_mr_state_from_event(payload)) in ('closed', 'merged'):
        # Do not ignore the MR event when it is in the process of being closed.
        if payload.get('object_attributes', {}).get('action') != 'close':
            LOGGER.info("Ignoring event with '%s' state.", state)
            return False

    message = Message(payload)

    # Pass in a gl_instance?
    if kwargs.get('get_gl_instance', True):
        with message.gl_instance() as gl_instance:
            if not hasattr(gl_instance, 'user'):
                gl_instance.auth()

            if not args.disable_user_check:
                if gl_instance.user.username == message.payload['user']['username']:
                    LOGGER.info('Ignoring bot message from queue.')
                    return False

            webhooks[object_kind](gl_instance, message, **kwargs)
            return True

    webhooks[object_kind](message, **kwargs)
    return True  # unit tests


def _process_amqp_bridge_message(body, handlers, **kwargs):
    """Process a message from UMB passed through via the AMQP bridge."""
    if 'amqp-bridge' not in handlers:
        LOGGER.info('No amqp-bridge handler, ignoring: %s', json.dumps(body, indent=None))
        return False
    return handlers['amqp-bridge'](body, **kwargs)


def _process_umb_bridge_message(headers, handlers, **kwargs):
    """Process a message from the umb_bridge hook."""
    if defs.UMB_BRIDGE_MESSAGE_TYPE not in handlers:
        LOGGER.info('No %s handler, ignoring: %s', defs.UMB_BRIDGE_MESSAGE_TYPE,
                    json.dumps(headers, indent=None))
        return False
    return handlers[defs.UMB_BRIDGE_MESSAGE_TYPE](headers, **kwargs)


def process_message(args, webhooks, routing_key=None, body=None, headers=None, **kwargs):
    """Process a amqp message."""
    message_type = headers['message-type']
    LOGGER.debug('Received message on %s of type %s.', routing_key, message_type)

    if message_type == 'gitlab':
        return _process_gitlab_message(body, webhooks, args, **kwargs)

    if message_type == 'amqp-bridge':
        return _process_amqp_bridge_message(body, webhooks, **kwargs)

    if message_type == defs.UMB_BRIDGE_MESSAGE_TYPE:
        return _process_umb_bridge_message(headers, webhooks, **kwargs)

    # How did we get here?
    LOGGER.warning('Ignoring unknown message type %s: %s', message_type,
                   json.dumps(body, indent=2))
    return True


def get_messagequeue(args, **kwargs):
    """Return a new message queue instance."""
    return MessageQueue(host=args.rabbitmq_host, port=args.rabbitmq_port, user=args.rabbitmq_user,
                        password=args.rabbitmq_password, **kwargs)


def consume_queue_messages(args, webhooks, **kwargs):
    """Begin processing the main loop by reading messages from the queue."""
    if args.json_message_file:
        # Only process a single message
        msg_json = pathlib.Path(args.json_message_file).read_text(encoding='utf-8')
        msg = json.loads(msg_json)
        headers = {'message-type': 'gitlab'}
        process_message(args, webhooks, None, msg, headers, **kwargs)
        return

    if not args.rabbitmq_routing_key:
        LOGGER.error('The argument --rabbitmq-routing-key must be')
        LOGGER.error('specified in order to process the queue. Hint: You may want to process ')
        LOGGER.error('a single merge request with the --merge-request argument.')
        sys.exit(1)

    queue = get_messagequeue(args)

    # Add the queue's send_message() method to the kwargs so handlers can access it.
    kwargs['send_message'] = queue.send_message

    metrics.prometheus_init()
    misc.sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)

    queue.consume_messages(args.rabbitmq_exchange, args.rabbitmq_routing_key.split(),
                           lambda **cbkwargs: process_message(args, webhooks, **cbkwargs, **kwargs),
                           queue_name=args.rabbitmq_queue_name, callback_kwargs=True)


def get_argparse_environ_opts(key, is_list=False):
    """Read default value for argparse from an environment variable if present."""
    val = os.environ.get(key)
    val = val.split() if val and is_list else val
    return {'default': val} if val else {'required': True}


def print_notes(notes):
    """Print the notes section of upstream commit ID report."""
    report = ""
    noteid = 0
    while noteid < len(notes):
        report += f"{noteid + 1}. "
        if notes[noteid] is None:
            noteid += 1
            continue
        report += notes[noteid].rstrip("\n").replace("\n", "\n   ")
        report += "\n"
        noteid += 1
    report += "\n" if report else ""
    report = report.replace("<", "&lt;")
    report = report.replace(">", "&gt;")
    report = report.replace("\n   ", "<br>&emsp;")
    return report


def make_payload_project(mr_url):
    """Return a dict containing a faux hook event project."""
    url = parse.urlsplit(mr_url)
    mr_id = int(url.path.split('/')[-1])
    path_with_namespace = url.path.removesuffix(f'/-/merge_requests/{mr_id}')[1:]
    namespace, project_name = path_with_namespace.rsplit('/', 1)

    return {'id': 123,
            'name': project_name,
            'description': 'A faux project',
            'web_url': f'{url.scheme}://{url.netloc}/{path_with_namespace}',
            'git_ssh_url': f'git@gitlab.com:{path_with_namespace}.git',
            'git_http_url': f'{url.scheme}://{url.netloc}/{path_with_namespace}.git',
            'namespace': namespace,
            'visibility_level': 0,
            'path_with_namespace': path_with_namespace,
            'default_branch': 'main',
            "ci_config_path": ""
            }


def make_payload(url, kind):
    """Create a fake payload dict."""
    payload = {'object_kind': kind,
               'object_attributes': {},
               'project': make_payload_project(url),
               'changes': {},
               'user': {'username': 'cli'},
               'labels': []
               }

    key = 'merge_request' if kind in ('note', 'pipeline') else 'object_attributes'
    payload[key] = {'iid': int(url.split('/')[-1]),
                    'state': 'opened',
                    'target_branch': 'main',
                    'work_in_progress': False
                    }

    if kind == 'note':
        payload['object_attributes']['noteable_type'] = 'MergeRequest'
    return payload


def process_mr_url(mr_url, action, note):
    """Create a fake payload for the given mr/note."""
    if action == 'note':
        payload = make_payload(mr_url, 'note')
        payload["object_attributes"]["note"] = note
    elif action == 'merge_request':
        payload = make_payload(mr_url, 'merge_request')
        payload['object_attributes']['action'] = 'open'
    elif action == 'pipeline':
        payload = make_payload(mr_url, 'pipeline')
    return payload


def generic_loop(args, hook_handlers, **kwargs):
    """Run hook loop."""
    if args.merge_request:
        if not args.action:
            if args.note:
                action = 'note'
            else:
                action = 'merge_request'
        else:
            action = args.action
        payload = process_mr_url(args.merge_request, action, args.note)
        headers = {'message-type': 'gitlab'}
        process_message(args, hook_handlers, 'cmdline', payload, headers, **kwargs)
        return
    consume_queue_messages(args, hook_handlers, **kwargs)


def commits_have_not_changed(payload):
    """Return True if the commits in the MR have not changed."""
    action = payload['object_attributes']['action']
    if action == 'update' and 'oldrev' not in payload['object_attributes']:
        return True
    return False


def mr_action_affects_commits(message):
    """Return True if the message indicates there has been any change to the MR's commits."""
    # Some messages have no action? Not sure why but a KeyError won't get us anywhere.
    action = message.payload['object_attributes'].get('action')
    if action is None:
        LOGGER.warning("Message payload does not have an 'action'.")
        return None

    # Check to see if the target branch was changed.
    if action == 'update' and 'changes' in message.payload and \
       'merge_status' in message.payload['changes']:
        return True

    # True if action is 'open' or, action is 'update' and 'oldrev' is set.
    if commits_have_not_changed(message.payload):
        LOGGER.debug("Ignoring MR \'update\' action without an oldrev.")
        return False
    if action not in ('update', 'open'):
        LOGGER.debug("Ignoring MR action '%s'.", action)
        return False
    return True


def build_note_string(notes):
    """Build note string for report table."""
    notestr = ", ".join(notes)
    notestr = "See " + notestr + "|\n" if notestr else "-|\n"
    return notestr


def build_commits_for_row(row):
    """Build list of commits for a row in report table."""
    commits = row[1] if len(row[1]) < 2 else row[1][:2] + ["(...)"]
    count = 0
    while count < len(commits):
        commits[count] = commits[count][:8]
        count += 1
    return commits


def create_label_object(name, color, description):
    """Return an object ready to pass to add_label_to_merge_request in a list."""
    return {'name': name, 'color': color, 'description': description}


def _get_gitlab_group(gl_instance, gl_project):
    """Get the gitlab group object for this project/MR."""
    namespace = gl_project.namespace['full_path']
    results = gl_instance.groups.list(search=namespace)
    group_id = results[0].id
    gl_group = gl_instance.groups.get(group_id)
    return gl_group


def _create_group_label(gl_instance, gl_project, label):
    """Create a new label at the group level."""
    LOGGER.info('Creating label %s on group.', label)
    gl_group = _get_gitlab_group(gl_instance, gl_project)
    if misc.is_production():
        try:
            gl_group.labels.create(label)
        except GitlabCreateError as err:
            if err.response_code == 409 and "Label already exists" in err.error_message:
                LOGGER.info('%s: %s.', err.error_message, label['name'])
            else:
                raise


def _create_project_label(gl_project, label):
    """Create a new label on the project."""
    LOGGER.info('Creating label %s on project.', label)
    if misc.is_production():
        try:
            gl_project.labels.create(label)
        except GitlabCreateError as err:
            if err.response_code == 409 and "Label already exists" in err.error_message:
                LOGGER.info('%s: %s.', err.error_message, label['name'])
            else:
                raise


def _edit_project_label(gl_project, existing_label, new_label):
    """Check if a project label needs updating. Creates the label if it does not exist."""
    if existing_label:
        # If the label exists then confirm the existing properties match the new label values.
        label_changed = False
        for item in new_label:
            if new_label[item] != getattr(existing_label, item):
                setattr(existing_label, item, new_label[item])
                label_changed = True
        if label_changed:
            LOGGER.info('Editing label %s on project.', existing_label.name)
            if misc.is_production():
                existing_label.save()
    else:
        _create_project_label(gl_project, new_label)


def _edit_group_label(gl_instance, gl_project, existing_label, new_label):
    """Check if a group label needs updating. Creates the label if it does not exist."""
    if existing_label:
        # If the label exists then confirm the existing properties match the new label values.
        label_changed = False
        for item in new_label:
            if new_label[item] != getattr(existing_label, item):
                setattr(existing_label, item, new_label[item])
                label_changed = True
        if label_changed:
            LOGGER.info('Editing label %s on group.', existing_label.name)
            if misc.is_production():
                existing_label.save()
    else:
        _create_group_label(gl_instance, gl_project, new_label)


def _match_label(project, target_label, label_list=None):
    """Return the ProjectLabel object whose name matches the target."""
    if not label_list:
        label_list = project.labels.list(search=target_label)
    return next((label for label in label_list if label.name == target_label), None)


def _add_plabel_quick_actions(gl_project, label_list):
    # Use /label quick action to add the label to the merge request. This requires ensuring that
    # the label is available on the project.
    label_cmds = []

    # If we're only operating on a few labels then don't bother downloading
    # the project's entire label list, just search for them one at a time in _match_label().
    all_labels = gl_project.labels.list(all=True) if len(label_list) >= 5 else None
    for label in label_list:
        existing_label = _match_label(gl_project, label['name'], all_labels)
        _edit_project_label(gl_project, existing_label, label)
        label_cmds.append(f"/label \"{label['name']}\"")

    return label_cmds


def _add_label_quick_actions(gl_instance, gl_project, label_list):
    # Use /label quick action to add the label to the merge request. This requires ensuring that
    # the label is available on the project.
    label_cmds = []

    # If we're only operating on a few labels then don't bother downloading
    # the project's entire label list, just search for them one at a time in _match_label().
    all_labels = gl_project.labels.list(all=True) if len(label_list) >= 5 else None
    for label in label_list:
        existing_label = _match_label(gl_project, label['name'], all_labels)
        if gl_project.id == defs.ARK_PROJECT_ID:
            _edit_project_label(gl_project, existing_label, label)
        else:
            _edit_group_label(gl_instance, gl_project, existing_label, label)
        label_cmds.append(f"/label \"{label['name']}\"")

    return label_cmds


def _find_extra_required_labels(labels):
    extra_required_labels = []
    for label in labels:
        if label.split("::")[-1] in defs.TESTING_SUFFIXES:
            proposed_extra_label = f'{label.split("::")[0]}::{defs.READY_SUFFIX}'
            if proposed_extra_label not in defs.READY_FOR_MERGE_DEPS:
                extra_required_labels.append(proposed_extra_label)
    LOGGER.debug("Extra required labels: %s", extra_required_labels)
    return extra_required_labels


def _compute_mr_status_labels(gl_instance, gl_project, gl_mergerequest, label_cmds):
    required_labels = set(defs.READY_FOR_MERGE_DEPS)
    current_labels = gl_mergerequest.labels
    # special handling for Dependencies::OK::<sha>
    for label in current_labels:
        if label.startswith(f"Dependencies::{defs.READY_SUFFIX}::"):
            current_labels.append(f"Dependencies::{defs.READY_SUFFIX}")
            break
    required_labels.update(_find_extra_required_labels(current_labels))
    if not gl_mergerequest.draft and required_labels.issubset(set(current_labels)):
        if defs.READY_FOR_MERGE_LABEL not in current_labels:
            merge_label = validate_labels([defs.READY_FOR_MERGE_LABEL])
            label_cmds += _add_label_quick_actions(gl_instance, gl_project, merge_label)
            gl_mergerequest.labels.append(defs.READY_FOR_MERGE_LABEL)
            label_cmds.append(f'/unlabel "{defs.READY_FOR_QA_LABEL}"')
    elif not gl_mergerequest.draft and set(defs.READY_FOR_QA_DEPS).issubset(set(current_labels)):
        if defs.READY_FOR_QA_LABEL not in current_labels:
            qa_label = validate_labels([defs.READY_FOR_QA_LABEL])
            label_cmds += _add_label_quick_actions(gl_instance, gl_project, qa_label)
            gl_mergerequest.labels.append(defs.READY_FOR_QA_LABEL)
            label_cmds.append(f'/unlabel "{defs.READY_FOR_MERGE_LABEL}"')
    elif defs.READY_FOR_MERGE_LABEL in current_labels \
            or defs.READY_FOR_QA_LABEL in current_labels:
        label_cmds.append(f'/unlabel "{defs.READY_FOR_MERGE_LABEL}"')
        label_cmds.append(f'/unlabel "{defs.READY_FOR_QA_LABEL}"')
    return label_cmds


def _run_label_commands(gl_mergerequest, label_cmds):
    if label_cmds:
        LOGGER.info('Modifying labels on merge request %s: %s', gl_mergerequest.iid,
                    ' '.join(label_cmds))
        if misc.is_production():
            try:
                gl_mergerequest.notes.create({'body': '\n'.join(label_cmds)})
            except GitlabCreateError as err:
                if err.response_code == 400 and "can't be blank" in err.error_message:
                    LOGGER.exception('Unexpected Gitlab response when creating quick action note.')
                else:
                    raise
        _update_bugzilla_state(gl_mergerequest, label_cmds)
        return True

    LOGGER.info('No labels to change on merge request %s', gl_mergerequest.iid)
    return False


def _filter_mr_labels(merge_request, label_list, remove_scoped):
    """Remove existing scoped labels that match list and add new labels. Return the new list."""
    # If this is a scoped label, then remove the old value from the mr object list so that the
    # readyForMerge label is added or removed appropriately. Support nested scoped labels like
    # BZ::123::OK in the prefix variable.
    filtered_labels = []
    to_remove = []
    for label in label_list:
        if label['name'] not in merge_request.labels:
            if '::' in label['name']:
                if not remove_scoped:
                    prefix = '::'.join(label['name'].split('::')[0:-1])
                    to_remove += [x for x in merge_request.labels if x.startswith(f'{prefix}::') and
                                  x.count('::') == label['name'].count('::')]
                else:
                    prefix = label['name'].split('::')[0]
                    to_remove += [x for x in merge_request.labels if x.startswith(f'{prefix}::')]
            merge_request.labels.append(label['name'])
            filtered_labels.append(label)
    merge_request.labels = [x for x in merge_request.labels if x not in to_remove]
    return filtered_labels, to_remove


def add_plabel_to_merge_request(gl_project, mr_id, label_input, remove_scoped=False):
    """Add project-level labels to a GitLab merge request."""
    LOGGER.info('Evaluating label %s for addition to MR %s/%s',
                label_input, gl_project.path_with_namespace, mr_id)

    # Validate labels against labels.yaml
    labels = validate_labels(label_input)

    # Some of the webhooks can take several minutes to run. To help avoid collisions with the
    # other webhooks, fetch the most recent version of the merge request to get the latest labels.
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    (filtered_labels, old_scoped_labels) = _filter_mr_labels(gl_mergerequest, labels,
                                                             remove_scoped)
    label_cmds = _add_plabel_quick_actions(gl_project, filtered_labels)
    for label in old_scoped_labels:
        label_cmds.append(f'/unlabel "{label}"')

    return _run_label_commands(gl_mergerequest, label_cmds)


def match_single_label(label_name, yaml_labels):
    """Return a label object which matches the label_name, if any."""
    match = None
    for ylabel in yaml_labels:
        if ylabel['name'] == label_name:
            match = create_label_object(ylabel['name'], ylabel['color'], ylabel['description'])
            break
        if ylabel.get('regex'):
            if name_match := re.match(ylabel['name'], label_name):
                description = ylabel['description'].replace('%s', name_match.group(1))
                match = create_label_object(label_name, ylabel['color'], description)
                break
    return match


def validate_labels(label_names, yaml_path=None):
    """Return a list of label objects created from the input list of label names."""
    if not yaml_path:
        yaml_path = defs.LABELS_YAML_PATH
    yaml_data = load_yaml_data(yaml_path)
    if not yaml_data or not yaml_data.get('labels'):
        raise RuntimeError(f'No label data found in {yaml_path}')

    labels = []
    unknown_labels = []
    for label_name in label_names:
        if label := match_single_label(label_name, yaml_data['labels']):
            labels.append(label)
        else:
            unknown_labels.append(label_name)

    LOGGER.debug('Found labels: %s', labels)
    if unknown_labels or len(label_names) != len(labels):
        raise RuntimeError((f'Problem loading labels from {yaml_path}. input: {label_names},'
                            f' unknown: {unknown_labels}'))
    return labels


def _create_group_milestone(gl_group, product, rhmeta):
    """Create a new group milestone based on target release."""
    release = ""
    if rhmeta.internal_target_release:
        release = rhmeta.internal_target_release
    elif rhmeta.zstream_target_release:
        release = rhmeta.zstream_target_release

    if not release:
        LOGGER.warning("Unable to find release for milestone %s", rhmeta.milestone)
        return None

    description = f'{product}, release {release}'
    LOGGER.debug("Creating new milestone for %s (%s)", rhmeta.milestone, description)

    if not misc.is_production():
        LOGGER.info("Would have created milestone for %s (%s)", rhmeta.milestone, description)
        return None

    new_milestone = gl_group.milestones.create({'title': rhmeta.milestone,
                                                'description': description})
    return new_milestone


def add_merge_request_to_milestone(gl_instance, gl_project, gl_mergerequest):
    """Add a merge request to a milestone, creating it, if need be."""
    proj_data = Projects().get_project_by_id(gl_project.id)
    rhmeta = proj_data.get_branch_by_name(gl_mergerequest.target_branch)
    if not rhmeta.milestone:
        LOGGER.debug("No milestone defined for %s in rh_metadata", rhmeta.component)
        return

    gl_group = _get_gitlab_group(gl_instance, gl_project)
    group_milestones = gl_group.milestones.list(as_list=False)

    if gl_mergerequest.milestone and gl_mergerequest.milestone['title'] == rhmeta.milestone:
        LOGGER.debug("This MR is already assigned to target milestone %s (id: %s)",
                     gl_mergerequest.milestone['title'], gl_mergerequest.milestone['id'])
        return

    LOGGER.debug("Attempting to assign %s to milestone %s",
                 gl_mergerequest.references['full'], rhmeta.milestone)
    if gl_mergerequest.milestone:
        LOGGER.debug("MR currently assigned to milestone %s (id: %s)",
                     gl_mergerequest.milestone['title'], gl_mergerequest.milestone['id'])
    new_milestone = None
    for milestone in group_milestones:
        if milestone.title == rhmeta.milestone:
            LOGGER.debug("Found existing milestone %s (id: %s) to assign MR to",
                         milestone.title, milestone.id)
            new_milestone = milestone

    if new_milestone is None:
        new_milestone = _create_group_milestone(gl_group, proj_data.product, rhmeta)

    if new_milestone is not None and misc.is_production():
        gl_mergerequest.milestone_id = new_milestone.id
        gl_mergerequest.save()
        LOGGER.debug("Assigned MR %s to Milestone %s (id: %s)",
                     gl_mergerequest.references['full'], new_milestone.title, new_milestone.id)


def add_label_to_merge_request(gl_instance, gl_project, mr_id, label_input, remove_scoped=False):
    """Add group-level labels to a GitLab merge request.

    Args:
        gl_instance: GitLab instance object as returned by the gitlab module.
        gl_project: Project object as returned by the gitlab module.
        mr_id: The ID of the MR to add the label(s) to.
        label_input: A List containing at least one dict describing a label. See
                     create_label_object().

    Returns:
        True if any labels on the given MR changed.
        False if there was no change to the MR's labels.
    """
    LOGGER.info('Evaluating label %s for addition to MR %s/%s',
                label_input, gl_project.path_with_namespace, mr_id)

    # Validate labels against labels.yaml
    labels = validate_labels(label_input)

    # Some of the webhooks can take several minutes to run. To help avoid collisions with the
    # other webhooks, fetch the most recent version of the merge request to get the latest labels.
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    (filtered_labels, old_scoped_labels) = _filter_mr_labels(gl_mergerequest, labels,
                                                             remove_scoped)
    label_cmds = _add_label_quick_actions(gl_instance, gl_project, filtered_labels)
    for label in old_scoped_labels:
        label_cmds.append(f'/unlabel "{label}"')

    label_cmds = _compute_mr_status_labels(gl_instance, gl_project, gl_mergerequest, label_cmds)
    return _run_label_commands(gl_mergerequest, label_cmds)


def remove_labels_from_merge_request(gl_project, mr_id, labels):
    """Remove a label on a GitLab merge request."""
    # Some of the webhooks can take several minutes to run. To help avoid collisions with the
    # other webhooks, fetch the most recent version of the merge request to get the latest labels.
    gl_mergerequest = gl_project.mergerequests.get(mr_id)

    label_cmds = []
    for label in labels:
        if label in gl_mergerequest.labels:
            label_cmds.append(f'/unlabel "{label}"')
            gl_mergerequest.labels.remove(label)

    return _run_label_commands(gl_mergerequest, label_cmds)


def required_label_removed(payload, suffix, changed_labels):
    """Return True if an extra required label was removed, else False."""
    no_commit_changes = commits_have_not_changed(payload)
    for label in changed_labels:
        if not label.endswith(suffix):
            continue
        # Don't act on the base ready labels, let their hooks handle them
        if label in defs.READY_FOR_MERGE_DEPS:
            continue
        prefix = label.split("::")[0]
        # Filter out any and all Acks::<subsystem>::scope labels
        if prefix == "Acks":
            continue
        if suffix == defs.NEEDS_TESTING_SUFFIX:
            if f'{prefix}::{defs.READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_FAILED_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == defs.READY_SUFFIX:
            if f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_FAILED_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == defs.TESTING_FAILED_SUFFIX:
            if f'{prefix}::{defs.READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_WAIVED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
        elif suffix == defs.TESTING_WAIVED_SUFFIX:
            if f'{prefix}::{defs.READY_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.NEEDS_TESTING_SUFFIX}' not in changed_labels and \
               f'{prefix}::{defs.TESTING_FAILED_SUFFIX}' not in changed_labels and \
               no_commit_changes:
                return True
    return False


def get_changed_labels(changes):
    """Return the set of changed label names."""
    if 'labels' not in changes:
        return set()

    prev_labels = {item['title'] for item in changes['labels']['previous']}
    cur_labels = {item['title'] for item in changes['labels']['current']}

    changed_labels = set()
    changed_labels.update(prev_labels.difference(cur_labels))
    changed_labels.update(cur_labels.difference(prev_labels))
    return changed_labels


def has_label_suffix_changed(msg_payload, label_suffix):
    """Return True if a changed label matches on suffix, else False."""
    if changed_labels := get_changed_labels(msg_payload['changes']):
        return required_label_removed(msg_payload, label_suffix, changed_labels)
    return False


def has_label_prefix_changed(changes, label_prefix):
    """Return True if a changed label matches on prefix, else False."""
    if changed_labels := get_changed_labels(changes):
        return bool([label for label in changed_labels if label.startswith(label_prefix)])
    return False


def has_label_changed(changes, label_name):
    """Return True if the matching label changed, else False."""
    return bool(label_name in get_changed_labels(changes))


def force_webhook_evaluation(notetext, webhook_name):
    """Check to see if the note text requested a evaluation from the webhook."""
    return notetext.startswith('request-evaluation') or \
        notetext.startswith(f'request-{webhook_name}-evaluation')


def try_bugzilla_conn():
    """If BUGZILLA_API_KEY is set then try to return a bugzilla connection object."""
    if not os.environ.get('BUGZILLA_API_KEY'):
        LOGGER.info("No bugzilla API key, not connecting to bugzilla.")
        return False
    return connect_bugzilla(os.environ.get('BUGZILLA_API_KEY'))


def connect_bugzilla(api_key, cookie_file=None, token_file=None):
    """Connect to bugzilla and return a bugzilla connection object."""
    try:
        # See https://github.com/python-bugzilla/python-bugzilla/blob/master/bugzilla/base.py#L175
        bzcon = bugzilla.Bugzilla('bugzilla.redhat.com', api_key=api_key,
                                  cookiefile=cookie_file, tokenfile=token_file)
    except ConnectionError:
        LOGGER.exception("Problem connecting to bugzilla server.")
        return False
    except PermissionError:
        LOGGER.exception("Problem with file permissions for bugzilla connection.")
        return False
    return bzcon


def find_bz_in_line(line, prefix):
    """Return bug numbers from a line we think contains bugzilla info."""
    line = line.rstrip()
    pattern = r'^\s*' + prefix + \
        r':( *|\t)(?P<url>http(s)?://bugzilla\.redhat\.com/(show_bug\.cgi\?id=)?)(?P<bug>\d{4,8})$'
    bznum_re = re.compile(pattern, re.IGNORECASE)
    bugs = bznum_re.match(line)
    if bugs:
        url_prefix = bugs.group('url')
        bug = bugs.group('bug')
        LOGGER.debug("Found bz url of %s%s", url_prefix, bug)
        return bug, url_prefix
    if prefix in line and 'INTERNAL' in line:
        return 'INTERNAL', ''
    return None, None


def find_cve_in_line(line, prefix):
    """Return CVE number from properly formatted CVE: line."""
    # CVEs must be called out one-per-line, begin with f'{prefix}: ' and contain a complete CVE
    # format. Format being, prefix + Year + Arbitrary Digits, where Arbitrary digit are a sequence
    # from 5 to 7 digits
    line = line.rstrip()
    cvenum_re = re.compile(prefix + r'(^\s*|: )(?P<cve>CVE-\d{4}-\d{4,7})')
    cves = cvenum_re.match(line)

    return cves.group('cve') if cves else None


def find_dep_mr_in_line(namespace, line):
    """Return a Merge Request number from a Depends: <mr url> line."""
    # Depends: <mr url> lines require a full MR URL, one-per-line.
    line = line.rstrip()
    pattern = r'Depends: https://gitlab\.com/(.*)/-/merge_requests/(\d+)'
    if match := re.match(pattern, line):
        if match[1] != namespace:
            LOGGER.warning("Dependent MR is from another project")
            return None
        LOGGER.debug("Extracted MR number of %s from GitLab URL", match[2])
        return int(match[2])

    # Alternate GitLab syntax, Depends: !<MR#>, always from the same project
    pattern = r'Depends: !(?P<mr_number>\d+)'
    if match := re.match(pattern, line):
        mr_number = match.group('mr_number')
        LOGGER.debug("Extracted MR number of %s from GitLab syntax", mr_number)
        return int(mr_number)

    return None


def get_owners_parser(owners_yaml):
    """Return a parser for the owners.yaml to lookup kernel subsystem information."""
    return owners.Parser(pathlib.Path(owners_yaml).read_text(encoding='utf-8'))


def extract_all_from_message(message, mr_bugs, mr_cves, dependencies):
    """Extract all BZs from the message."""
    # pylint: disable=too-many-locals,too-many-branches
    bzs = []
    cves = []
    non_mr_bzs = []
    non_mr_cves = []
    dep_bzs = []
    errors = ''
    no_bug_found = True

    if message:
        mlines = message.splitlines()
        for line in mlines:
            # BZs must be called out one-per-line, begin with 'Bugzilla:' and
            # contain a complete BZ URL or 'INTERNAL'. We parse it in a
            # permissive way first to show proper error messages.
            if "bugzilla" in line.lower():
                tag = "Bugzilla"
                bug, url_prefix = find_bz_in_line(line, tag)
                if not bug:
                    continue
                if not line.startswith(f'{tag}: {url_prefix}{bug}'):
                    if no_bug_found:
                        errors += f'`Expected:` `{tag}: {url_prefix}{bug}`  \n'
                        errors += f'`Found   :` `{line}`  \n'
                    continue
                if bug in dependencies:
                    if bug in mr_bugs:
                        LOGGER.warning("Found bug %s as both Bugzilla: and Depends:", bug)
                    dep_bzs.append(bug)
                    no_bug_found = False
                elif mr_bugs and bug not in mr_bugs:
                    LOGGER.debug("Bugzilla: %s not listed in MR description.", bug)
                    non_mr_bzs.append(bug)
                else:
                    bzs.append(bug)
                    no_bug_found = False

            elif "CVE" in line:
                cve = find_cve_in_line(line, "CVE")
                if not cve:
                    continue
                if mr_cves and cve not in mr_cves:
                    LOGGER.debug("CVE: %s not listed in MR description.", cve)
                    non_mr_cves.append(cve)
                else:
                    cves.append(cve)

    # We return empty arrays if no bugs are found
    return bzs, non_mr_bzs, dep_bzs, cves, non_mr_cves, errors


def extract_bzs(message):
    """Extract BZs from the message."""
    bzs, _x, _y, _z, _w, errors = extract_all_from_message(message, [], [], [])
    return bzs, errors


def extract_cves(message):
    """Extract BZs from the message."""
    _x, _y, _z, cves, _w, _v = extract_all_from_message(message, [], [], [])
    return cves


def extract_dependencies(project, description):
    """Extract Depends: bugs from MR description."""
    bzs = []
    mr_list = []
    errors = ''

    # Depends must be called out one-per-line, begin with 'Depends:' and
    # contain a complete BZ URL. We parse it in a permissive way first to show
    # proper error messages.
    if description:
        dlines = description.splitlines()
        for line in dlines:
            bug, url_prefix = find_bz_in_line(line, 'Depends')
            if bug:
                if not line.startswith(f'Depends: {url_prefix}{bug}'):
                    errors += f'`Expected:` `Depends: {url_prefix}{bug}`  \n'
                    errors += f'`Found   :` `{line.strip()}`  \n'
                bzs.append(bug)
                continue
            if dep_mr := find_dep_mr_in_line(project.path_with_namespace, line):
                mr_list.append(dep_mr)
    if mr_list:
        for dep_mr in mr_list:
            mreq = get_mr(project, dep_mr)
            for line in mreq.description.splitlines():
                if bug := find_bz_in_line(line, 'Bugzilla')[0]:
                    LOGGER.debug("Adding dependent bug from MR URL %s", bug)
                    bzs.append(bug)
    # We return an empty array if there are no bz deps
    return bzs, errors


def bugs_to_process(mr_description, action, changes):
    """Return a dict with the sets of bugs to process for this MR based on the action."""
    bugs = {'link': set(), 'unlink': set()}
    current_bugs = set(extract_bzs(mr_description)[0])
    if action == 'close':
        bugs['unlink'] = current_bugs
        return bugs

    old_bugs = set()
    if 'description' in changes:
        old_bugs = set(extract_bzs(changes['description']['previous'])[0])

    # Just because a BZ was mentioned in the previous description don't assume it was linked.
    bugs['link'] = current_bugs
    bugs['unlink'] = old_bugs - current_bugs
    return bugs


def get_mr(gl_project, mr_id):
    """Return a MR object."""
    try:
        return gl_project.mergerequests.get(mr_id)
    except GitlabGetError as err:
        if err.response_code == 404:
            LOGGER.warning('MR %s does not exist in project %s (404).', mr_id, gl_project.id)
            return None
        raise


def get_pipeline(gl_project, pipeline_id):
    """Return a pipeline object."""
    try:
        return gl_project.pipelines.get(pipeline_id)
    except GitlabGetError as err:
        if err.response_code == 404:
            LOGGER.warning('Pipeline %s does not exist in project %s (404).', pipeline_id,
                           gl_project.id)
            return None
        raise


class _CkiRequestsHTTPTransport(RequestsHTTPTransport):
    """A RequestsHTTPTransport with our own session object."""

    # pylint: disable=too-few-public-methods
    def __init__(self, *args, session=None, headers=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.session = session or get_session(__name__)
        self.session.headers.update(headers or {})

    def connect(self) -> None:
        """Skip connect as we have our own session."""

    def close(self) -> None:
        """Skip close as we have our own session."""


def gl_graphql_client(headers=None, session=None, fetch_schema=False):
    """Return a gql client object connected to GL."""
    if not headers:
        headers = {}

    if not session and (token := get_token('https://gitlab.com')) is not None:
        headers['Authorization'] = f'Bearer {token}'

    transport = _CkiRequestsHTTPTransport(session=session, headers=headers,
                                          url='https://gitlab.com/api/graphql')
    return Client(transport=transport, fetch_schema_from_transport=fetch_schema)


def draft_status(payload):
    """Return a tuple with the current Draft status and whether it just changed."""
    is_draft = payload['object_attributes'].get('work_in_progress')
    changed = False
    if 'title' in payload['changes']:
        old_title = payload['changes']['title'].get('previous', '')
        new_title = payload['changes']['title'].get('current', '')

        str_tuple = ('[Draft]', 'Draft:', '(Draft)')
        tests = [old_title.startswith(str_tuple), new_title.startswith(str_tuple)]
        if any(tests) and not all(tests):
            changed = True
    LOGGER.debug('is_draft: %s, changed: %s', is_draft, changed)
    return is_draft, changed


def _grep_for_config(config, kernel_src):
    # pylint: disable=subprocess-run-check
    """Grep for the config item and return matching Kconfig paths."""
    new_paths = []
    config = config.removeprefix('CONFIG_')
    cmd = ['grep', '-lErs', '--include=Kconfig*']
    cmd.append(rf'^(menu)?config {config}($|[[:space:]]*\#)')
    cmd.append(kernel_src)
    results = run(cmd, capture_output=True, text=True)
    if results.returncode > 1:
        results.check_returncode()
    elif results.returncode == 0:
        new_paths = [path.removeprefix(kernel_src + '/') for path in results.stdout.split()]
    LOGGER.debug('New paths for CONFIG_%s: %s', config, new_paths)
    return new_paths


def find_config_items_kconfigs(configs, kernel_src):
    """Return the Kconfig file paths for the given list of kernel config items."""
    kconfig_paths = set()
    for config in configs:
        kconfig_paths.update(_grep_for_config(config, os.path.normpath(kernel_src)))
    return kconfig_paths


def process_config_items(kernel_src, path_list):
    """Return a list of Kconfig paths that correspond to any config files in the path_list."""
    config_paths = ('redhat/configs/ark/',
                    'redhat/configs/common/',
                    'redhat/configs/debug/',
                    'redhat/configs/generic/'
                    )
    config_label = []
    configs = [path.rsplit('/')[-1] for path in path_list if path.startswith(config_paths) and
               '/CONFIG_' in path]
    if configs:
        config_label = [defs.CONFIG_LABEL]
    return find_config_items_kconfigs(set(configs), kernel_src), config_label


def load_yaml_data(yaml_path):
    """Return the yaml data from the given file."""
    try:
        # pylint: disable=unspecified-encoding
        with open(yaml_path) as yaml_file:
            return safe_load(yaml_file)
    # pylint: disable=broad-except
    except Exception:
        LOGGER.exception('Problem loading yaml file')
        return None


def do_not_run_hook(gl_project, gl_mergerequest, hook_name, run_on_drafts):
    """Let us know (and log) if the hook should not run."""
    if misc.is_production() and gl_mergerequest.draft and not run_on_drafts:
        LOGGER.info("Not running %s hook on %s/%s, MR is in draft state",
                    hook_name, gl_project.path_with_namespace, gl_mergerequest.iid)
        return True

    if mr_is_closed(gl_mergerequest):
        LOGGER.info("Not running %s hook on %s/%s, MR is closed",
                    hook_name, gl_project.path_with_namespace, gl_mergerequest.iid)
        return True

    if gl_project.archived:
        LOGGER.info("Not running %s hook on %s/%s, project is archived",
                    hook_name, gl_project.path_with_namespace, gl_mergerequest.iid)
        return True

    return False


def get_authlevel(gl_project, author_id):
    """Get the gitlab access level for the given author id."""
    authal = 0
    try:
        authinfo = gl_project.members_all.get(author_id)
        authal = authinfo.access_level
    except GitlabGetError as err:
        if err.response_code == 404:
            LOGGER.info('%s: author id %s.', err.error_message, author_id)
        else:
            raise
    return authal


def get_commits_count(gl_project, gl_mergerequest):
    """Get the number of commits in the MR."""
    count = len(gl_mergerequest.commits())
    # For some value of large, large merge requests report 0 to len, unless
    # we wrap the call with list()
    if count == 0:
        count = len(list(gl_mergerequest.commits()))

    authlevel = get_authlevel(gl_project, gl_mergerequest.author['id'])
    if count > defs.MAX_COMMITS_PER_MR and authlevel < MAINTAINER_ACCESS:
        LOGGER.warning("MR %s has %d commits, too many to process -- wrong target branch?",
                       gl_mergerequest.iid, count)

    return count, authlevel


def match_gl_username_to_email(gl_instance, email, username):
    """Match a gitlab username to an email -- works for both public and non-public emails."""
    if username is None:
        return False

    userlist = gl_instance.users.list(search=email, as_list=False)
    for user in userlist:
        if user.username == username:
            return True

    return False


def _update_bugzilla_state(gl_mergerequest, label_cmds):
    """Run set_bug_state if we're adding a readyForX label and not called from the bugzilla hook."""
    if f'/label "{defs.READY_FOR_QA_LABEL}"' not in label_cmds and \
            f'/label "{defs.READY_FOR_MERGE_LABEL}"' not in label_cmds:
        return

    # The bugzilla hook got us here and it is going to call set_bug_state() itself.
    if f'/label "Bugzilla::{defs.READY_SUFFIX}"' in label_cmds or \
            f'/label "Bugzilla::{defs.NEEDS_TESTING_SUFFIX}"' in label_cmds:
        return

    set_bug_state(None, None, None, None, defs.BZ_STATE_MODIFIED, gl_mergerequest)


def set_bug_state(gl_project, mrid, bug_list, bzcon, new_state, gl_mergerequest=None):
    # pylint: disable=too-many-arguments
    """Wrap set_bug_state."""
    merge_request = gl_mergerequest if gl_mergerequest else gl_project.mergerequests.get(mrid)
    if bug_list is None:
        bug_list, *_ = extract_bzs(gl_mergerequest.description)
    if bzcon is None:
        bzcon = try_bugzilla_conn()
    _set_bug_state(merge_request, bug_list, bzcon, new_state)


def _set_bug_state(merge_request, bug_list, bzcon, new_state):
    # pylint: disable=too-many-branches
    """Set relevant bugzillas to new state."""
    update_bug_list = []

    # Re-read merge request data to get latest labels, which may have been recently updated
    ready_labels = [defs.READY_FOR_QA_LABEL, defs.READY_FOR_MERGE_LABEL]
    can_be_modified = any(label in merge_request.labels for label in ready_labels)
    if new_state == defs.BZ_STATE_MODIFIED and not can_be_modified:
        LOGGER.info("This merge request has no %s or %s label, not updating to %s",
                    defs.READY_FOR_QA_LABEL, defs.READY_FOR_MERGE_LABEL, defs.BZ_STATE_MODIFIED)
        return

    # Do not update bugs when the MR is in Draft state
    if merge_request.draft:
        LOGGER.info("Not updating bugs for MR %s, because it's marked as a Draft.",
                    merge_request.iid)
        return

    # We only support POST and MODIFIED as valid new states we'll set
    if new_state not in (defs.BZ_STATE_POST, defs.BZ_STATE_MODIFIED):
        LOGGER.error("Invalid new BZ state provided: %s", new_state)
        return

    for bug_no in bug_list:
        # This isn't a bug number, we can't tell bugzilla to change it's state
        if bug_no == 'INTERNAL':
            LOGGER.info("This isn't a bug, it's a commit or MR flagged as INTERNAL.")
            continue

        bug = bzcon.getbug(bug_no, include_fields=defs.BUG_FIELDS)
        if not bug or bug.id != int(bug_no):
            LOGGER.error("getbug() did not return a useful result for BZ %s.", bug_no)
            continue

        # Do we even need to do anything for this bug?
        if bug.status == new_state:
            LOGGER.info("Bug %s already in state %s, nothing to do here.", bug.id, bug.status)
            continue

        # Only move bug to POST if NEW or ASSIGNED
        if new_state == defs.BZ_STATE_POST:
            if bug.status in (defs.BZ_STATE_NEW, defs.BZ_STATE_ASSIGNED):
                LOGGER.info("Will update bug %s from %s to %s.",
                            bug_no, bug.status, defs.BZ_STATE_POST)
                update_bug_list.append(bug.id)
            else:
                LOGGER.info("Not modifying bug %s from %s to %s.",
                            bug_no, bug.status, defs.BZ_STATE_POST)

        # Only move bug to MODIFIED if in POST
        elif new_state == defs.BZ_STATE_MODIFIED:
            if bug.status == defs.BZ_STATE_POST:
                LOGGER.info("Will update bug %s from %s to %s.",
                            bug_no, bug.status, defs.BZ_STATE_MODIFIED)
                update_bug_list.append(bug.id)
            else:
                LOGGER.info("Not modifying bug %s from %s to %s.",
                            bug_no, bug.status, defs.BZ_STATE_MODIFIED)

    if update_bug_list and misc.is_production():
        update = bzcon.build_update(status=new_state)
        bzcon.update_bugs(update_bug_list, update)


def wrap_comment_table(table_header, table_entries, footnotes, desc,
                       limit=defs.TABLE_ENTRY_THRESHOLD):
    """Wrap a comment table with more than X entries in collapsible element markdown."""
    collapse_header = ""
    collapse_footer = ""

    if not table_entries:
        return ""

    count = len(table_entries.splitlines())
    if count > limit:
        LOGGER.info("Comment table has %s entries, wrapping (threshold: %s)", count, limit)
        collapse_header = f"<details><summary>Click to show/hide {desc}</summary>\n\n"
        collapse_footer = "</details>\n\n"

    return collapse_header + table_header + table_entries + footnotes + collapse_footer


def update_webhook_comment(gl_mergerequest, bot_name, identifier, new_comment):
    """Find the webhook's status comment so we can edit it in place, add one if none exists."""
    comment = None
    bot_discussions = [discussion for discussion in gl_mergerequest.discussions.list(as_list=False)
                       if discussion.attributes['notes'][0]['author']['username'] == bot_name]
    for _, discussion in enumerate(bot_discussions):
        first_comment_data = discussion.attributes['notes'][0]
        if identifier in first_comment_data['body']:
            comment = discussion.notes.get(first_comment_data['id'])
            break
    if comment:
        LOGGER.debug('Overwriting existing webhook comment:\n%s', new_comment)
        if misc.is_production():
            comment.body = new_comment
            comment.save()
    else:
        LOGGER.debug('Creating new webhook comment:\n%s', new_comment)
        if misc.is_production():
            gl_mergerequest.notes.create({'body': new_comment})


def get_pipeline_variable(payload, key):
    """Return the value of the given variables key, or None."""
    variables = misc.get_nested_key(payload, 'object_attributes/variables')
    return next((var['value'] for var in variables if var['key'] == key), None) if variables \
        else None


def is_mr_event_branch_active(payload):
    """Return True if the target_branch in the merge request event is active, otherwise False."""
    return is_branch_active(payload['project']['id'], payload['object_attributes']['target_branch'])


def is_note_event_branch_active(payload):
    """Return True if the target_branch in the note event is active, otherwise False."""
    return is_branch_active(payload['project']['id'], payload['merge_request']['target_branch'])


def is_pipeline_event_branch_active(payload):
    """Return True if the target_branch in the pipeline event is active, otherwise False."""
    project_name = target = None
    # pipeline events triggered by an MR have target branch details
    if target := misc.get_nested_key(payload, 'merge_request/target_branch'):
        project_name = payload['project']['name']
    # pipeline events triggered by another pipeline have no MR details. But maybe the pipeline
    # variables include the target 'branch'? And use the project name from pipeline vars, not the
    # downstream project name.
    elif mr_url := get_pipeline_variable(payload, 'mr_url'):
        target = get_pipeline_variable(payload, 'branch')
        project_name = mr_url.split('/')[-4]
    return is_branch_active(project_name, target) if (project_name and target) else None


def is_event_target_branch_active(msg_payload):
    """Wrap is_`object_kind`_branch_active() functions."""
    match msg_payload['object_kind']:
        case 'merge_request':
            return is_mr_event_branch_active(msg_payload)
        case 'note':
            if msg_payload['object_attributes'].get('noteable_type') != 'MergeRequest':
                LOGGER.info('Note event not associated with an MR, no branch to check.')
                return None
            return is_note_event_branch_active(msg_payload)
        case 'pipeline':
            return is_pipeline_event_branch_active(msg_payload)
    LOGGER.warning('No handler for object_kind %s', msg_payload['object_kind'])
    return None
