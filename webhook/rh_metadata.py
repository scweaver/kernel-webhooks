"""Metadata for RHEL projects."""

from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
from dataclasses import replace
from os import environ

from cki_lib.logger import get_logger
from cki_lib.yaml import load

from webhook.cpc import get_policy_data
from webhook.defs import RH_METADATA_YAML_PATH

LOGGER = get_logger(__name__)


def check_data(this_dc):
    """Check the dataclass object fields have the right types and are not empty values."""
    dc_type = type(this_dc)
    for dc_field in fields(this_dc):
        fvalue = getattr(this_dc, dc_field.name)
        # fields should hold data of the correct type
        if not isinstance(fvalue, dc_field.type):
            raise TypeError(f'{dc_type}.{dc_field.name} must be {dc_field.type}: {this_dc}')
        # do not test for empty values if a field is a bool or has an empty default value
        if isinstance(fvalue, bool) or (isinstance(fvalue, str) and dc_field.default == '') or \
           (isinstance(fvalue, list) and dc_field.default_factory is list):
            continue
        # fields with non-empty default values should not have empty values
        if not fvalue:
            raise ValueError(f'{dc_type}.{dc_field.name} cannot be empty: {this_dc}')


@dataclass(frozen=True)
class Branch:
    # pylint: disable=too-many-instance-attributes
    """Branch metadata."""

    name: str
    component: str
    distgit_ref: str
    internal_target_release: str = ''
    zstream_target_release: str = ''
    milestone: str = ''
    inactive: bool = False
    policy: list = field(default_factory=list)

    def __post_init__(self):
        """Check data."""
        check_data(self)

    def set_policy(self, tokens):
        """Set the given policy."""
        self.__dict__['policy'] = tokens


@dataclass(frozen=True)
class Project:
    """Project metadata."""

    ids: list
    name: str
    product: str
    branches: list
    inactive: bool = False

    def __post_init__(self):
        """Set up branches and check data."""
        self.__dict__['branches'] = [Branch(**b) for b in self.branches]
        check_data(self)

    def _return_branch(self, branch):
        """Return the branch with the inactive field set."""
        return branch if not branch or not self.inactive else replace(branch, inactive=True)

    def get_branch_by_name(self, branch_name):
        """Return the branch with the given name, or None."""
        return self._return_branch(next((b for b in self.branches if b.name == branch_name), None))

    def get_branches_by_itr(self, itr):
        """Return a list of active branches whose policy matches the given ITR."""
        # if the project is inactive then we treat all branches as inactive
        return [] if self.inactive else [b for b in self.branches if
                                         b.internal_target_release == itr and not b.inactive]

    def get_branches_by_ztr(self, ztr):
        """Return a list of active branches whose policy matches the given ZTR."""
        # if the project is inactive then we treat all branches as inactive
        return [] if self.inactive else [b for b in self.branches if
                                         b.zstream_target_release == ztr and not b.inactive]


@dataclass(frozen=True)
class Projects:
    """Projects metadata."""

    projects: list = field(init=False)
    load_policies: InitVar[bool] = False

    def __post_init__(self, load_policies):
        """Load yaml into Projects and check data."""
        projects_yaml = load(file_path=environ.get('RH_METADATA_YAML_PATH', RH_METADATA_YAML_PATH))
        self.__dict__['projects'] = [Project(**p) for p in projects_yaml['projects']]
        if load_policies:
            self.do_load_policies()
        check_data(self)

    def get_project_by_id(self, project_id):
        """Return the project with the given project_id, or None."""
        return next((p for p in self.projects if int(project_id) in p.ids), None)

    def get_project_by_name(self, project_name):
        """Return the project with the given name, or None."""
        return next((p for p in self.projects if p.name == project_name), None)

    def do_load_policies(self):
        """Load policy tokens for each branch."""
        policies = get_policy_data()
        for project in self.projects:
            for branch in project.branches:
                if policy := policies.get(branch.distgit_ref):
                    branch.set_policy(policy)


def is_branch_active(project_id, target_branch):
    """Return True if the target branch is active, otherwise False."""
    projects = Projects()
    if isinstance(project_id, int) or project_id.isdigit():
        project = projects.get_project_by_id(project_id)
    else:
        project = projects.get_project_by_name(project_id)
    if project:
        if branch := project.get_branch_by_name(target_branch):
            return not branch.inactive
    else:
        LOGGER.warning('Project %s is not present in rh_metadata.', project_id)
    return False
