"""Webhook interaction tests."""
import copy
from unittest import TestCase
from unittest import mock

from gitlab.exceptions import GitlabGetError

from webhook import ack_nack
from webhook import common
from webhook import defs
from webhook import owners


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
class TestAckNack(TestCase):
    PAYLOAD_NOTE = {'object_kind': 'note',
                    'user': {'id': 1, 'username': 'user1'},
                    'project': {'id': 1,
                                'web_url': 'https://web.url/g/p',
                                'path_with_namespace': 'g/p'
                                },
                    'object_attributes': {'iid': 1,
                                          'noteable_type': 'MergeRequest',
                                          'note': 'comment',
                                          'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    @mock.patch('webhook.common.update_webhook_comment')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_comment_save(self, add_label, add_comment):
        inst = mock.Mock()
        inst.user.username = "shadowman"
        proj = mock.Mock()
        mreq = mock.MagicMock()
        mreq.state = 'open'
        status = "OK"
        labels = ["Acks::net::OK"]
        message = "Everything is fine, I swear..."
        ack_nack._save(inst, proj, mreq, status, labels, message)
        add_comment.assert_called_once()
        note = f'**ACK/NACK Summary: {status}**\n\n{message}'
        add_comment.assert_called_with(mreq, "shadowman", "**ACK/NACK Summary:", note)

    @mock.patch('webhook.common.update_webhook_comment', mock.Mock(return_value=None))
    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.ack_nack._edit_approval_rule')
    def test_get_reviewers(self, ear, gccount):
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "     - name: User 3\n"
                       "       email: user3@redhat.com\n"
                       "       gluser: user3\n"
                       "       restricted: true\n"
                       "   reviewers:\n"
                       "     - name: User 4\n"
                       "       email: user4@redhat.com\n"
                       "       gluser: user4\n"
                       "       restricted: false\n"
                       "     - name: User 5\n"
                       "       email: user5@redhat.com\n"
                       "       gluser: user5\n"
                       "     - name: User 6\n"
                       "       email: user6@redhat.com\n"
                       "       gluser: user6\n"
                       "       restricted: true\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n"
                       " - subsystem: Another Subsystem\n"
                       "   labels:\n"
                       "     name: AnotherSubsystem\n"
                       "   requiredApproval: false\n"
                       "   maintainers:\n"
                       "     - name: User 7\n"
                       "       gluser: user7\n"
                       "       email: user7@redhat.com\n"
                       "   reviewers:\n"
                       "     - name: User 8\n"
                       "       gluser: user8\n"
                       "       email: user8@redhat.com\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - Documentation/\n")
        owners_parser = owners.Parser(owners_yaml)
        gl_instance = mock.Mock()
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        gl_project = mock.Mock()
        ainfo = mock.Mock()
        gccount.return_value = 20, 30
        ainfo.access_level = 30
        gl_project.members_all.get.return_value = ainfo
        gl_mergerequest = mock.Mock()
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 2
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        gl_mergerequest.target_branch = "main"
        gl_mergerequest.author = {'id': 1, 'username': 'user1'}
        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                                      ['redhat/Makefile'], owners_parser)
        # user1@redhat.com is the merge request submitter and shouldn't be in the reviewer list.
        self.assertEqual(reviewers, [(frozenset(['user2', 'user3', 'user4', 'user5', 'user6']),
                         'SomeSubsystem')])
        self.assertEqual(required, [(frozenset(['user2', 'user4', 'user5']), 'SomeSubsystem')])

        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                                      [], owners_parser)
        self.assertEqual(reviewers, [])
        self.assertEqual(required, [])

        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                                      ['Documentation/foo'], owners_parser)
        self.assertEqual(reviewers, [(frozenset(['user7', 'user8']), 'AnotherSubsystem')])
        self.assertEqual(required, [])

        gl_mergerequest.target_branch = "main-auto"
        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                                      ['redhat/Makefile'], owners_parser)
        self.assertEqual(reviewers, [])
        self.assertEqual(required, [])

        gccount.return_value = 2001, 30
        gl_mergerequest.target_branch = "main"
        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                                      ['redhat/Makefile'], owners_parser)
        self.assertEqual(reviewers, [])
        self.assertEqual(required, [])

        # simulate the case where we can't find the author details
        gccount.return_value = 20, 30
        exceptions = [[GitlabGetError('404 Not Found', 404), True],
                      [GitlabGetError('random error', 0), False]]
        for e in exceptions:
            gl_project.members_all.get.side_effect = e[0]
            exception = False
            gitlab_exception = False
            try:
                reviewers, required = ack_nack._get_reviewers(gl_instance, gl_project,
                                                              gl_mergerequest, ['redhat/Makefile'],
                                                              owners_parser)
            except GitlabGetError:
                gitlab_exception = True
            except Exception:
                exception = True
            if e[1]:
                self.assertFalse(gitlab_exception)
            else:
                self.assertFalse(exception)

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    @mock.patch('webhook.common.get_commits_count')
    def test_get_reviewers_no_self_review(self, gccount, to_user):
        # The person that opened the merge request is the only person listed as a subsystem
        # maintainer. That person should not be listed as a required reviewer.
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        owners_parser = owners.Parser(owners_yaml)
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'username': 'user1'}
        gccount.return_value = 20, 30
        ainfo = mock.Mock()
        ainfo.access_level = 30
        gl_project.members_all.get.return_value = ainfo
        to_user.return_value = ['user1']
        notified, reviewers = ack_nack._get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                                      ['redhat/Makefile'], owners_parser)
        self.assertEqual(notified, [])
        self.assertEqual(reviewers, [])

    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.ack_nack._edit_approval_rule')
    def test_get_reviewers_zstream_maintainer_mr(self, ear, gccount):
        # This is a z-stream maintainer MR w/base approvals set to 0, in which case, we should
        # not set any required approvals for subsystems, just mark them optional
        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   labels:\n"
                       "     name: SomeSubsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "   reviewers:\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "     - name: User 3\n"
                       "       email: user3@redhat.com\n"
                       "       gluser: user3\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - redhat/\n")
        owners_parser = owners.Parser(owners_yaml)
        gl_instance = mock.Mock()
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        gl_project = mock.Mock()
        ainfo = mock.Mock()
        gccount.return_value = 20, 30
        ainfo.access_level = 40
        gl_project.members_all.get.return_value = ainfo
        gl_mergerequest = mock.Mock()
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        gl_mergerequest.author = {'id': 1, 'username': 'user1'}
        reviewers, required = ack_nack._get_reviewers(gl_instance, gl_project, gl_mergerequest,
                                                      ['redhat/README'], owners_parser)
        # user1@redhat.com is the merge request submitter and shouldn't be in the reviewer list.
        self.assertEqual(reviewers, [(frozenset(['user2', 'user3']), 'SomeSubsystem')])
        ear.assert_called_with(gl_instance, gl_mergerequest, 'SomeSubsystem', {'user2', 'user3'}, 0)

    def test_ensure_base_approval_rule(self):
        gl_project = mock.Mock()
        gl_project.name = "Testing"
        gl_mergerequest = mock.Mock()
        gl_mergerequest.state = "opened"
        gl_mergerequest.draft = False
        gl_mergerequest.iid = 666
        all_members_rule = mock.Mock()
        all_members_rule.approvals_required = 2
        all_members_rule.id = 1234
        gl_mergerequest.author = mock.MagicMock(id=3579)
        author_info = mock.Mock()
        author_info.access_level = 30
        gl_mergerequest.target_branch = 'main'
        gl_project.members_all.get.return_value = author_info
        gl_project.approvalrules.list.return_value = [all_members_rule]
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 2
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        # Everything is fine
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('DEBUG:cki.webhook.ack_nack:Testing MR 666 has '
                              '\'All Members\' rule set appropriately'))
        # Project has no All Members rule
        gl_project.approvalrules.list.return_value = []
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Project Testing has no '
                              '\'All Members\' approval rule'))
        # Project has required approvals set to 0
        all_members_rule.approvals_required = 0
        gl_project.approvalrules.list.return_value = [all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Project Testing isn\'t '
                              'requiring any reviewers for MRs'))
        # MR had approvals required set to 0 in main
        all_members_rule.approvals_required = 2
        gl_project.approvalrules.list.return_value = [all_members_rule]
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 (branch: main) had '
                              'approvals required set to 0'))
        # MR had approvals required set to 0 by non-maintainer
        gl_mergerequest.target_branch = 'z-stream'
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 (branch: z-stream) had '
                              'approvals required set to 0 by non-maintainer'))
        author_info.access_level = 40
        gl_project.members_all.get.return_value = author_info
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('DEBUG:cki.webhook.ack_nack:Testing MR 666 (branch: z-stream) has '
                              '\'All Members\' rule adjusted by maintainer'))
        # MR had no base approvals rule present
        gl_mergerequest.approval_rules.list.return_value = []
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._ensure_base_approval_rule(gl_project, gl_mergerequest)
            self.assertEqual(logs.output[-1],
                             ('WARNING:cki.webhook.ack_nack:Testing MR 666 had '
                              'no base approvals required'))

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_recheck_base_approval_rule(self, is_prod):
        gl_mergerequest = mock.Mock()
        mr_all_members_rule = mock.Mock()
        mr_all_members_rule.name = "All Members"
        mr_all_members_rule.approvals_required = 0
        gl_mergerequest.approval_rules.list.return_value = [mr_all_members_rule]
        gl_mergerequest.target_branch = "main"
        gl_mergerequest.draft = False

        # All Members has 0 required approvals
        error = {'body': '*ERROR*: MR has 0 required approvals'}
        ack_nack._recheck_base_approval_rule(gl_mergerequest)
        gl_mergerequest.discussions.create.assert_called_with(error)

        # All Members is missing
        mr_all_members_rule.name = "foo"
        error = {'body': '*ERROR*: MR has no "All Members" approval rule'}
        ack_nack._recheck_base_approval_rule(gl_mergerequest)
        gl_mergerequest.discussions.create.assert_called_with(error)

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    @mock.patch('webhook.ack_nack._reviewers_to_gl_user_ids')
    def test_edit_approval_rule(self, rtgluid, is_prod):
        gl_instance = mock.Mock()
        gl_mergerequest = mock.Mock()
        rtgluid.return_value = [105, 106]
        rule1 = mock.Mock()
        rule1.name = "yeehaw"
        gl_mergerequest.approval_rules.list.return_value = [rule1]

        subsystem = "george"
        reviewers = ["user1@redhat.com", "user2@redhat.com"]
        num_req = 0
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._edit_approval_rule(gl_instance, gl_mergerequest,
                                         subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Create rule for ss george, '
                              'with 0 required approval(s) from user(s) '
                              '[\'user1@redhat.com\', \'user2@redhat.com\']'))

        subsystem = "bob"
        reviewers = ["user1@redhat.com", "user2@redhat.com"]
        num_req = 2
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._edit_approval_rule(gl_instance, gl_mergerequest,
                                         subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:Create rule for ss bob, '
                              'with 2 required approval(s) from user(s) '
                              '[\'user1@redhat.com\', \'user2@redhat.com\']'))

        subsystem = "yeehaw"
        with self.assertLogs('cki.webhook.ack_nack', level='DEBUG') as logs:
            ack_nack._edit_approval_rule(gl_instance, gl_mergerequest,
                                         subsystem, reviewers, num_req)
            self.assertEqual(logs.output[-1],
                             ("INFO:cki.webhook.ack_nack:Approval rule for subsystem yeehaw "
                              "already exists"))

    def test_get_ark_config_mr_ccs(self):
        mr = mock.Mock()
        mr.description = ''
        self.assertEqual(ack_nack.get_ark_config_mr_ccs(mr), [])
        mr.description = 'Cool description.\nCc: user1 <user1@redhat.com>\n'
        self.assertEqual(ack_nack.get_ark_config_mr_ccs(mr), ['user1@redhat.com'])

    @mock.patch('webhook.ack_nack.get_ark_config_mr_ccs')
    def test_get_ark_reviewers(self, mocked_getarkconfig):
        mocked_mr = mock.Mock()
        ack_nack.get_ark_config_mr_ccs.return_value = []
        cc_reviewers = \
            ack_nack.get_ark_reviewers(defs.ARK_PROJECT_ID, mocked_mr,
                                       ['redhat/configs/hi', 'net/core/dev.c'])
        self.assertFalse(cc_reviewers)
        ack_nack.get_ark_config_mr_ccs.return_value = ['ark_user1@redhat.com']
        cc_reviewers = \
            ack_nack.get_ark_reviewers(defs.ARK_PROJECT_ID, mocked_mr,
                                       ['redhat/configs/hi'])
        self.assertEqual(cc_reviewers, [({'ark_user1@redhat.com'}, None)])

    def test_get_reviewers_from_approval_rules(self):
        R1EA = [{'id': 12345, 'name': 'Ram Eater', 'username': 'ram'},
                {'id': 56789, 'name': 'Mike Memory', 'username': 'mmem'},
                {'id': 8675309, 'name': 'Jenny Cache', 'username': 'jcache'}]
        R2EA = [{'id': 1234, 'name': 'Joe Developer', 'username': 'jdev'},
                {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bob'}]
        APPROVED_BY = [{'user': {'id': 12345, 'name': 'Ram Eater', 'username': 'ram'}},
                       {'user': {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bob'}}]
        MM_RESULT = ['mm', 1, 1, ['ram', 'mmem', 'jcache'], [12345, 56789, 8675309]]
        X86_RESULT = ['x86', 0, 1, ['jdev', 'bob'], [1234, 5678]]

        rule1 = mock.Mock()
        rule1.name = 'mm'
        rule1.eligible_approvers = R1EA
        rule1.approvals_required = 1

        rule2 = mock.Mock()
        rule2.name = 'x86'
        rule2.eligible_approvers = R2EA
        rule2.approvals_required = 0

        approvals = mock.Mock()
        approvals.approved = False
        approvals.approvals_required = 3
        approvals.approvals_left = 1
        approvals.approved_by = APPROVED_BY

        gl_mergerequest = mock.Mock()
        gl_mergerequest.approvals.get.return_value = approvals
        gl_mergerequest.approval_rules.list.return_value = [rule1, rule2]

        result = ack_nack.get_reviewers_from_approval_rules(gl_mergerequest)
        self.assertEqual(result, [MM_RESULT, X86_RESULT])

    @mock.patch('webhook.ack_nack._get_old_subsystems_from_labels')
    def test_get_approval_summary(self, old_subsystems):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_mergerequest = mock.Mock()

        user1 = {'id': 1, 'name': 'Joe Developer', 'username': 'joedev'}
        user2 = {'id': 2, 'name': 'Bob Reviewer', 'username': 'bobrev'}
        user3 = {'id': 3, 'name': 'Ram Eater', 'username': 'rameat'}
        user4 = {'id': 4, 'name': 'Mike Michaelson', 'username': 'mikesq'}
        user5 = {'id': 5, 'name': 'User 5', 'username': 'user5'}
        user6 = {'id': 6, 'name': 'User 6', 'username': 'user6'}
        user7 = {'id': 7, 'name': 'User 7', 'username': 'user7'}
        user8 = {'id': 8, 'name': 'User 8', 'username': 'user8'}
        approvers = [{'user': user1}, {'user': user2}]
        gl_instance.users.get = \
            lambda x: mock.Mock(username=f'user{x}')
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        gl_project.only_allow_merge_if_all_discussions_are_resolved = True
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': True}
        gl_mergerequest.resourcelabelevents.list.return_value = []
        subsys1_rule = mock.Mock()
        subsys1_rule.name = 'subsys1'
        subsys1_rule.eligible_approvers = [user1, user2]
        subsys1_rule.approvals_required = 1
        subsys2_rule = mock.Mock()
        subsys2_rule.name = 'subsys2'
        subsys2_rule.eligible_approvers = [user4, user5]
        subsys2_rule.approvals_required = 1
        subsys3_rule = mock.Mock()
        subsys3_rule.name = 'subsys3'
        subsys3_rule.eligible_approvers = [user6, user7]
        subsys3_rule.approvals_required = 1
        subsys4_rule = mock.Mock()
        subsys4_rule.name = 'subsys4'
        subsys4_rule.eligible_approvers = [user1, user8]
        subsys4_rule.approvals_required = 1
        gl_mergerequest.approval_rules.list.return_value = [subsys1_rule]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=2,
                                                               approved_by=[])
        old_subsystems.return_value = ([], [])
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], []),
                         ('NeedsReview', [], '\n\nRequires 2 more Approval(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=[{'user': user1}])

        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], []),
                         ('NeedsReview', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          '\nRequires 1 more Approval(s).'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], []),
                         ('OK', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nMerge Request has all necessary Approvals.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': False}
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], []),
                         ('Blocked', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nAll discussions must be resolved.'))
        gl_mergerequest.changes.return_value = {'blocking_discussions_resolved': True}

        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com',
                                                           'user2@redhat.com'], 'subsys1')], []),
                         ('OK', ['Acks::subsys1::OK'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nMerge Request has all necessary Approvals.'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com', 'user3@redhat.com'],
                                                          'subsys1')], []),
                         ('OK', ['Acks::subsys1::OK'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nMerge Request has all necessary Approvals.'))

        gl_mergerequest.approval_rules.list.return_value = [subsys1_rule, subsys2_rule]
        approvers = [{'user': user1}, {'user': user2}, {'user': user3}]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1'),
                                                         (['user4@redhat.com', 'user5@redhat.com'],
                                                          'subsys2')], []),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          ' - Ram Eater (rameat)\n'
                          '\nRequires 1 more Approval(s).'))

        gl_mergerequest.approval_rules.list.return_value = [subsys1_rule, subsys2_rule,
                                                            subsys3_rule, subsys4_rule]
        approvers = [{'user': user1}, {'user': user2}, {'user': user3}]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [(['user1@redhat.com', 'user2@redhat.com'],
                                                          'subsys1'),
                                                         (['user4@redhat.com', 'user5@redhat.com'],
                                                          'subsys2'),
                                                         (['user6@redhat.com', 'user7@redhat.com'],
                                                          'subsys3'),
                                                         (['user1@redhat.com', 'user8@redhat.com'],
                                                          'subsys4')], []),
                         ('NeedsReview', ['Acks::subsys1::OK', 'Acks::subsys2::NeedsReview',
                                          'Acks::subsys3::NeedsReview', 'Acks::subsys4::OK'],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Bob Reviewer (bobrev)\n'
                          ' - Ram Eater (rameat)\n'
                          '\nRequires 1 more Approval(s).'))

        # Approval Rules Reviewers based Required Approvals
        arr1 = ['RuleX', 1, 0, ['user1'], [1]]
        arr2 = ['RuleY', 0, 0, ['user2'], [2]]
        arr3 = ['RuleZ', 1, 1, ['user3'], [3]]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=[{'user': user2}])
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], [arr1]),
                         ('NeedsReview', [],
                          'Approved by:\n'
                          ' - Bob Reviewer (bobrev)\n'
                          '\nRequired Approvals:  \n'
                          ' - Approval Rule "RuleX" requires at least 1 ACK(s) '
                          '(0 given) from set (user1).  \n'))

        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=[{'user': user1}])
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], [arr2]),
                         ('NeedsReview', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          '\nOptional Approvals:  \n'
                          ' - Approval Rule "RuleY" requests optional ACK(s) from set (user2).  \n'
                          '\nRequires 1 more Approval(s).'))

        approvers = [{'user': user1}, {'user': user3}]
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=approvers)
        self.assertEqual(ack_nack._get_approval_summary(gl_instance, gl_project, gl_mergerequest,
                                                        [], [arr2, arr3]),
                         ('OK', [],
                          'Approved by:\n'
                          ' - Joe Developer (joedev)\n'
                          ' - Ram Eater (rameat)\n'
                          '\nOptional Approvals:  \n'
                          ' - Approval Rule "RuleY" requests optional ACK(s) from set (user2).  \n'
                          '\nSatisfied Approvals:  \n'
                          ' - Approval Rule "RuleZ" already has 1 ACK(s) (1 required).  \n'
                          '\nMerge Request has all necessary Approvals.'))

    @mock.patch('webhook.ack_nack.get_reviewers_from_approval_rules')
    @mock.patch('webhook.cdlib.set_dependencies_label')
    @mock.patch('webhook.cdlib.get_filtered_changed_files')
    @mock.patch('webhook.ack_nack._do_assign_reviewers')
    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=(True, False)))
    @mock.patch('webhook.common.update_webhook_comment', mock.Mock(return_value=None))
    def test_process_merge_request1(self, assign_reviewers, get_files, set_label, rfar):
        get_files.return_value = ['include/linux/netdevice.h']
        set_label.return_value = "Dependencies::OK"
        rfar.return_value = []

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = owners.Parser(owners_yaml)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 2))
        msgs.append(self.__create_note_body('Acked-by: Impersonated User <notuser3@redhat.com>',
                                            '2021-01-09T19:48:00.000Z', 3))

        ainfo = mock.Mock()
        ainfo.access_level = 30
        APPROVED_BY = [{'user': {'id': 1234, 'name': 'Joe Developer', 'username': 'joedev'}},
                       {'user': {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bobrev'}}]
        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}],
                                                'blocking_discussions_resolved': True}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.commits.return_value = \
            [mock.Mock(committed_date='2021-01-08T20:00:00.000Z')]
        gl_mergerequest.reviewers = []
        gl_mergerequest.resourcelabelevents.list.return_value = []
        gl_mergerequest.diffs.list.return_value = []
        gl_mergerequest.approval_rules.list.return_value = []
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=0,
                                                               approved_by=APPROVED_BY)
        gl_mergerequest.approvals_required = 2

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        gl_project.commits.get.return_value = mock.Mock(parent_ids=["1234"])
        gl_project.members_all.get.return_value = ainfo
        gl_project.archived = False

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(id=author_id)
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                           owners_parser, 'mocked')
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary: OK**\n'
                              '\nApproved by:\n'
                              ' - Joe Developer (joedev)\n'
                              ' - Bob Reviewer (bobrev)\n'
                              '\nMerge Request has all necessary Approvals.'))
            assign_reviewers.assert_called_with(gl_mergerequest, {'user1', 'user2'})

        set_label.return_value = 'Dependencies::deadbeefabcd'
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                           owners_parser, 'mocked')
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary: OK**\n'
                              '\nApproved by:\n'
                              ' - Joe Developer (joedev)\n'
                              ' - Bob Reviewer (bobrev)\n'
                              '\nMerge Request has all necessary Approvals.'))
            get_files.assert_called_once()

        set_label.return_value = 'Dependencies::deadbeefabcd'
        gl_project.id = defs.ARK_PROJECT_ID
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest,
                                           owners_parser, 'mocked')
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary: OK**\n'
                              '\nApproved by:\n'
                              ' - Joe Developer (joedev)\n'
                              ' - Bob Reviewer (bobrev)\n'
                              '\nMerge Request has all necessary Approvals.'))

    @mock.patch('webhook.common.get_commits_count')
    @mock.patch('webhook.cdlib.get_mr_pathlist')
    @mock.patch('webhook.common.extract_all_from_message')
    @mock.patch('webhook.common.extract_dependencies')
    @mock.patch('webhook.cdlib.mr_code_changed', mock.Mock(return_value=(True, True)))
    @mock.patch('webhook.common.update_webhook_comment', mock.Mock(return_value=None))
    def test_process_merge_request2(self, ext_deps, ext_bzs, get_paths, gccount):
        ext_deps.return_value = ([], '')
        ext_bzs.return_value = ([], [], [])
        get_paths.return_value = []
        gccount.return_value = 20, 30
        ainfo = mock.Mock()
        ainfo.access_level = 30

        owners_yaml = ("subsystems:\n"
                       " - subsystem: Some Subsystem\n"
                       "   requiredApproval: true\n"
                       "   maintainers:\n"
                       "     - name: User 1\n"
                       "       email: user1@redhat.com\n"
                       "       gluser: user1\n"
                       "     - name: User 2\n"
                       "       email: user2@redhat.com\n"
                       "       gluser: user2\n"
                       "   paths:\n"
                       "       includes:\n"
                       "          - kernel/\n")
        owners_parser = owners.Parser(owners_yaml)

        msgs = []
        msgs.append(self.__create_note_body('Some comment', '2021-01-09T19:40:00.000Z', 1))
        msgs.append(self.__create_note_body('Another comment', '2021-01-09T19:42:00.000Z', 1))
        msgs.append(self.__create_note_body('Acked-by: User 1 <user1@redhat.com>',
                                            '2021-01-09T19:44:00.000Z', 1))
        msgs.append(self.__create_note_body('Yet another comment', '2021-01-09T19:46:00.000Z', 2))
        # Code is pushed up based on committed_date below; previous ack is no longer valid
        msgs.append(self.__create_note_body('Acked-by: User 2 <user2@redhat.com>',
                                            '2021-01-10T07:00:00.000Z', 2))

        APPROVED_BY = [{'user': {'id': 1234, 'name': 'User 1', 'username': 'user1'}}]
        gl_mergerequest = mock.Mock()
        gl_mergerequest.author = {'id': 4, 'username': 'someuser'}
        gl_mergerequest.notes.list.return_value = msgs
        gl_mergerequest.changes.return_value = {'changes': [{'new_path': 'kernel/fork.c'}],
                                                'blocking_discussions_resolved': True}
        gl_mergerequest.labels = ['Dependencies::OK']
        gl_mergerequest.reviewers = []
        gl_mergerequest.resourcelabelevents.list.return_value = []
        diff = mock.MagicMock(id=1234, created_at='2021-01-10T03:00:00.000Z')
        gl_mergerequest.diffs.list.return_value = [diff]
        gl_mergerequest.approval_rules.list.return_value = []
        gl_mergerequest.approvals.get.return_value = mock.Mock(approvals_required=2,
                                                               approvals_left=1,
                                                               approved_by=APPROVED_BY)
        gl_mergerequest.approvals_required = 2

        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.labels.list.return_value = []
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        gl_project.members_all.get.return_value = ainfo
        gl_project.archived = False

        gl_instance = mock.Mock()
        gl_instance.users.get = \
            lambda author_id: mock.Mock(id=author_id)
        gl_instance.users.list = \
            lambda search: [mock.Mock(username=search.split('@')[0])]
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]

        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack.process_merge_request(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked')
            self.assertEqual(logs.output[-1],
                             ('INFO:cki.webhook.ack_nack:**ACK/NACK Summary: NeedsReview**\n'
                              '\nApproved by:\n'
                              ' - User 1 (user1)\n'
                              '\nRequires 1 more Approval(s).'))

    def __create_note_body(self, body, timestamp, author_id):
        ret = mock.Mock()
        ret.body = body
        ret.updated_at = timestamp
        ret.author = {'id': author_id, 'username': f'user{author_id}'}
        return ret

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message0(self, mocked_process_mr, mock_gl):
        self._test_note("request-ack-nack-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message1(self, mocked_process_mr, mock_gl):
        self._test_note("request-evaluation", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called_once()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message5(self, mocked_process_mr, mock_gl):
        self._test_note("Some comment", mocked_process_mr, mock_gl)
        mocked_process_mr.assert_called()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message6(self, mocked_process_mr, mock_gl):
        # blocking_discussions_resolved is False and MR has Acks::OK
        self._test_note("Some comment", mocked_process_mr, mock_gl, False, ['Acks::OK'])
        mocked_process_mr.assert_called()

    @mock.patch('webhook.common.get_owners_parser', mock.Mock())
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_message7(self, mocked_process_mr, mock_gl):
        # blocking_discussions_resolved is True and MR has Acks::Blocked
        self._test_note("Some comment", mocked_process_mr, mock_gl, True, ['Acks::Blocked'])
        mocked_process_mr.assert_called()

    def _test_note(self, note_text, mocked_process_mr, mock_gl, discussions_resolved=True,
                   labels=None):
        resolv = discussions_resolved
        if not labels:
            labels = []
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_project.mergerequests.get.return_value = mock.Mock(author={'id': 2, 'username': 'user1'},
                                                              blocking_discussions_resolved=resolv,
                                                              labels=labels)
        gl_instance.projects.get.return_value = gl_project
        gl_instance.users.get = lambda user_id: mock.Mock(username=f'user{user_id}')
        mock_gl.return_value.__enter__.return_value = gl_instance

        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = note_text
        headers = {'message-type': 'gitlab'}
        args = common.get_arg_parser('ACK_NACK').parse_args('')
        self.assertTrue(common.process_message(args, ack_nack.WEBHOOKS, "ROUTING_KEY", payload,
                                               headers, owners_parser='mocked',
                                               rhkernel_src='mocked'))

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_other_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Change a label not related to ack/nack
        msg.payload['changes']['labels']['previous'] = [{'title': 'Acks::OK'},
                                                        {'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'}]

        owners_parser = mock.Mock()
        self.assertTrue(ack_nack.process_mr_webhook(gl_instance, msg, owners_parser, 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked')

    @mock.patch('webhook.ack_nack.process_merge_request')
    def test_process_mr_webhook_ack_nack_label(self, mock_process_mr):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_instance.projects.get.return_value = gl_project
        gl_mergerequest = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        msg = mock.Mock()
        msg.payload = self._create_merge_payload('update')

        # Add ack_nack label
        msg.payload['changes']['labels']['previous'] = [{'title': 'somethingelse::OK'}]
        msg.payload['changes']['labels']['current'] = [{'title': 'Acks::OK'},
                                                       {'title': 'somethingelse::OK'}]

        owners_parser = mock.Mock()
        self.assertTrue(ack_nack.process_mr_webhook(gl_instance, msg, owners_parser, 'mocked'))
        mock_process_mr.assert_called_with(gl_instance, gl_project, gl_mergerequest, owners_parser,
                                           'mocked')

    def _create_merge_payload(self, action):
        return {'object_kind': 'merge_request',
                'user': {'id': 1, 'username': 'u1', 'name': 'User 1', 'email': 'user1@redhat.com'},
                'project': {'id': 1,
                            'web_url': 'https://web.url/g/p',
                            'path_with_namespace': 'g/p'
                            },
                'object_attributes': {'iid': 2, 'action': action,
                                      'last_commit': {'timestamp': '2021-01-08T20:00:00.000Z'}},
                'changes': {'labels': {'previous': [], 'current': []}}}

    def test_filter_stale_reviewers(self):
        mock_mr = mock.Mock()
        stale_reviewers = ['someone', 'nobody']
        note1 = mock.Mock(id="1234", author={'username': 'cki-kwf-bot'},
                          body='requested review from @someone')
        note2 = mock.Mock(id="1235", author={'username': 'user35'},
                          body='requested review from @nobody')
        notes = mock.MagicMock()
        notes.list.return_value = [note1, note2]
        mock_mr.notes = notes
        output = ack_nack._filter_stale_reviewers(mock_mr, stale_reviewers)
        self.assertEqual(output, ['someone'])
        stale_reviewers = ['nobody', 'anotherone']
        output = ack_nack._filter_stale_reviewers(mock_mr, stale_reviewers)
        self.assertEqual(output, [])

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.common.mr_is_closed', mock.Mock(return_value=False))
    def test_assign_reviewers(self):
        mock_instance = mock.Mock()
        mock_proj = mock.Mock()
        mock_mr = mock.MagicMock()
        mock_mr.iid = 222
        mock_mr.author = {'username': 'joeschmoe'}
        APPROVED_BY = [{'user': {'id': 5678, 'name': 'Bob Reviewer', 'username': 'bob'}}]
        approvals = mock.Mock()
        approvals.approved_by = APPROVED_BY
        approvals.approvals_required = 2
        mock_mr.approvals.get.return_value = approvals
        reviewers = [({'nobody', 'someone'}, 'subsystem')]
        assign = {'body': '/assign_reviewer @nobody\n/assign_reviewer @someone'}
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._assign_reviewers(mock_instance, mock_proj, mock_mr, reviewers)
            self.assertIn("Assigning users ['nobody', 'someone'] to MR 222", logs.output[-1])
            mock_mr.notes.create.assert_called_with(assign)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.common.mr_is_closed', mock.Mock(return_value=False))
    def test_unassign_reviewers(self):
        mock_mr = mock.Mock()
        mock_mr.iid = 2
        users = ['someone', 'nobody']
        unassign = {'body': '/unassign_reviewer someone nobody'}
        with self.assertLogs('cki.webhook.ack_nack', level='INFO') as logs:
            ack_nack._unassign_reviewers(mock_mr, users)
            self.assertIn("Unassigning users ['someone', 'nobody'] to MR 2", logs.output[-1])
            mock_mr.notes.create.assert_called_with(unassign)

    def test_get_existing_reviewers(self):
        mock_mr = mock.Mock()
        rev1 = {'id': 1, 'name': 'Some One', 'username': 'someone'}
        rev2 = {'id': 2, 'name': 'No Body', 'username': 'nobody'}
        rev3 = {'id': 3, 'name': 'Red Hatter', 'username': 'redhatter'}
        mock_mr.reviewers = [rev1, rev2, rev3]
        output = ack_nack._get_existing_reviewers(mock_mr)
        self.assertEqual(output, ['someone', 'nobody', 'redhatter'])

    def test_emails_to_gl_user_names(self):
        mock_instance = mock.Mock()
        user1 = mock.Mock(id="1", name="Some One", username="someone")
        user2 = mock.Mock(id="2", name="No Body", username="nobody")
        user3 = mock.Mock(id="3", name="Red Hatter", username="redhatter")
        reviewers = ['someone@redhat.com', 'nobody@redhat.com', 'redhatter@redhat.com']
        mock_instance.users.list.return_value = [user1, user2, user3]
        output = ack_nack._emails_to_gl_user_names(mock_instance, reviewers)
        self.assertEqual(output, {'someone', 'nobody', 'redhatter'})

    @mock.patch('webhook.ack_nack._emails_to_gl_user_names')
    def test_get_stale_reviewers(self, user_names):
        old = ['user3']
        all_reviewers = [(['user1@redhat.com', 'user2@redhat.com'], 'subsys1')]
        user_names.return_value = ['user1', 'user2']
        output = ack_nack._get_stale_reviewers(old, all_reviewers)
        self.assertEqual(output, ['user3'])

    def test_get_old_subsystems_from_labels(self):
        mock_mr = mock.Mock()
        mock_mr.labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Something::whatever']
        (subsys, labels) = ack_nack._get_old_subsystems_from_labels(mock_mr)
        self.assertEqual(subsys, ['net', 'mm'])
        self.assertEqual(labels, ['Acks::net::NeedsReview', 'Acks::mm::OK'])

    def test_get_stale_labels(self):
        old_subsystems = ['net', 'mm', 'foobar']
        old_labels = ['Acks::net::NeedsReview', 'Acks::mm::OK', 'Acks::foobar::NACKed']
        subsys_scoped_labels = {'net': 'NeedsReview', 'mm': 'OK'}
        output = ack_nack._get_stale_labels(old_subsystems, old_labels, subsys_scoped_labels)
        self.assertEqual(output, ['Acks::foobar::NACKed'])
