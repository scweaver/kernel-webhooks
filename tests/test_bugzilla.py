"""Webhook interaction tests."""
import copy
import json
import os
from unittest import TestCase
from unittest import mock

from cki_lib import session

from webhook import bugzilla
from webhook import common
from webhook import defs


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
class TestBugzilla(TestCase):
    """ Test Webhook class."""

    PAYLOAD_MERGE = {'object_kind': 'merge_request',
                     'project': {'id': 1,
                                 'archived': False,
                                 'web_url': 'https://web.url/g/p',
                                 'path_with_namespace': 'g/p'
                                 },
                     'object_attributes': {'target_branch': 'main', 'iid': 2},
                     'state': 'opened'
                     }

    PAYLOAD_NOTE = {'object_kind': 'note',
                    'project': {'id': 1,
                                'archived': False,
                                'web_url': 'https://web.url/g/p',
                                'path_with_namespace': 'g/p'
                                },
                    'object_attributes': {'note': 'comment',
                                          'noteable_type': 'MergeRequest'},
                    'merge_request': {'target_branch': 'main', 'iid': 2},
                    'state': 'opened'
                    }

    BZ_RESULTS = [{'id': 1234567,
                   'external_bugs': [{'ext_bz_bug_id': '111222',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'redhat/rhel/8.y/kernel/-/merge_requests/10',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 8675309,
                   'external_bugs': [{'ext_bz_bug_id': '11223344',
                                      'type': {'description': 'Linux Kernel',
                                               'url': 'https://bugzilla.kernel.org'
                                               }
                                      },
                                     {'ext_bz_bug_id': 'group/project/-/merge_requests/321',
                                      'type': {'description': 'Gitlab',
                                               'url': 'https://gitlab.com/'
                                               }
                                      }]
                   },
                  {'id': 66442200,
                   'external_bugs': []
                   }]

    def test_bug_is_verified(self):

        # we have an unverified bug.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8', component='kernel',
                            cf_verified=[])
            self.assertFalse(bugzilla.bug_is_verified(bug))
            self.assertIn('Bug 12345678 is not Verified.', logs.output[-1])

        # getbug() returns a closed bug for kernel.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8', component='kernel',
                            cf_verified=[])
            self.assertFalse(bugzilla.bug_is_verified(bug))
            self.assertIn('Bug 12345678 is not Verified.', logs.output[-1])

        # getbug() returns a verified bug for kernel.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8', component='kernel',
                            cf_verified=['Tested'], status=defs.BZ_STATE_MODIFIED)
            self.assertTrue(bugzilla.bug_is_verified(bug))
            self.assertIn('Bug 12345678 is Verified.', logs.output[-1])

        # getbug() returns a verified bug for kernel-rt.
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            bug = mock.Mock(id=12345678, product='Red Hat Enterprise Linux 8',
                            component='kernel-rt', cf_verified=['Tested'],
                            status=defs.BZ_STATE_POST)
            self.assertTrue(bugzilla.bug_is_verified(bug))
            self.assertIn('Bug 12345678 is Verified.', logs.output[-1])

    def test_check_bzs_for_cve(self):
        bug_ids = '12345678'
        bzcon = mock.Mock()
        cve_fields = bugzilla.CVE_FIELDS

        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            # getbugs() returns a bug with a cve.
            cve_no = ["CVE-2021-21654"]
            bzcon.getbugs.return_value = [mock.Mock(id=bug_ids,
                                                    short_desc=f'{cve_no[0]}: Red Hat Linux')]
            cve = bugzilla.check_bzs_for_cve(bug_ids, bzcon)

            bzcon.getbugs.assert_called_once_with(bug_ids, include_fields=cve_fields)
            self.assertEqual(cve, {bug_ids: cve_no})
            self.assertIn(f"BZ: {bug_ids} found CVEs: {cve_no}", logs.output[-1])

            # getbugs() returns a bug with multiple cve.
            cve_no = ["CVE-2021-21654", "CVE-2022-21654"]
            bzcon.getbugs.return_value = [mock.Mock(id=bug_ids,
                                                    short_desc=f'{" ,".join(cve_no)}: Red Hat')]
            cve = bugzilla.check_bzs_for_cve(bug_ids, bzcon)

            bzcon.getbugs.assert_called_with(bug_ids, include_fields=cve_fields)
            self.assertEqual(cve, {bug_ids: cve_no})
            self.assertIn(f"BZ: {bug_ids} found CVEs: {cve_no}", logs.output[-1])

            # getbugs() returns a mulitple bugs with cves.
            bug_ids = ['12345678', '987654321']
            cve_no1 = ["CVE-2021-21654"]
            cve_no2 = ["CVE-2022-21654"]
            bzcon.getbugs.return_value = [mock.Mock(id=bug_ids[0], short_desc=f'{cve_no1[0]}: RH'),
                                          mock.Mock(id=bug_ids[1], short_desc=f'{cve_no2[0]}: RH')]
            cve = bugzilla.check_bzs_for_cve(bug_ids, bzcon)

            bzcon.getbugs.assert_called_with(bug_ids, include_fields=cve_fields)
            self.assertEqual(cve, {bug_ids[0]: cve_no1, bug_ids[1]: cve_no2})
            self.assertIn(f"BZ: {bug_ids[0]} found CVEs: {cve_no1}", logs.output[-2])
            self.assertIn(f"BZ: {bug_ids[1]} found CVEs: {cve_no2}", logs.output[-1])

            # getbugs() returns a bug without a cve
            cve_no = ""
            bzcon.getbugs.return_value = [mock.Mock(id=bug_ids, short_desc=f'{cve_no}: Red Hat')]
            cve = bugzilla.check_bzs_for_cve(bug_ids, bzcon)
            bzcon.getbugs.assert_called_with(bug_ids, include_fields=cve_fields)
            self.assertEqual(cve, None)
            self.assertIn(f'No CVEs found in BZ: {bug_ids}.', logs.output[-1])

        # getbugs() failure.
        with self.assertLogs('cki.webhook.bugzilla', level='ERROR') as logs:
            bzcon.getbugs.return_value = None
            cve = bugzilla.check_bzs_for_cve(bug_ids, bzcon)
            self.assertEqual(cve, None)
            bzcon.getbugs.assert_called_with(bug_ids, include_fields=cve_fields)
            self.assertIn(f'getbugs() did not return a useful result for BZ(s) {bug_ids}.',
                          logs.output[-1])

    # pylint: disable=no-self-use
    @mock.patch('cki_lib.messagequeue.pika.BlockingConnection')
    @mock.patch('webhook.common.process_message')
    @mock.patch.dict(os.environ, {'BUGZILLA_ROUTING_KEYS': 'mocked', 'BUGZILLA_QUEUE': 'mocked'})
    def test_entrypoint(self, mocked_process, mocked_connection):
        """Check basic queue consumption."""

        # setup dummy consume data
        payload = {'foo': 'bar'}
        properties = mock.Mock()
        headers = {'header-key': 'value'}
        setattr(properties, 'headers', headers)
        consume_value = [(mock.Mock(), properties, json.dumps(payload))]
        mocked_connection().channel().consume.return_value = consume_value

        bugzilla.main([])

        mocked_process.assert_called()

    @mock.patch('gitlab.Gitlab')
    def test_unsupported_object_kind(self, mocked_gitlab):
        """Check handling an unsupported object kind."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload.update({'object_kind': 'foo'})

        self._test_payload(mocked_gitlab, False, payload, False)

    def test_validate_internal_bz(self):
        """Check for expected return value."""
        commits = {"deadb": ['redhat/Makefile']}
        self.assertTrue(bugzilla.validate_internal_bz(commits))
        commits = {"deadb": ['redhat/Makefile', '.gitlab-ci.yml']}
        self.assertTrue(bugzilla.validate_internal_bz(commits))
        commits = {"deadb": ['.gitlab/CODEOWNERS', '.gitlab-ci.yml']}
        self.assertTrue(bugzilla.validate_internal_bz(commits))
        commits['deadb'] = ['info/owners.yaml']
        self.assertTrue(bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h']
        self.assertFalse(bugzilla.validate_internal_bz(commits))
        commits['abcde'] = ['net/core/dev.c', 'include/net/netdevice.h', 'redhat/configs/']
        self.assertFalse(bugzilla.validate_internal_bz(commits))

    def test_build_review_lists(self):
        """Test out the build_review_lists function."""
        commit = mock.Mock(id="123456abcdef", message="This is a commit message w/no BZ\n")
        commit.diff = mock.Mock(return_value=[{'new_path': 'include/linux/netdevice.h'}])
        review_lists = []
        bug = "1987253"
        cve = "CVE-1214-32425"
        output = bugzilla.build_review_lists(commit, review_lists, bug)
        self.assertEqual(output, {'commits': {'123456abcdef': []}, 'cves': []})
        output = bugzilla.build_review_lists(commit, review_lists, bug, [cve])
        self.assertEqual(output, {'commits': {'123456abcdef': []}, 'cves': [cve]})
        output = bugzilla.build_review_lists(None, review_lists, bug)
        self.assertEqual(output, {'commits': {defs.BZ_IN_DESCRIPTION_ONLY: []}, 'cves': []})

    def test_get_report_table(self):
        """Ensure the value of the returned mr_approved is correct."""
        bug1 = {'validation': {'approved': bugzilla.BZState.READY_FOR_QA, 'notes': '', 'logs': ''},
                'commits': [], 'cves': []}
        bug2 = {'validation': {'approved': bugzilla.BZState.READY_FOR_MERGE, 'notes': '',
                               'logs': ''}, 'commits': [], 'cves': []}
        bug3 = {'validation': {'approved': bugzilla.BZState.NOT_READY, 'notes': '', 'logs': ''},
                'commits': [], 'cves': []}
        bug4 = {'validation': {'approved': bugzilla.BZState.CLOSED, 'notes': '', 'logs': ''},
                'commits': [], 'cves': []}
        bug5 = {'validation': {'approved': bugzilla.BZState.NOT_READY, 'notes': '', 'logs': ''},
                'commits': [], 'cves': ['CVE-1234-12345']}
        bug6 = {'validation': {'approved': bugzilla.BZState.INVALID, 'notes': '', 'logs': ''},
                'commits': [], 'cves': []}

        # mr_approved should reflect lowest BZState value in the reviewed_items list.
        reviewed_items = {1234567: bug1, 2345678: bug2}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.READY_FOR_QA)
        reviewed_items = {1234567: bug1, 2345678: bug2, 3456753: bug3}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.NOT_READY)
        reviewed_items = {1234567: bug2, 2345678: bug2, 3456753: bug2}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.READY_FOR_MERGE)
        reviewed_items = {1234567: bug4}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.CLOSED)
        reviewed_items = {1234567: bug5}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.NOT_READY)
        reviewed_items = {1234567: bug6}
        (table, result) = bugzilla.get_report_table(reviewed_items)
        self.assertEqual(result, bugzilla.BZState.INVALID)

    def test_print_gitlab_report(self):
        """Test for various bug issue reporting scenarios."""
        mreq = mock.Mock()
        mreq.target_branch = 'main'
        mock_target = mreq.target_branch
        table = []
        approval = False
        notes = ["This report is an absolute disaster."]
        output = bugzilla.print_gitlab_report(mreq, notes, mock_target, table, approval)
        self.assertIn("**Bugzilla Readiness Error(s)!**", output)
        self.assertIn("There were no valid bugs in the MR description or in any commits", output)
        table = [['abcd', [defs.BZ_IN_DESCRIPTION_ONLY], 1, [], ['xyz'], ""]]
        output = bugzilla.print_gitlab_report(mreq, notes, mock_target, table, approval)
        self.assertIn("**Bugzilla Readiness Error(s)!**", output)
        self.assertIn("There are bugs in the MR description not in any commits", output)
        self.assertIn("Merge Request **fails** bugzilla validation", output)
        approval = True
        table = [['abcd', ['1234'], 1, ['CVE-2020-111111'], ['xyz'], ""]]
        output = bugzilla.print_gitlab_report(mreq, notes, mock_target, table, approval)
        self.assertIn("**Bugzilla Readiness Report**", output)
        self.assertIn("Merge Request **passes** bugzilla validation", output)

    @mock.patch('gitlab.Gitlab')
    def test_merge_request(self, mocked_gitlab):
        """Check handling of a merge request."""
        payload = copy.deepcopy(self.PAYLOAD_MERGE)
        payload['object_attributes']['target_branch'] = 'main'
        payload['object_attributes']['action'] = 'update'
        payload['changes'] = 'merge_status'
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_note(self, mocked_gitlab):
        """Check handling of a note."""
        self._test_payload(mocked_gitlab, True, self.PAYLOAD_NOTE, False)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation1(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    @mock.patch('gitlab.Gitlab')
    def test_bz_re_evaluation2(self, mocked_gitlab):
        """Check handling of bz re-evaluation."""
        payload = copy.deepcopy(self.PAYLOAD_NOTE)
        payload["object_attributes"]["note"] = "request-bz-evaluation"
        self._test_payload(mocked_gitlab, True, payload, True)

    def _test_payload(self, mocked_gitlab, result, payload, assert_gitlab_called):
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)
        with mock.patch('cki_lib.misc.is_production', return_value=False):
            self._test(mocked_gitlab, result, payload, assert_gitlab_called)

    # pylint: disable=too-many-arguments,too-many-locals
    @mock.patch('webhook.common.add_merge_request_to_milestone')
    @mock.patch('webhook.cdlib.is_rhdocs_commit')
    @mock.patch('webhook.common.try_bugzilla_conn')
    @mock.patch.dict(os.environ, {'COMMIT_POLICY_URL': 'https://example.com/rules.html'})
    def _test(self, mocked_gitlab, result, payload, assert_gitlab_called, mock_try_bugzilla, rhdoc,
              add_to_ms):
        # setup dummy bugzilla data
        bzcon = mock.Mock()
        bzcon.getbug.return_value = mock.Mock(id='1234', short_desc='Red Hat Linux')
        bzcon.getbugs.return_value = [mock.Mock(id='1234', short_desc='Red Hat Linux')]
        mock_try_bugzilla.return_value = bzcon
        rhdoc.return_value = False

        # setup dummy gitlab data
        instance = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        instance.groups.list.return_value = [rh_group]
        project = mock.Mock(name_with_namespace="foo.bar",
                            namespace={'name': '8.y'})
        project.name = 'rhel-8'
        project.archived = False
        project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")
        target = "main"
        if payload['object_kind'] == 'merge_request':
            target = payload['object_attributes']['target_branch']
        if payload['object_kind'] == 'note':
            target = payload['merge_request']['target_branch']
        merge_request = mock.MagicMock(target_branch=target)
        c1 = mock.Mock(id="1234",
                       message="1\n"
                       "Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1234",
                       parent_ids=["abcd"])
        c2 = mock.Mock(id="4567",
                       message="2\nBugzilla: http://bugzilla.test?id=45678\n"
                       "Bugzilla: https://bugzilla.redhat.com/4567\n"
                       "Depends: http://bugzilla.redhat.com/1234",
                       parent_ids=["1234"])
        c3 = mock.Mock(id="890a",
                       message="3\nBugzilla: INTERNAL",
                       parent_ids=["face"])
        c3.diff = mock.Mock(return_value=[{'new_path': 'redhat/Makefile'}])
        c4 = mock.Mock(id="face",
                       message="This is a merge",
                       parent_ids=["abcd", "4567"])
        c5 = mock.Mock(id="deadbeef",
                       message="5\nBugzilla: 565472, 7784378\n"
                       "Nothing to see here\n"
                       "Bugzilla: http://bugzilla.redhat.com/1234567\n"
                       "Bugzilla: https://bugzilla.redhat.com/7654321\n"
                       "Bugzilla: http://bugzilla.redhat.com/1989898",
                       parent_ids=["890a"])
        c6 = mock.Mock(id="1800555",
                       message="6\nBugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1800555",
                       parent_ids=["deadbeef"])
        c7 = mock.Mock(id="e404",
                       message="7\n"
                       "https://bugzilla.redhat.com/show_bug.cgi?id=9983023",
                       parent_ids=["1800555"])
        c8 = mock.Mock(id="abcd",
                       message="8\n"
                       "Depends: "
                       "https://bugzilla.redhat.com/show_bug.cgi?id=7654321\n",
                       parent_ids=["1800555"])
        c9 = mock.Mock(id="buggymacbug",
                       message="9\n"
                       "CVE: CVE-2021-99999\n"
                       "CVE: CVE-1900-1234567",
                       parent_ids=["1800555"])
        c10 = mock.Mock(id="1800556",
                        message="10\nBugzilla:  http://bugzilla.redhat.com/1800556",
                        parent_ids=["feedbeef"])
        c11 = mock.Mock(id="45678fad",
                        message="11\nBugzilla: http://bugzilla.test?id=45678fad\n"
                        "Depends:   http://bugzilla.redhat.com/12349999",
                        parent_ids=["1234abcd"])
        mocked_gitlab().__enter__().projects.get.return_value = project
        project.mergerequests.get.return_value = merge_request
        project.labels.list.return_value = []
        merge_request.iid = 2
        merge_request.labels = []
        merge_request.commits.return_value = [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11]
        merge_request.pipeline = {'status': 'success'}
        merge_request.description = ("Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1234\n"
                                     "Bugzilla: INTERNAL")
        branch = mock.Mock()
        branch.configure_mock(name="8.2")
        project.branches.list.return_value = [branch]
        project.commits = {"1234": c1, "4567": c2, "890a": c3, "face": c4, "deadbeef": c5,
                           "1800555": c6, "e404": c7, "abcd": c8, "buggymacbug": c9,
                           "1800556": c10, "45678fad": c11}
        self.assertEqual(common.extract_bzs(c1.message), (["1234"], ''))
        self.assertEqual(common.extract_dependencies(project, c1.message), ([], ''))
        self.assertEqual(common.extract_bzs(c2.message), (["4567"], ''))
        self.assertEqual(common.extract_dependencies(project, c2.message), (["1234"], ''))
        self.assertEqual(common.extract_bzs(c3.message), (["INTERNAL"], ''))
        self.assertEqual(common.extract_bzs(c5.message), (["1234567", "7654321", "1989898"], ''))
        self.assertEqual(common.extract_bzs(c6.message), (["1800555"], ''))
        self.assertEqual(common.extract_bzs(c7.message), ([], ''))
        self.assertEqual(common.extract_dependencies(project, c8.message), (["7654321"], ''))
        self.assertEqual(common.extract_cves(c9.message), ["CVE-2021-99999", "CVE-1900-1234567"])
        merge_request.description = ("Bugzilla:  http://bugzilla.redhat.com/1800556\n")
        bugs, errors = common.extract_bzs(c10.message)
        self.assertEqual(bugs, [])
        self.assertIn("`Expected:` `Bugzilla: http://bugzilla.redhat.com/1800556`", errors)
        self.assertIn("`Found   :` `Bugzilla:  http://bugzilla.redhat.com/1800556`", errors)
        bugs, errors = common.extract_dependencies(project, c11.message)
        self.assertEqual(bugs, ["12349999"])
        self.assertIn("`Expected:` `Depends: http://bugzilla.redhat.com/12349999`", errors)
        self.assertIn("`Found   :` `Depends:   http://bugzilla.redhat.com/12349999`", errors)

        # setup dummy post results data
        presult = mock.Mock()
        presult.json.return_value = {
            "error": "*** No approved issue IDs referenced in log message or"
                     " changelog for HEAD\n*** Unapproved issue IDs referenced"
                     " in log message or changelog for HEAD\n*** Commit HEAD"
                     " denied\n*** Current checkin policy requires:\n"
                     "    release == +\n"
                     "*** See https://rules.doc/doc for more information",
            "result": "fail",
            "logs": "*** Checking commit HEAD\n*** Resolves:\n***   "
                    "Unapproved:\n***     rhbz#1234 (release?, qa_ack?, "
                    "devel_ack?, mirror+)\n"
        }

        with mock.patch('cki_lib.session.get_session') as mock_session:
            mock_session.side_effect = session.get_session()
            mock_session.post.return_value = presult
            headers = {'message-type': 'gitlab'}
            args = common.get_arg_parser('BUGZILLA').parse_args('')
            args.disable_user_check = True
            mock_branch = mock.Mock(distgit_ref=target)
            with mock.patch('webhook.bugzilla.get_target_branch', return_value=mock_branch):
                return_value = \
                    common.process_message(args, bugzilla.WEBHOOKS, 'dummy', payload, headers)

        if result:
            mocked_gitlab.assert_called_with(
                'https://web.url', private_token='TOKEN',
                session=mock.ANY)

            self.assertTrue(return_value)
            if assert_gitlab_called:
                mocked_gitlab().__enter__().projects.get.assert_called_with('g/p')
                project.mergerequests.get.assert_called_with(2)
            else:
                mocked_gitlab().__enter__().projects.get.assert_not_called()
                project.mergerequests.get.assert_not_called()
        elif not target:
            self.assertTrue(return_value)
        else:
            self.assertFalse(return_value)

    @mock.patch('webhook.common.parse_mr_url')
    @mock.patch('webhook.bugzilla.run_bz_validation')
    def test_bz_process_umb(self, mock_run_bz, mock_parse):
        mock_instance = mock.Mock()
        url = 'https://gitlab.com/group/project/-/merge_requests/123'
        headers = {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE,
                   'mrpath': 'group/project!123'
                   }

        mock_parse.return_value = (mock_instance, 'project', 'merge_request')

        bugzilla.bz_process_umb(headers)
        mock_parse.assert_called_with(url)
        mock_instance.auth.assert_called_once()
        mock_run_bz.assert_called_with(mock_instance, 'project', 'merge_request', False)

    @mock.patch('webhook.common.update_webhook_comment')
    @mock.patch('webhook.bugzilla.print_gitlab_report')
    def test_bz_post_report(self, gl_report, add_comment):
        gl_report.return_value = "Here's your report, good sir\n"
        inst = mock.Mock()
        inst.user.username = "shadowman"
        mreq = mock.Mock()
        bugzilla.bz_post_report(inst, mreq, "mocked", "", "rhel-17", "", True)
        add_comment.assert_called_once()
        add_comment.assert_called_with(mreq, "shadowman", "Bugzilla Readiness",
                                       "Here's your report, good sir\n")

    def test_set_unfetched_bug_properties(self):
        valid_mr_bug = False
        commits = {'DescOnly': []}
        bz_properties = {'approved': bugzilla.BZState.NOT_READY, 'notes': [], 'logs': ''}
        # bugzilla found in a commit, but not listed in MR description
        bugzilla.set_unfetched_bug_properties(valid_mr_bug, commits, bz_properties)
        self.assertEqual(bz_properties['approved'], bugzilla.BZState.NOT_READY)
        self.assertEqual(bz_properties['logs'], 'Not found in MR description')

        valid_mr_bug = True
        # bug is in MR description, but not in any commits
        bugzilla.set_unfetched_bug_properties(valid_mr_bug, commits, bz_properties)
        self.assertEqual(bz_properties['approved'], bugzilla.BZState.NOT_READY)
        self.assertEqual(bz_properties['logs'], 'Not in any commits')

        commits = {'abcd': ['foo']}
        # bug number isn't valid at all (does not exist -- typo'd an extra digit?)
        bugzilla.set_unfetched_bug_properties(valid_mr_bug, commits, bz_properties)
        self.assertEqual(bz_properties['approved'], bugzilla.BZState.INVALID)
        self.assertEqual(bz_properties['logs'], 'Not a valid bug number')

    def test_set_invalid_bug_info(self):
        notes = []
        bz_properties = {'approved': bugzilla.BZState.NOT_READY, 'notes': [], 'logs': ''}
        bugzilla.set_invalid_bug_info(notes, bz_properties)
        expected_note = 'The bugzilla associated with these commits is not approved at this time.'
        self.assertEqual(notes, [expected_note])
        self.assertEqual(bz_properties['notes'], ['1'])

    @mock.patch('webhook.common.update_webhook_comment', mock.Mock())
    @mock.patch('webhook.common.get_commits_count')
    def test_bz_validation_noaction(self, gccount):
        instance = mock.Mock()
        project = mock.Mock()
        project.path_with_namespace = "route"
        project.archived = False
        merge_request = mock.Mock()
        merge_request.state = 'closed'
        merge_request.iid = 66
        gccount.return_value = 2001, 30
        bugzilla.run_bz_validation(instance, project, merge_request, True)
        gccount.assert_called_once_with(project, merge_request)
        gccount.return_value = 20, 30
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bugzilla.run_bz_validation(instance, project, merge_request, True)
            self.assertIn('Not running bugzilla validation hook on route/66, MR is closed',
                          ' '.join(logs.output))
        merge_request.state = 'open'
        project.archived = True
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bugzilla.run_bz_validation(instance, project, merge_request, True)
            self.assertIn('Not running bugzilla validation hook on route/66, project is archived',
                          ' '.join(logs.output))

    @mock.patch('webhook.cdlib.extract_files')
    @mock.patch('webhook.bugzilla.get_report_table',
                mock.Mock(return_value=([], bugzilla.BZState.READY_FOR_MERGE)))
    @mock.patch('webhook.bugzilla.check_bzs_for_cve', mock.Mock(return_value=None))
    @mock.patch('webhook.bugzilla.validate_bzs')
    def test_check_on_bzs(self, vbz, filelist):
        vbz.return_value = (['Some note'], 'sometarget')
        filelist.return_value = ['redhat/Makefile']
        project = mock.Mock()
        merge_request = mock.Mock()
        merge_request.iid = 666
        c1 = mock.Mock(id="abcd1234",
                       message="Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=1234567",
                       parent_ids=["abcdef12"])
        project.commits.get.return_value = c1
        merge_request.commits.return_value = [c1]
        # Bug/dep/cve IDs are what was found in merge_request.description
        bug_ids = ['1234567']
        dep_ids = []
        cve_ids = []
        bzcon = mock.Mock()
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            notes, tgt, table, approved = bugzilla.check_on_bzs(project, merge_request, bug_ids,
                                                                dep_ids, cve_ids, bzcon)
            self.assertIn('All MR 666 description bugs present and accounted for in commit(s)',
                          ' '.join(logs.output))
        bug_ids = ['1234567', '1900888']
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            notes, tgt, table, approved = bugzilla.check_on_bzs(project, merge_request, bug_ids,
                                                                dep_ids, cve_ids, bzcon)
            self.assertIn('Some bugs found in MR 666 description not found in any commit(s)',
                          ' '.join(logs.output))
            self.assertIn("Bugs found in MR 666 description: ['1234567', '1900888']",
                          ' '.join(logs.output))
            self.assertIn("Bugs found in MR 666 commits: ['1234567']",
                          ' '.join(logs.output))
        bug_ids = ['1900888']
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            notes, tgt, table, approved = bugzilla.check_on_bzs(project, merge_request, bug_ids,
                                                                dep_ids, cve_ids, bzcon)
            self.assertIn('Some bugs found in MR 666 description not found in any commit(s)',
                          ' '.join(logs.output))
            self.assertIn("Bugs found in MR 666 description: ['1900888']",
                          ' '.join(logs.output))
            self.assertIn("Bugs found in MR 666 commits: []",
                          ' '.join(logs.output))
        bug_ids = ['1234567']
        dep_ids = ['1234567']
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            notes, tgt, table, approved = bugzilla.check_on_bzs(project, merge_request, bug_ids,
                                                                dep_ids, cve_ids, bzcon)
            self.assertIn("Found bug 1234567 as both Bugzilla: and Depends:",
                          ' '.join(logs.output))
        bug_ids = ['INTERNAL']
        c1.message = "Bugzilla: INTERNAL"
        cve_ids = ['CVE-2040-88888']
        with self.assertLogs('cki.webhook.bugzilla', level='DEBUG') as logs:
            notes, tgt, table, approved = bugzilla.check_on_bzs(project, merge_request, bug_ids,
                                                                dep_ids, cve_ids, bzcon)
            self.assertIn('All MR 666 description bugs present and accounted for in commit(s)',
                          ' '.join(logs.output))
            self.assertIn("Found cve CVE-2040-88888 in MR 666 description", ' '.join(logs.output))

    @mock.patch('webhook.bugzilla.Projects')
    @mock.patch('webhook.bugzilla.get_one_policy')
    def test_get_target_branch(self, mock_get_policy, mock_projects):
        """Fetches Branch from Projects and sets policy."""
        mock_branch = mock.Mock()
        mock_projects().get_project_by_id().get_branch_by_name.return_value = mock_branch
        result = bugzilla.get_target_branch(123, 'main')
        mock_projects().get_project_by_id.assert_called_with(123)
        mock_projects().get_project_by_id().get_branch_by_name.assert_called_with('main')
        mock_get_policy.assert_called_with(mock_branch.distgit_ref)
        mock_branch.set_policy.assert_called_with(mock_get_policy.return_value)
        self.assertEqual(result, mock_branch)

        # Raises RuntimeError if no policy is found
        mock_get_policy.return_value = None
        exception_raised = False
        try:
            bugzilla.get_target_branch(123, 'main')
        except RuntimeError:
            exception_raised = True
        self.assertTrue(exception_raised)
