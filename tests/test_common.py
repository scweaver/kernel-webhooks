"""Webhook interaction tests."""
from copy import deepcopy
import json
import os
import pathlib
import tempfile
from unittest import TestCase
from unittest import mock

from gitlab.exceptions import GitlabCreateError
from gitlab.exceptions import GitlabGetError

from tests import fakes
from webhook import common
from webhook import defs

NEEDS_REVIEW_LABEL_COLOR = '#FF0000'
NEEDS_TESTING_LABEL_COLOR = '#CAC542'
READY_LABEL_COLOR = '#428BCA'
MERGE_LABEL_COLOR = '#8BCA42'


class MyLister:
    def __init__(self, labels):
        self.labels = labels
        self.call_count = 0
        self.called_with_all = None

    def list(self, search=None, all=False):
        self.call_count += 1
        if all:
            self.called_with_all = True
            return self.labels
        self.called_with_all = False
        if not search:
            raise AttributeError
        return [label for label in self.labels if label.name == search]


@mock.patch('cki_lib.gitlab.get_token', mock.Mock(return_value='TOKEN'))
class TestCommon(TestCase):
    PROJECT_LABELS = [{'id': 1,
                       'name': 'readyForMerge',
                       'color': MERGE_LABEL_COLOR,
                       'description': ('All automated checks pass, this merge request'
                                       ' should be suitable for inclusion in main now.'),
                       'text_color': '#FFFFFF',
                       'priority': 1
                       },
                      {'id': 2,
                       'name': 'readyForQA',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 3,
                       'name': 'Acks::NACKed',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request has been explicitly Nacked'
                                       ' by Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 4,
                       'name': 'Acks::NeedsReview',
                       'color': NEEDS_REVIEW_LABEL_COLOR,
                       'description': ('This merge request needs more reviews and acks'
                                       ' from Red Hat engineering.'),
                       'text_color': '#FFFFFF',
                       'priority': 3
                       },
                      {'id': 5,
                       'name': 'Acks::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ('This merge request has been reviewed by Red Hat'
                                       ' engineering and approved for inclusion.'),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 6,
                       'name': 'Bugzilla::OK',
                       'color': READY_LABEL_COLOR,
                       'description': ("This merge request's bugzillas are approved for"
                                       " integration"),
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 7,
                       'name': 'Bugzilla::NeedsTesting',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': "This merge request's bugzillas are ready for QA testing.",
                       'text_color': '#FFFFFF',
                       'priority': 4
                       },
                      {'id': 8,
                       'name': 'lnst::NeedsTesting',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': ('The subsystem-required lnst testing has not yet'
                                       ' been completed.'),
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 9,
                       'name': 'lnst::OK',
                       'color': READY_LABEL_COLOR,
                       'text_color': '#FFFFFF',
                       'priority': 12
                       },
                      {'id': 10,
                       'name': 'Dependencies::abcdef012345',
                       'color': '#123456',
                       'description': 'This MR has dependencies.',
                       'text_color': '#FFFFFF',
                       'priority': 15
                       },
                      {'id': 11,
                       'name': 'Acks::net::NeedsReview',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'descroption': 'Subsystem needs Acks'
                       },
                      {'id': 12,
                       'name': 'CKI::Failed::test',
                       'color': NEEDS_TESTING_LABEL_COLOR,
                       'description': 'Failed CKI testing'
                       }]

    MAPPINGS = {'subsystems':
                [{'subsystem': 'MEMORY MANAGEMENT',
                  'labels': {'name': 'mm'},
                  'paths': {'includes': ['include/linux/mm.h', 'include/linux/vmalloc.h', 'mm/']}
                  },
                 {'subsystem': 'NETWORKING',
                  'labels': {'name': 'net',
                             'readyForMergeDeps': ['lnst']},
                  'paths': {'includes': ['include/linux/net.h', 'include/net/', 'net/']},
                  },
                 {'subsystem': 'XDP',
                  'labels': {'name': 'xdp'},
                  'paths': {'includes': ['kernel/bpf/devmap.c', 'include/net/page_pool.h'],
                            'includeRegexes': ['xdp']}
                  }]
                }

    FILE_CONTENTS = (
        '---\n'
        'subsystems:\n'
        ' - subsystem: MEMORY MANAGEMENT\n'
        '   labels:\n'
        '     name: mm\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/mm.h\n'
        '          - include/linux/vmalloc.h\n'
        '          - mm/\n'
        ' - subsystem: NETWORKING\n'
        '   labels:\n'
        '     name: net\n'
        '     readyForMergeDeps:\n'
        '       - lnst\n'
        '   paths:\n'
        '       includes:\n'
        '          - include/linux/net.h\n'
        '          - include/net/\n'
        '          - net/\n'
        ' - subsystem: XDP\n'
        '   labels:\n'
        '     name: xdp\n'
        '   paths:\n'
        '       includes:\n'
        '          - kernel/bpf/devmap.c\n'
        '          - include/net/page_pool.h\n'
        '       includeRegexes:\n'
        '          - xdp\n'
        )

    YAML_LABELS = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                   {'name': 'Bugzilla::OnQA', 'color': '#CAC542', 'description': 'maybe'},
                   {'name': '^Subsystem:(\\w+)$',
                    'color': '#778899',
                    'description': 'hi %s',
                    'regex': True}
                   ]

    NATTRS = {'body': 'This should be unique per webhook\n\nEverything is fine, I swear...',
              'author': {'username': 'shadowman'},
              'id': '1234'}

    MOCK_PROJS = {'projects': [{'name': 'project1',
                                'ids': [12345, 56789],
                                'inactive': False,
                                'product': 'Project 1',
                                'branches': [{'name': 'main',
                                              'inactive': False,
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.1.0',
                                              'internal_target_release': '10.1.0',
                                              'milestone': 'RHEL-10.1.0'
                                              },
                                             {'name': '10.0',
                                              'component': 'kernel',
                                              'distgit_ref': 'rhel-10.0.0',
                                              'zstream_target_release': '10.0.0',
                                              'milestone': 'RHEL-10.0.0'
                                              },
                                             {'name': 'main-auto',
                                              'component': 'kernel-automotive',
                                              'distgit_ref': 'rhel-10.0.0',
                                              'zstream_target_release': '10.0.0'
                                              }]
                                }]
                  }

    def test_mr_is_closed(self):
        mrequest = mock.Mock()
        mrequest.state = "closed"
        status = common.mr_is_closed(mrequest)
        self.assertTrue(status)
        mrequest.state = "opened"
        status = common.mr_is_closed(mrequest)
        self.assertFalse(status)

    def test_build_note_string(self):
        notes = []
        notes += ["1"]
        notes += ["2"]
        notes += ["3"]
        notestring = common.build_note_string(notes)
        self.assertEqual(notestring, "See 1, 2, 3|\n")

    def test_build_commits_for_row(self):
        commits = ["abcdef012345"]
        table = [["012345abcdef", commits, 1, "", ""]]
        for row in table:
            commits = common.build_commits_for_row(row)
            self.assertEqual(commits, ["abcdef01"])

    def test_mr_action_affects_commits(self):
        """Check handling of a merge request with unwanted actions."""
        message = mock.Mock()
        # Missing 'action'
        payload = {'object_attributes': {}}
        message.payload = payload
        self.assertEqual(common.mr_action_affects_commits(message), None)

        # 'open' action should return True.
        payload = {'object_attributes': {'action': 'open'}}
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

        # 'update' action with oldrev set should return True.
        payload = {'object_attributes': {'action': 'update',
                                         'oldrev': 'hi'}
                   }
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

        # 'update' action without oldrev should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'update'}}
            message.payload = payload
            self.assertFalse(common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR \'update\' action without an oldrev.", logs.output[-1])

        # Action other than 'new' or 'update' should return False and log it.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload = {'object_attributes': {'action': 'partyhard'}}
            message.payload = payload
            self.assertFalse(common.mr_action_affects_commits(message))
            self.assertIn("Ignoring MR action 'partyhard'", logs.output[-1])

        # Target branch changed should return True
        payload = {'object_attributes': {'action': 'update'},
                   'changes': {'merge_status': {'previous': 'can_be_merged',
                                                'current': 'unchecked'}}}
        message.payload = payload
        self.assertTrue(common.mr_action_affects_commits(message))

    def test_required_label_removed(self):
        payload = {'object_attributes': {'action': 'update'}}
        testing = defs.NEEDS_TESTING_SUFFIX
        ready = defs.READY_SUFFIX
        failed = defs.TESTING_FAILED_SUFFIX
        waived = defs.TESTING_WAIVED_SUFFIX

        changed_labels = ['lnst::NeedsTesting', 'whatever::OK']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Drivers:bonding']
        result = common.required_label_removed(payload, testing, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['rdmalab::OK']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['Bugzilla::OK']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['lnst::TestingFailed']
        result = common.required_label_removed(payload, ready, changed_labels)
        self.assertEqual(result, False)

        changed_labels = ['lnst::TestingFailed']
        result = common.required_label_removed(payload, failed, changed_labels)
        self.assertEqual(result, True)

        changed_labels = ['lnst::Waived']
        result = common.required_label_removed(payload, waived, changed_labels)
        self.assertEqual(result, True)

    def test_has_label_changed(self):
        """Returns True if the label name has changed."""
        # No labels key in changes
        changes = {}
        self.assertFalse(common.has_label_changed(changes, 'my_cool_label'))

        # Label has not changed.
        labels = {'previous': [{'title': 'my_cool_label'}],
                  'current': [{'title': 'my_cool_label'}]
                  }
        changes['labels'] = labels
        self.assertFalse(common.has_label_changed(changes, 'my_cool_label'))

        # Label has changed.
        labels = {'previous': [{'title': 'my_cool_label'}],
                  'current': [{'title': 'a different label'}]
                  }
        changes['labels'] = labels
        self.assertTrue(common.has_label_changed(changes, 'my_cool_label'))

    def test_has_label_prefix_changed(self):
        """Returns True if label matched on prefix and changed, or False."""
        # No labels key in changes
        changes = {}
        self.assertFalse(common.has_label_prefix_changed(changes, 'Acks::'))

        # Label has not changed.
        changes = {}
        labels = {'previous': [{'title': 'Acks::OK'}],
                  'current': [{'title': 'Acks::OK'}]
                  }
        changes['labels'] = labels
        self.assertFalse(common.has_label_prefix_changed(changes, 'Acks::'))

        # Label has changed.
        labels = {'previous': [{'title': 'Acks::NeedsReview'}],
                  'current': [{'title': 'Acks::OK'}]
                  }
        changes['labels'] = labels
        self.assertTrue(common.has_label_prefix_changed(changes, 'Acks::'))

    def test_find_extra_required_labels(self):
        labels = ['lnst::NeedsTesting']
        result = common._find_extra_required_labels(labels)
        self.assertEqual(result, ['lnst::OK'])

    def _process_message(self, mock_gl, msg, auth_user):
        webhooks = {'note': mock.Mock()}

        fake_gitlab = mock.Mock(spec=[])

        def mock_auth():
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="mock_auth")

        fake_gitlab.auth = mock_auth
        if auth_user:
            fake_gitlab.user = mock.Mock(username=auth_user, _setup_from="init")

        mock_gl.return_value.__enter__.return_value = fake_gitlab

        with tempfile.NamedTemporaryFile() as tmp:
            pathlib.Path(tmp.name).write_text(json.dumps(msg, indent=None))
            parser = common.get_arg_parser('test')
            args = parser.parse_args(['--json-message-file', tmp.name])
            common.consume_queue_messages(args, webhooks)

        if auth_user:
            self.assertEqual(fake_gitlab.user._setup_from, "init")
        else:
            self.assertEqual(fake_gitlab.user._setup_from, "mock_auth")

        return webhooks['note']

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_json_processing(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_no_auth(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, None)
        webhook.assert_called_once()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_bot_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'cki-bot'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_not_called()

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
    def test_cmdline_regular_username(self, mock_gl):
        msg = {'object_kind': 'note', 'state': 'opened', 'user': {'username': 'user1'},
               'object_attributes': {'noteable_type': 'MergeRequest'}}
        webhook = self._process_message(mock_gl, msg, 'cki-bot')
        webhook.assert_called_once()

    def test_make_payload(self):
        # Note payload
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'note')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 123)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload['object_attributes']['noteable_type'], 'MergeRequest')
        self.assertEqual(payload['merge_request']['iid'], 123)

        # MR payload
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'merge_request')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 123)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'merge_request')
        self.assertEqual(payload['object_attributes']['iid'], 123)
        self.assertFalse(payload['object_attributes']['work_in_progress'])

        # Pipeline payload
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'pipeline')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 123)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'pipeline')
        self.assertEqual(payload['merge_request']['iid'], 123)

        # Some other payload?
        payload = common.make_payload('https://web.url/g/p/-/merge_requests/123', 'lion_attack')
        self.assertEqual(payload['user']['username'], 'cli')
        self.assertEqual(payload['project']['id'], 123)
        self.assertEqual(payload['project']['web_url'], 'https://web.url/g/p')
        self.assertEqual(payload['object_kind'], 'lion_attack')

    def test_process_mr_url(self):
        url = 'https://web.url/g/p/-/merge_requests/123'

        # Note
        note_text = 'request-evaluation'
        payload = common.process_mr_url(url, 'note', note_text)
        self.assertEqual(payload['object_kind'], 'note')
        self.assertEqual(payload["object_attributes"]["note"], note_text)

        # MR
        payload = common.process_mr_url(url, 'merge_request', None)
        self.assertEqual(payload['object_kind'], 'merge_request')

        # Pipeline
        payload = common.process_mr_url(url, 'pipeline', None)
        self.assertEqual(payload['object_kind'], 'pipeline')

    @mock.patch('webhook.common.consume_queue_messages')
    @mock.patch('webhook.common.process_mr_url')
    @mock.patch('webhook.common.process_message')
    def test_generic_loop(self, mock_process_message, mock_mr_url, mock_consume):
        WEBHOOKS = {}
        mr_url = 'https://web.url/g/p/-/merge_requests/123'
        headers = {'message-type': 'gitlab'}

        # --merge-request, no --note, no --action
        args = mock.Mock(merge_request=mr_url, action=None, note=None)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'merge_request', None)
        mock_process_message.assert_called_with(args, WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # --merge-request with --note, no --action
        note_text = 'request-evaluation'
        args = mock.Mock(merge_request=mr_url, action=None, note=note_text)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'note', note_text)
        mock_process_message.assert_called_with(args, WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # --merge-request and --action set, no --note.
        args = mock.Mock(merge_request=mr_url, action='pipeline', note=None)
        common.generic_loop(args, WEBHOOKS)
        mock_consume.assert_not_called()
        mock_mr_url.assert_called_with(mr_url, 'pipeline', None)
        mock_process_message.assert_called_with(args, WEBHOOKS, 'cmdline', mock_mr_url.return_value,
                                                headers)

        # no --merge-request
        mock_mr_url.reset_mock()
        mock_process_message.reset_mock()
        args = mock.Mock(merge_request=None)
        common.generic_loop(args, WEBHOOKS)
        mock_mr_url.assert_not_called()
        mock_process_message.assert_not_called()
        mock_consume.assert_called_with(args, WEBHOOKS)

    def test_process_umb_bridge_message(self):
        """Test processing of umb_bridge messages."""
        # No handler.
        webhooks = {}
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            result = common._process_umb_bridge_message('headers', webhooks)
            self.assertFalse(result)
            self.assertIn(f'No {defs.UMB_BRIDGE_MESSAGE_TYPE} handler, ignoring',
                          logs.output[-1])

        # A handler.
        mock_handler = mock.Mock()
        webhooks = {defs.UMB_BRIDGE_MESSAGE_TYPE: mock_handler}
        result = common._process_umb_bridge_message('headers', webhooks)
        self.assertTrue(result)
        mock_handler.assert_called_with('headers')

    def test_process_amqp_bridge_message(self):
        """Test processing of amqp-bridge messages."""
        # No amqp-bridge handler.
        webhooks = {}
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            result = common._process_amqp_bridge_message('body', webhooks)
            self.assertFalse(result)
            self.assertIn('No amqp-bridge handler, ignoring', logs.output[-1])

        # A handler.
        mock_handler = mock.Mock()
        webhooks = {'amqp-bridge': mock_handler}
        result = common._process_amqp_bridge_message('body', webhooks)
        self.assertTrue(result)
        mock_handler.assert_called_with('body')

    @mock.patch('webhook.common._process_umb_bridge_message')
    @mock.patch('webhook.common._process_amqp_bridge_message')
    @mock.patch('webhook.common._process_gitlab_message')
    def test_process_message(self, mock_gitlab, mock_amqp, mock_umb):
        """Test message types."""
        args = common.get_arg_parser('TEST').parse_args('')
        # Handle a gitlab message.
        headers = {'message-type': 'gitlab'}
        common.process_message(args, 'webhooks', 'routing.key', 2, headers)
        mock_gitlab.assert_called_with(2, 'webhooks', args)

        # Handle an amqp message.
        headers = {'message-type': 'amqp-bridge'}
        common.process_message(args, 'webhooks', 'routing.key', 2, headers)
        mock_amqp.assert_called_with(2, 'webhooks')

        # Handle a umb_bridge message.
        headers = {'message-type': defs.UMB_BRIDGE_MESSAGE_TYPE}
        common.process_message(args, 'webhooks', 'routing.key', 2, headers)
        mock_umb.assert_called_with(headers, 'webhooks')

        # Complain about unknown message type.
        headers = {'message-type': 'pigeon'}
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            result = common.process_message(args, {}, 'routing.key', '', headers)
            self.assertTrue(result)
            self.assertIn('Ignoring unknown message type pigeon', logs.output[-1])

    @mock.patch('json.dumps')
    @mock.patch('webhook.common.is_event_target_branch_active')
    def test_process_gitlab_message(self, mock_branch_active, dumps):
        """Check for expected return value."""
        webhooks = {'merge_request': mock.Mock(), 'note': mock.Mock()}
        payload = {}
        args = common.get_arg_parser('TEST').parse_args('')

        # Wrong object kind should be ignored, return False.
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'delete_project'
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn('Ignoring message with missing or unhandled object_kind.',
                          logs.output[-1])

        # Inactive branches should be ignored.
        mock_branch_active.return_value = False
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'merge_request'
            payload['object_attributes'] = {'state': 'opened'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn("Ignoring message with target branch that is not active", logs.output[-1])

        # Closed state should be ignored, return False.
        mock_branch_active.return_value = True
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            payload['object_kind'] = 'merge_request'
            payload['object_attributes'] = {'state': 'closed'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn("Ignoring event with 'closed' state.", logs.output[-1])

        # If username matches return False (event was generated by our own activity).
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bot')
            payload['object_kind'] = 'merge_request'
            payload['object_attributes'] = {'state': 'opened'}
            payload['user'] = {'username': 'bot'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            payload['user'] = {'username': 'notbot'}
            self.assertTrue(common._process_gitlab_message(payload, webhooks, args))

        # Ignore notes on issues.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'Issue'}
            self.assertFalse(common._process_gitlab_message(payload, webhooks, args))
            self.assertIn('Ignoring note event for non-MR.', logs.output[-1])

        # Notes on merge requests should be processed.
        with self.assertLogs('cki.webhook.common', level='INFO'), \
             mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'note'
            payload['object_attributes'] = {'noteable_type': 'MergeRequest'}
            self.assertTrue(common._process_gitlab_message(payload, webhooks, args))
            self.assertEqual(len(webhooks['merge_request'].call_args.args), 2)

        # Recognize get_gl_instance=False.
        with mock.patch('webhook.common.Message.gl_instance') as mocked_gl:
            mocked_gl().__enter__().user = mock.Mock(username='bob')
            payload['object_kind'] = 'merge_request'
            payload['state'] = 'opened'

            res = common._process_gitlab_message(payload, webhooks, args, get_gl_instance=False)
            self.assertTrue(res)
            self.assertEqual(len(webhooks['merge_request'].call_args.args), 1)

    @mock.patch('webhook.common.validate_labels')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_plabel_commands(self, mr_labels, new_labels, expected_cmd, is_prod, mock_validate):
        label_names = [label['name'] for label in new_labels]
        mock_validate.return_value = new_labels
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_mergerequest = mock.Mock()
        gl_mergerequest.iid = 2
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_plabel_to_merge_request(gl_project, 2, label_names)

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and label_ret:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and label_ret:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertIn("Editing label %s on project." % label['name'],
                                  ' '.join(logs.output))
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("Creating label {\'name\': \'%s\', \'color\': \'%s\',"
                                    " \'description\': \'%s\'} on project.") \
                                    % (label['name'], label['color'], label['description'])
                    self.assertIn(label_string, ' '.join(logs.output))
                    call_list.append(mock.call(label))
            if call_list:
                gl_project.labels.create.assert_has_calls(call_list)
            else:
                gl_project.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            self.assertTrue(label_ret)
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
        else:
            self.assertFalse(label_ret)
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._update_bugzilla_state')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def _test_label_commands(self, mr_labels, new_labels, expected_cmd, is_prod, mock_update_bzs,
                             remove_scoped=False, draft=False):
        label_names = [label['name'] for label in new_labels]
        label_objects = []
        for label in self.PROJECT_LABELS:
            label_obj = mock.Mock(name=label['name'])
            label_obj.save = mock.Mock()
            for key in label:
                setattr(label_obj, key, label[key])
            label_objects.append(label_obj)

        gl_instance = mock.Mock()
        rh_group = mock.Mock(id=10810393)
        gl_instance.groups.list.return_value = [rh_group]
        gl_group = mock.Mock()
        gl_instance.groups.get.return_value = gl_group

        gl_mergerequest = mock.Mock(iid=2, draft=draft)
        gl_mergerequest.labels = [label['name'] for label in mr_labels]
        gl_mergerequest.notes.create = mock.Mock()

        gl_project = mock.Mock()
        gl_project.labels = MyLister(label_objects)
        gl_project.labels.create = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mergerequest
        gl_project.namespace = mock.MagicMock(id=1, full_path="redhat/rhel/src/kernel")

        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            label_ret = common.add_label_to_merge_request(gl_instance, gl_project, 2, label_names,
                                                          remove_scoped)

            # If there are more than 5 labels we should have used one API call to labels.list().
            labels_called_with_all = None
            labels_call_count = 0
            if len(new_labels) >= 5 and label_ret:
                labels_called_with_all = True
                labels_call_count = 1
            elif len(new_labels) <= 5 and label_ret:
                labels_called_with_all = False
                labels_call_count = expected_cmd.count('/label')
            self.assertEqual(gl_project.labels.called_with_all, labels_called_with_all)
            self.assertEqual(gl_project.labels.call_count, labels_call_count)

            # If label existed on the project, ensure it was edited if needed.
            for label in new_labels:
                pl_obj = \
                    next((pl for pl in label_objects if pl.name == label['name']), None)
                pl_dict = \
                    next((pl for pl in self.PROJECT_LABELS if pl['name'] == label['name']), None)
                if pl_obj and not (label.items() <= pl_dict.items()):
                    self.assertIn("Editing label %s on group." % label['name'],
                                  ' '.join(logs.output))
                    pl_obj.save.assert_called_once()
            # If new label didn't exist on the project, confirm it was added.
            call_list = []
            for label in new_labels:
                if not [plabel for plabel in self.PROJECT_LABELS
                        if plabel['name'] == label['name']]:
                    label_string = ("Creating label {\'name\': \'%s\', \'color\': \'%s\',"
                                    " \'description\': \'%s\'} on group.") \
                                    % (label['name'], label['color'], label['description'])
                    self.assertIn(label_string, ' '.join(logs.output))
                    call_list.append(mock.call(label))
            if call_list:
                gl_group.labels.create.assert_has_calls(call_list)
            else:
                gl_group.labels.create.assert_not_called()

        gl_project.mergerequests.get.assert_called_with(2)

        if expected_cmd:
            self.assertTrue(label_ret)
            gl_mergerequest.notes.create.assert_called_with({'body': expected_cmd})
            mock_update_bzs.assert_called_once()
        else:
            self.assertFalse(label_ret)
            gl_mergerequest.notes.create.assert_not_called()

    @mock.patch('webhook.common._compute_mr_status_labels')
    @mock.patch('webhook.common._add_label_quick_actions', return_value=['/label "hello"'])
    @mock.patch('webhook.common._filter_mr_labels')
    def test_add_label_exception(self, mock_filter, mock_quickaction, mock_compute):
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        instance = mock.Mock()
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        mock_filter.return_value = ([], [])
        label_list = ['Acks::OK']

        # Catch "can't be blank" and move on.
        err_text = "400 Bad request - Note {:note=>[\"can't be blank\"]}"
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message=err_text,
                                                                  response_code=400,
                                                                  response_body='')
        exception_raised = False
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            with mock.patch('cki_lib.misc.is_production', return_value=True):
                try:
                    result = common.add_label_to_merge_request(instance, project, mergerequest.iid,
                                                               label_list)
                except GitlabCreateError:
                    exception_raised = True
                self.assertTrue(result)
                self.assertFalse(exception_raised)
                self.assertIn("can't be blank", logs.output[-1])

        # For any other error let it bubble up.
        mergerequest.notes.create.side_effect = GitlabCreateError(error_message='oops',
                                                                  response_code=403,
                                                                  response_body='')
        exception_raised = False
        with mock.patch('cki_lib.misc.is_production', return_value=True):
            try:
                common.add_label_to_merge_request(instance, project, mergerequest.iid, label_list)
            except GitlabCreateError:
                exception_raised = True
        self.assertTrue(exception_raised)

    def test_add_plabel_to_merge_request(self):
        # Add a project label which does not exist on the project.
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::abcdef012345"')

        # Add scoped Deps::OK::sha label
        new_labels = [{'name': 'Dependencies::OK::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands([], new_labels, '/label "Dependencies::OK::abcdef012345"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                      'description': 'This MR has dependencies.'}]
        new_labels = [{'name': 'Dependencies::abcdef012345', 'color': '#123456',
                       'description': 'This MR has dependencies.'}]
        self._test_plabel_commands(mr_labels, new_labels, None)

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_create_group_milestone(self, prod):
        gl_group = mock.Mock()
        product = "Red Hat Enterprise Linux 10"
        rhmeta = mock.Mock()
        rhmeta.internal_target_release = ""
        rhmeta.zstream_target_release = ""
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            new_ms = common._create_group_milestone(gl_group, product, rhmeta)
            self.assertEqual(new_ms, None)
            self.assertIn("Unable to find release for milestone", " ".join(logs.output))

        rhmeta.internal_target_release = "10.1.0"
        rhmeta.milestone = "RHEL-10.1.0"
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            common._create_group_milestone(gl_group, product, rhmeta)
            self.assertIn("Creating new milestone for", " ".join(logs.output))
            gl_group.milestones.create.assert_called_once()

        prod.return_value = False
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            new_ms = common._create_group_milestone(gl_group, product, rhmeta)
            self.assertEqual(new_ms, None)
            self.assertIn("Would have created milestone for", " ".join(logs.output))

    @mock.patch('webhook.common._get_gitlab_group')
    @mock.patch('webhook.rh_metadata.load')
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_add_merge_request_to_milestone(self, is_prod, projects, gglg):
        gl_instance = mock.Mock()
        gl_project = mock.Mock()
        gl_project.id = 12345
        gl_mergerequest = mock.Mock()
        gl_mergerequest.target_branch = 'main'
        gl_group = mock.Mock()
        gglg.return_value = gl_group
        target_milestone = mock.Mock()
        target_milestone.title = 'RHEL-10.1.0'
        target_milestone.id = 6060606
        gl_group.milestones.list.return_value = [target_milestone]
        gl_mergerequest.milestone_id = 6060606
        gl_mergerequest.milestone = {'title': 'RHEL-10.1.0', 'id': 6060606}
        gl_mergerequest.references = {'full': 'path/to/project!666'}
        projects.return_value = self.MOCK_PROJS

        # MR is already assigned to the correct Milestone
        common.add_merge_request_to_milestone(gl_instance, gl_project, gl_mergerequest)
        gl_mergerequest.save.assert_not_called()

        # MR will be assigned to an existing Milestone
        gl_mergerequest.milestone_id = 0
        gl_mergerequest.milestone = None
        common.add_merge_request_to_milestone(gl_instance, gl_project, gl_mergerequest)
        self.assertEqual(gl_mergerequest.milestone_id, target_milestone.id)
        gl_mergerequest.save.assert_called_once()

        # MR will be assigned to a newly created Milestone
        gl_mergerequest.milestone_id = 0
        target_milestone.title = 'RHEL-10.1.0'
        gl_mergerequest.target_branch = '10.0'
        gl_mergerequest.save.call_count = 0
        common.add_merge_request_to_milestone(gl_instance, gl_project, gl_mergerequest)
        gl_group.milestones.create.assert_called_once()
        gl_mergerequest.save.assert_called_once()

    def test_add_label_to_merge_request(self):
        # Add a label which already exists on the project.
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Existing project label with new color.
        new_labels = [{'name': 'Acks::OK', 'color': '#123456'}]
        self._test_label_commands([], new_labels, '/label "Acks::OK"')

        # Label already exists on MR, nothing to do.
        mr_labels = [{'name': 'Acks::OK'},
                     {'name': 'CommitRefs::OK'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels, None)

        # Add a label which does not exist.
        new_labels = [{'name': 'Subsystem:net', 'color': '#778899',
                       'description': 'An MR that affects code related to net.'}]
        self._test_label_commands(mr_labels, new_labels, '/label "Subsystem:net"')

        # Add a label which triggers readyForMerge being added to the MR.
        mr_labels = [{'name': 'Bugzilla::OK'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'readyForQA'}
                     ]
        new_labels = [{'name': 'Acks::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::OK"\n/label "readyForMerge"\n'
                                   '/unlabel "readyForQA"'))

        # Add a label which triggers readyForMerge being removed from the MR.
        mr_labels += [{'name': 'Acks::OK'}, {'name': 'readyForMerge'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add labels which trigger readyForQA.
        mr_labels = [{'name': 'Acks::NeedsReview'},
                     {'name': 'CKI::OK'},
                     {'name': 'CommitRefs::OK'},
                     {'name': 'Dependencies::OK'},
                     {'name': 'Signoff::OK'},
                     {'name': 'readyForMerge'}
                     ]
        new_labels = [{'name': 'Acks::OK'}, {'name': 'Bugzilla::NeedsTesting'}]
        expected_note = ['/label "Acks::OK"',
                         '/label "Bugzilla::NeedsTesting"',
                         '/unlabel "Acks::NeedsReview"',
                         '/label "readyForQA"',
                         '/unlabel "readyForMerge"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Would trigger readyForQA but the MR is in Draft state.
        expected_note = ['/label "Acks::OK"',
                         '/label "Bugzilla::NeedsTesting"',
                         '/unlabel "Acks::NeedsReview"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note), draft=True)

        # Add a double scoped label.
        new_labels = [{'name': 'CKI::Failed::test', 'color': '#FF0000',
                       'description': "This MR\'s latest CKI pipeline failed."}]
        expected_note = ['/label "CKI::Failed::test"',
                         '/unlabel "readyForMerge"',
                         '/unlabel "readyForQA"']
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a NeedsTesting label.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'}]
        new_labels = [{'name': 'lnst::NeedsTesting'}]
        self._test_label_commands(mr_labels, new_labels, '/label "lnst::NeedsTesting"')

        # Add an lnst::OK label.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'CommitRefs::OK'},
                     {'name': 'lnst::NeedsTesting'}]
        new_labels = [{'name': 'lnst::OK'}]
        self._test_label_commands(mr_labels, new_labels,
                                  '/label "lnst::OK"\n/unlabel "lnst::NeedsTesting"')

        # Change a single scoped label and ensure related double-scoped are not removed too.
        mr_labels += [{'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::NeedsReview'}]
        expected_note = ['/label "Acks::NeedsReview"',
                         '/unlabel "Acks::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Change a double scoped label and ensure only related double scoped are removed.
        mr_labels = [{'name': 'Acks::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'Acks::net::NeedsReview'}]
        expected_note = ['/label "Acks::net::NeedsReview"',
                         '/unlabel "Acks::net::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note))

        # Add a double scoped label with remove_scoped set.
        mr_labels = [{'name': 'CKI::OK'}, {'name': 'Acks::net::OK'}]
        new_labels = [{'name': 'CKI::Failed::test'}]
        expected_note = ['/label "CKI::Failed::test"',
                         '/unlabel "CKI::OK"'
                         ]
        self._test_label_commands(mr_labels, new_labels, '\n'.join(expected_note),
                                  remove_scoped=True)

        # Add a lot of labels to trigger labels.list(all=True).
        new_labels = []
        count = 1
        while count <= 8:
            name = f"Acks::ss{count}::OK"
            desc = f"This MR has been approved by reviewers in the ss{count} group."
            label = {'name': name,
                     'color': '#428BCA',
                     'description': desc}
            new_labels.append(label)
            count += 1
        self._test_label_commands(mr_labels, new_labels,
                                  ('/label "Acks::ss1::OK"\n/label "Acks::ss2::OK"'
                                   '\n/label "Acks::ss3::OK"\n/label "Acks::ss4::OK"'
                                   '\n/label "Acks::ss5::OK"\n/label "Acks::ss6::OK"'
                                   '\n/label "Acks::ss7::OK"\n/label "Acks::ss8::OK"'))

    def test_find_dep_mr_in_line(self):
        namespace = 'dummy/test'
        line = 'Depends: https://gitlab.com/dummy/test/-/merge_requests/135'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), 135)
        namespace = 'foo/bar'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), None)
        line = 'Depends: https://foo.bar/nope/-/merge_requests/1'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), None)
        line = 'Depends: !8675309'
        self.assertEqual(common.find_dep_mr_in_line(namespace, line), 8675309)

    def test_extract_dependencies(self):
        """Check for expected output."""
        # Test handling of None input message.
        dep_mr = mock.Mock()
        dep_mr.iid = 110
        dep_mr.description = "Bugzilla: https://bugzilla.redhat.com/24681357"
        project = mock.Mock()
        project.mergerequests.get.return_value = dep_mr
        project.path_with_namespace = 'fake/stuff'
        bzs, errors = common.extract_dependencies(project, None)
        self.assertEqual(bzs, [])
        self.assertEqual(errors, '')

        message = ('This BZ has dependencies.\n'
                   'Bugzilla: https://bugzilla.redhat.com/1234567\n'
                   'Depends: https://bugzilla.redhat.com/22334455\n'
                   'Depends: https://bugzilla.redhat.com/33445566\n'
                   'Depends: https://gitlab.com/fake/stuff/-/merge_requests/110\n')
        bzs, errors = common.extract_dependencies(project, message)
        self.assertEqual(bzs, ['22334455', '33445566', '24681357'])

    def test_try_bugzilla_conn_no_key(self):
        """Check for negative return when BUGZILLA_API_KEY is not set."""
        self.assertFalse(common.try_bugzilla_conn())

    @mock.patch.dict(os.environ, {'BUGZILLA_API_KEY': 'mocked'})
    def test_try_bugzilla_conn_with_key(self):
        """Check for positive return when BUGZILLA_API_KEY is set."""
        with mock.patch('webhook.common.connect_bugzilla', return_value=True):
            self.assertTrue(common.try_bugzilla_conn())

    def test_connect_bugzilla(self):
        """Check for expected return value."""
        api_key = 'totally_fake_api_key'
        bzcon = mock.Mock()
        with mock.patch('bugzilla.Bugzilla', return_value=bzcon):
            self.assertEqual(common.connect_bugzilla(api_key), bzcon)

    @mock.patch('bugzilla.Bugzilla')
    def test_connect_bugzilla_exception(self, mocked_bugzilla):
        """Check ConnectionError exception generates logging."""
        api_key = 'totally_fake_api_key'
        mocked_bugzilla.side_effect = ConnectionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem connecting to bugzilla server.', logs.output[-1])
        mocked_bugzilla.side_effect = PermissionError
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            self.assertFalse(common.connect_bugzilla(api_key))
            self.assertIn('Problem with file permissions', logs.output[-1])

    @mock.patch('webhook.common._edit_group_label')
    @mock.patch('webhook.common._edit_project_label')
    @mock.patch('webhook.common._match_label')
    def test_add_label_quick_actions(self, existing_label, plabel, glabel):
        """Check for the right label type (project or group) being evaluated."""
        mergerequest = mock.Mock()
        mergerequest.iid = 10
        instance = mock.Mock()
        project = mock.Mock()
        project.mergerequests.get.return_value = mergerequest
        label_list = [{'name': 'Acks::mm::OK'}]
        existing_label.return_value = ""
        output = common._add_label_quick_actions(instance, project, label_list)
        plabel.assert_not_called()
        glabel.assert_called_once()
        self.assertEqual(['/label "Acks::mm::OK"'], output)
        project.id = defs.ARK_PROJECT_ID
        common._add_label_quick_actions(instance, project, label_list)
        plabel.assert_called_once()
        self.assertEqual(['/label "Acks::mm::OK"'], output)

    def test_extract_all_from_message(self):
        """Check for expected output."""
        # Test handling of None input message.
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez, errors = common.extract_all_from_message(
            None, [], [], []
        )
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        self.assertEqual(cves, [])
        self.assertEqual(non_mr_cvez, [])

        message1 = 'Here is my perfect patch.\nBugzilla: https://bugzilla.redhat.com/18123456\n' \
                   'CVE: CVE-2021-00001'
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez, errors = common.extract_all_from_message(
            message1, [], [], []
        )
        self.assertEqual(bzs, ['18123456'])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, [])
        self.assertEqual(cves, ['CVE-2021-00001'])
        self.assertEqual(non_mr_cvez, [])
        bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez, errors = common.extract_all_from_message(
            message1, [], [], ['18123456']
        )
        self.assertEqual(bzs, [])
        self.assertEqual(non_mr_bzs, [])
        self.assertEqual(dep_bzs, ['18123456'])
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            bzs, non_mr_bzs, dep_bzs, cves, non_mr_cvez, errors = \
                common.extract_all_from_message(message1, ['12345678'], ['CVE-2021-00002'], [])
            self.assertIn('Bugzilla: 18123456 not listed in MR description.', logs.output[-2])
            self.assertIn('CVE: CVE-2021-00001 not listed in MR description.', logs.output[-1])
            self.assertEqual(bzs, [])
            self.assertEqual(non_mr_bzs, ['18123456'])
            self.assertEqual(dep_bzs, [])
            self.assertEqual(cves, [])
            self.assertEqual(non_mr_cvez, ['CVE-2021-00001'])

    def test_parse_gl_project_path(self):
        url1 = 'http://www.gitlab.com/mycoolproject/-/merge_requests/12345'
        url2 = 'http://www.gitlab.com/group/subgroup/project/-/pipelines/35435747'
        url3 = 'https://gitlab.com/this/is/a/test/-/issues/'

        self.assertEqual('mycoolproject', common.parse_gl_project_path(url1))
        self.assertEqual('group/subgroup/project', common.parse_gl_project_path(url2))
        self.assertEqual('this/is/a/test', common.parse_gl_project_path(url3))

    def test_bugs_to_process(self):
        old_description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                           'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                           'Bugzilla: https://bugzilla.redhat.com/3456789\n'
                           'Bugzilla: https://bugzilla.redhat.com/8765432')
        description = ('Bugzilla: https://bugzilla.redhat.com/1234567\n'
                       'Bugzilla: https://bugzilla.redhat.com/2345678\n'
                       'Bugzilla: https://bugzilla.redhat.com/3456789')
        changes = {'description': {'previous': old_description,
                                   'current': description}
                   }

        # Link all in description, unlink 8765432.
        result = common.bugs_to_process(description, 'open', changes)
        self.assertEqual(set(result['link']), {'1234567', '2345678', '3456789'})
        self.assertEqual(set(result['unlink']), {'8765432'})

        # If the action is close then just unlink everything in the current description.
        result = common.bugs_to_process(description, 'close', {})
        self.assertEqual(result['unlink'], {'1234567', '2345678', '3456789'})
        self.assertEqual(result['link'], set())

    def test_get_mr(self):
        gl_project = mock.Mock(id=234567)
        gl_mr = mock.Mock()
        gl_project.mergerequests.get.return_value = gl_mr

        # No error, gl_mr object is returned.
        exception_raised = False
        try:
            result = common.get_mr(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.mergerequests.get.assert_called_with(123)
        self.assertEqual(result, gl_mr)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock(return_value=True)
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=404,
                                                                  error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_mr(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.mergerequests.get.assert_called_with(456)
            self.assertIn('MR 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.mergerequests.get.reset_mock()
        gl_project.mergerequests.get.side_effect = GitlabGetError(response_code=502,
                                                                  error_message='oh no!')
        try:
            common.get_mr(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.mergerequests.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    def test_get_pipeline(self):
        gl_project = mock.Mock(id=234567)
        gl_pipeline = mock.Mock()
        gl_project.pipelines.get.return_value = gl_pipeline

        # No error, gl_pipeline object is returned.
        exception_raised = False
        try:
            result = common.get_pipeline(gl_project, 123)
        except:  # noqa: E722
            exception_raised = True
        gl_project.pipelines.get.assert_called_with(123)
        self.assertEqual(result, gl_pipeline)
        self.assertFalse(exception_raised)

        # GitlabGetError code 404, None returned.
        exception_raised = False
        gl_project.pipelines.get.reset_mock(return_value=True)
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=404,
                                                              error_message='oh no!')
        with self.assertLogs('cki.webhook.common', level='WARNING') as logs:
            try:
                result = common.get_pipeline(gl_project, 456)
            except GitlabGetError as err:
                if err.response_code == 404:
                    exception_raised = True
            gl_project.pipelines.get.assert_called_with(456)
            self.assertIn('Pipeline 456 does not exist in project 234567 (404).', logs.output[-1])
            self.assertEqual(result, None)
            self.assertFalse(exception_raised)

        # An unhandled exception so blow up.
        exception_raised = False
        gl_project.pipelines.get.reset_mock()
        gl_project.pipelines.get.side_effect = GitlabGetError(response_code=502,
                                                              error_message='oh no!')
        try:
            common.get_pipeline(gl_project, 789)
        except GitlabGetError as err:
            if err.response_code == 502:
                exception_raised = True
        gl_project.pipelines.get.assert_called_with(789)
        self.assertTrue(exception_raised)

    @mock.patch('webhook.common.get_session')
    @mock.patch('webhook.common.get_token')
    @mock.patch('webhook.common.Client')
    def test_gl_graphql_client(self, mock_client, mock_get_token, mock_get_session):
        mock_get_session.return_value = mock.Mock(headers={})
        mock_get_token.return_value = 'fake_token'
        result = common.gl_graphql_client()
        transport = mock_client.call_args.kwargs['transport']
        self.assertEqual(transport.__class__.__name__, '_CkiRequestsHTTPTransport')
        self.assertEqual(transport.session, mock_get_session())
        self.assertEqual(transport.session.headers, {'Authorization': 'Bearer fake_token'})
        self.assertEqual(result, mock_client())

    def test_draft_status(self):
        merge_dict = {'object_kind': 'merge_request',
                      'project': {'id': 1,
                                  'web_url': 'https://web.url/g/p'},
                      'object_attributes': {'target_branch': 'main',
                                            'iid': 2,
                                            'work_in_progress': False},
                      'state': 'opened',
                      'action': 'open',
                      'labels': []
                      }

        # Not a Draft, MR message changes do not include 'title'.
        merge_dict['object_attributes']['work_in_progress'] = False
        merge_dict['changes'] = {}
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # Not a draft and Titles don't mention Draft:.
        merge_dict['changes'] = {'title': {'previous': 'My awesome MR',
                                           'current': 'My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # No 'previous' title member.
        merge_dict['changes'] = {'title': {'current': 'My cool MR'}}
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertFalse(changed)

        # It's not a Draft any more!
        merge_dict['changes'] = {'title': {'previous': '[Draft] My cool MR',
                                           'current': 'My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertFalse(is_draft)
        self.assertTrue(changed)

        # I turned into a Draft.
        merge_dict['object_attributes']['work_in_progress'] = True
        merge_dict['changes'] = {'title': {'previous': 'My cool MR',
                                           'current': 'Draft: My cool MR'}
                                 }
        (is_draft, changed) = common.draft_status(merge_dict)
        self.assertTrue(is_draft)
        self.assertTrue(changed)

    @mock.patch('webhook.common.run')
    def test__grep_for_config(self, mock_run):
        # No results
        mock_run.return_value = mock.Mock(returncode=1)
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        self.assertEqual(results, [])
        mock_run.return_value.check_returncode.assert_not_called()

        # Found something
        mock_run.return_value.returncode = 0
        mock_run.return_value.stdout = '\n/linus/arch/Kconfig\n/linus/arch/x86_64/Kconfig'
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        self.assertEqual(results, ['arch/Kconfig', 'arch/x86_64/Kconfig'])
        mock_run.return_value.check_returncode.assert_not_called()

        # Called check_returncode()
        mock_run.return_value.returncode = 2
        results = common._grep_for_config('CONFIG_FUN', '/linus')
        mock_run.return_value.check_returncode.assert_called_once()

    @mock.patch('webhook.common._grep_for_config')
    def test_process_config_items(self, mock_grep):
        # Input nothing, return nothing
        path_list = []
        results = common.process_config_items('/linus', path_list)
        self.assertEqual(results, (set(), []))

        # A hit
        path_list = ['redhat/configs/common/generic/x86/x86_64/CONFIG_TURBO', 'README']
        mock_grep.return_value = ['arch/x86_64/Kconfig']
        results = common.process_config_items('/linus', path_list)
        self.assertEqual(results, (set(['arch/x86_64/Kconfig']), [defs.CONFIG_LABEL]))

    def test_load_yaml_data(self):
        with self.assertLogs('cki.webhook.common', level='ERROR') as logs:
            result = common.load_yaml_data('bad_map_file.yml')
            self.assertEqual(result, None)
            self.assertIn("No such file or directory: 'bad_map_file.yml'", logs.output[-1])

        with mock.patch('builtins.open', mock.mock_open(read_data=self.FILE_CONTENTS)):
            result = common.load_yaml_data('map_file.yml')
            self.assertEqual(result, self.MAPPINGS)

    def test_match_single_label(self):
        # No match
        results = common.match_single_label('readyForMerge', self.YAML_LABELS)
        self.assertEqual(results, None)

        # Basic match
        results = common.match_single_label('Acks::OK', self.YAML_LABELS)
        self.assertEqual(results, self.YAML_LABELS[0])

        # Regex match
        results = common.match_single_label('Subsystem:Cookies', self.YAML_LABELS)
        expected = {'name': 'Subsystem:Cookies', 'color': '#778899', 'description': 'hi Cookies'}
        self.assertEqual(results, expected)

    @mock.patch('webhook.common.load_yaml_data')
    def test_validate_labels(self, mock_load_yaml):
        # No data generates a runtime error
        mock_load_yaml.return_value = None
        raised = False
        try:
            results = common.validate_labels(['hi', 'there'], 'utils/labels.yaml')
        except RuntimeError:
            raised = True
        mock_load_yaml.assert_called_with('utils/labels.yaml')
        self.assertTrue(raised)

        # Find some labels
        mock_load_yaml.return_value = {'labels': self.YAML_LABELS}
        results = common.validate_labels(['Acks::OK', 'Subsystem:net'], 'yaml_path')
        expected = [{'name': 'Acks::OK', 'color': '#428BCA', 'description': 'all good'},
                    {'name': 'Subsystem:net', 'color': '#778899', 'description': 'hi net'}
                    ]
        mock_load_yaml.assert_called_with('yaml_path')
        self.assertEqual(results, expected)

        # Can't find it!
        raised = False
        try:
            results = common.validate_labels(['Acks::OK', 'Frogs::OK', 'Subsystem:net'], 'yaml')
        except RuntimeError as err:
            raised = True
            self.assertIn("unknown: ['Frogs::OK']", err.args[0])
        mock_load_yaml.assert_called_with('yaml')
        self.assertTrue(raised)

    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_do_not_run_hook(self, is_prod):
        proj = mock.Mock()
        proj.archived = True
        proj.path_with_namespace = "test/path"
        mreq = mock.Mock()
        mreq.iid = 666
        mreq.state = "closed"
        hook_name = "foobar"
        run_on_drafts = False
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            common.do_not_run_hook(proj, mreq, hook_name, run_on_drafts)
            self.assertIn("Not running foobar hook on test/path/666, MR is in draft state",
                          logs.output[-1])
        run_on_drafts = True
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            common.do_not_run_hook(proj, mreq, hook_name, run_on_drafts)
            self.assertIn("Not running foobar hook on test/path/666, MR is close",
                          logs.output[-1])
        mreq.state = "opened"
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            common.do_not_run_hook(proj, mreq, hook_name, run_on_drafts)
            self.assertIn("Not running foobar hook on test/path/666, project is archived",
                          logs.output[-1])

    @mock.patch('webhook.common.get_authlevel')
    def test_get_commits_count(self, authlevel):
        mreq = mock.Mock()
        proj = mock.Mock()
        mreq.iid = 19
        commits = [1, 2, 3, 4]
        mreq.commits.return_value = commits
        mreq.author = mock.MagicMock(id=1234)
        authlevel.return_value = 30  # Developer access
        count, _ = common.get_commits_count(proj, mreq)
        self.assertEqual(count, 4)
        i = 5
        while i < 2002:
            commits.append(i)
            i += 1
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            count, _ = common.get_commits_count(proj, mreq)
            self.assertEqual(count, 2001)
            self.assertIn("MR 19 has 2001 commits, too many to process -- wrong target branch?",
                          logs.output[-1])
        mreq.commits.return_value = []
        count, _ = common.get_commits_count(proj, mreq)
        self.assertEqual(count, 0)

    @mock.patch('webhook.common.mr_action_affects_commits', return_value=True)
    @mock.patch('cki_lib.misc.is_production', return_value=True)
    def test_set_bug_state(self, is_prod, code_changed):
        bug1 = '12345678'
        bug1_data = mock.Mock(id=int(bug1), status='NEW', cf_verified='Verified')
        bug2 = '11223344'
        bug2_data = mock.Mock(id=int(bug2), status='ASSIGNED', cf_verified='Verified')
        bug3 = '24687531'
        bug3_data = mock.Mock(id=int(bug3), status='POST', cf_verified='Verified')
        bug4 = '8675309'
        bug4_data = mock.Mock(id=int(bug4), status='ON_QA', cf_verified='Verified')
        bug5 = '5551234'
        bug5_data = mock.Mock(id=int(bug5), status=defs.BZ_STATE_MODIFIED, cf_verified='Verified')
        bug6 = 'INTERNAL'
        bug6_data = mock.Mock(id=bug6, status='POST', cf_verified='Verified')
        project = mock.Mock()
        project.archived = False
        merge_request = mock.Mock()
        merge_request.draft = True
        merge_request.iid = 1234
        merge_request.labels = []
        project.mergerequests.get.return_value = merge_request
        bzcon = mock.Mock()
        bug_fields = defs.BUG_FIELDS

        # MR does not have readyForQA label on it
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug1_data
            common.set_bug_state(project, merge_request.iid, [bug1], bzcon, defs.BZ_STATE_MODIFIED)
            bzcon.getbug.assert_not_called()
            self.assertIn(f'This merge request has no {defs.READY_FOR_QA_LABEL} or '
                          f'{defs.READY_FOR_MERGE_LABEL} label, not '
                          f'updating to {defs.BZ_STATE_MODIFIED}',
                          logs.output[-1])

        merge_request.labels = [defs.READY_FOR_MERGE_LABEL]

        # MR is in draft state and has readyForMerge label set, do nothing
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug1_data
            common.set_bug_state(project, merge_request.iid, [bug1], bzcon, defs.BZ_STATE_MODIFIED)
            bzcon.getbug.assert_not_called()
            self.assertIn('Not updating bugs for MR 1234, because it\'s marked as a Draft.',
                          logs.output[-1])

        merge_request.labels = [defs.READY_FOR_QA_LABEL]

        # MR is in draft state and has readyForQA label set, do nothing
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug1_data
            common.set_bug_state(project, merge_request.iid, [bug1], bzcon, defs.BZ_STATE_MODIFIED)
            bzcon.getbug.assert_not_called()
            self.assertIn('Not updating bugs for MR 1234, because it\'s marked as a Draft.',
                          logs.output[-1])

        merge_request.draft = False

        # getbug() failure.
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = None
            common.set_bug_state(project, merge_request.iid, [bug1], bzcon, defs.BZ_STATE_MODIFIED)
            bzcon.getbug.assert_called_once_with(bug1, include_fields=bug_fields)
            self.assertIn('getbug() did not return a useful result for BZ 12345678.',
                          logs.output[-1])

        # Unsupported (by webhook) new bug state (ASSIGNED) specified
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug1_data
            common.set_bug_state(project, merge_request.iid, [bug1], bzcon, defs.BZ_STATE_ASSIGNED)
            self.assertIn(f'Invalid new BZ state provided: {defs.BZ_STATE_ASSIGNED}',
                          logs.output[-1])

        # Bug will be updated from NEW to POST
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug1_data
            common.set_bug_state(project, merge_request.iid, [bug1], bzcon, defs.BZ_STATE_POST)
            self.assertIn(f'Will update bug {bug1_data.id} from {bug1_data.status} to '
                          f'{defs.BZ_STATE_POST}.',
                          logs.output[-1])

        # Bug will be updated from ASSIGNED to POST
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug2_data
            common.set_bug_state(project, merge_request.iid, [bug2], bzcon, defs.BZ_STATE_POST)
            self.assertIn(f'Will update bug {bug2_data.id} from {bug2_data.status} to '
                          f'{defs.BZ_STATE_POST}.',
                          logs.output[-1])

        # Bug will be updated from POST to MODIFIED
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug3_data
            common.set_bug_state(project, merge_request.iid, [bug3], bzcon, defs.BZ_STATE_MODIFIED)
            self.assertIn(f'Will update bug {bug3_data.id} from {bug3_data.status} to '
                          f'{defs.BZ_STATE_MODIFIED}.',
                          logs.output[-1])

        # Bug will not be updated from ON_QA to POST
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            bzcon.getbug.return_value = bug4_data
            common.set_bug_state(project, merge_request.iid, [bug4], bzcon, defs.BZ_STATE_POST)
            self.assertIn(f'Not modifying bug {bug4_data.id} from {bug4_data.status} to '
                          f'{defs.BZ_STATE_POST}.',
                          logs.output[-1])

        # Bug will not be updated from ON_QA to MODIFIED
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug4_data
            common.set_bug_state(project, merge_request.iid, [bug4], bzcon, defs.BZ_STATE_MODIFIED)
            self.assertIn(f'Not modifying bug {bug4_data.id} from {bug4_data.status} to '
                          f'{defs.BZ_STATE_MODIFIED}.',
                          logs.output[-1])

        # Bug will not be updated from MODIFIED
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug5_data
            common.set_bug_state(project, merge_request.iid, [bug5], bzcon, defs.BZ_STATE_MODIFIED)
            self.assertIn(f'Bug {bug5_data.id} already in state {bug5_data.status}, nothing '
                          'to do here.',
                          logs.output[-1])

        # There's nothing to do for commits/MRs flagged as INTERNAL
        with self.assertLogs('cki.webhook.common', level='INFO') as logs:
            bzcon.getbug.return_value = bug6_data
            common.set_bug_state(project, merge_request.iid, [bug6], bzcon, defs.BZ_STATE_MODIFIED)
            self.assertIn("This isn't a bug, it's a commit or MR flagged as INTERNAL.",
                          logs.output[-1])

    @mock.patch('webhook.common._set_bug_state')
    @mock.patch('webhook.common.try_bugzilla_conn')
    def test_set_bug_state_wrapper(self, mock_try_bugzilla_con, mock_set_bug_state):
        project = fakes.FakeGitLabProject()
        mr_attributes = {'description': 'Bugzilla: https://bugzilla.redhat.com/1234567'}
        project.add_mr(123, actual_attributes=mr_attributes)

        # original call style
        common.set_bug_state(project, 123, [55667788], 'bzcon', 'new_state')
        mock_try_bugzilla_con.assert_not_called()
        mock_set_bug_state.assert_called_with(project.mergerequests[123], [55667788], 'bzcon',
                                              'new_state')

        # call style used by common module
        common.set_bug_state(None, None, None, None, 'new_state', project.mergerequests[123])
        mock_try_bugzilla_con.assert_called_once()
        mock_set_bug_state.assert_called_with(project.mergerequests[123], ['1234567'],
                                              mock_try_bugzilla_con.return_value, 'new_state')

    @mock.patch('webhook.common.set_bug_state')
    def test_update_bugzilla_state(self, mock_set_bug_state):
        # No ready label
        labels = ['/label "Acks::OK"', '/label "Bugzilla::OK"', '/label "Signoff::NeedsReview"']
        common._update_bugzilla_state('mrObject', labels)
        mock_set_bug_state.assert_not_called()

        # Ready label but with the Bugzilla label
        labels.append('/label "readyForMerge"')
        common._update_bugzilla_state('mrObject', labels)
        mock_set_bug_state.assert_not_called()

        # Ready label with no Bugzilla label
        labels.remove('/label "Bugzilla::OK"')
        common._update_bugzilla_state('mrObject', labels)
        mock_set_bug_state.assert_called_with(None, None, None, None, defs.BZ_STATE_MODIFIED,
                                              'mrObject')

    def test_match_gl_username_to_email(self):
        gl_instance = mock.Mock()
        gl_instance.users.list = \
            lambda search, as_list: [mock.Mock(username=search.split('@')[0])]
        email = "joedev@redhat.com"
        username = "joedev"
        self.assertTrue(common.match_gl_username_to_email(gl_instance, email, username))
        username = "biff"
        self.assertFalse(common.match_gl_username_to_email(gl_instance, email, username))
        username = None
        self.assertFalse(common.match_gl_username_to_email(gl_instance, email, username))

    @mock.patch('cki_lib.misc.is_production', mock.Mock())
    def test_update_webhook_comment(self):
        gl_mergerequest = mock.Mock()
        discussion = mock.Mock(attributes={'notes': [self.NATTRS]})
        # discussion.notes.get.return_value = '1234'
        gl_mergerequest.discussions.list.return_value = [discussion]
        bot_name = "shadowman"
        identifier = "This should be unique per webhook"
        new_comment = "This is my new comment"
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            common.update_webhook_comment(gl_mergerequest, bot_name, identifier, new_comment)
            self.assertIn("Overwriting existing webhook comment", ' '.join(logs.output))
            gl_mergerequest.notes.create.assert_not_called()
        bot_name = "a_different_user"
        with self.assertLogs('cki.webhook.common', level='DEBUG') as logs:
            common.update_webhook_comment(gl_mergerequest, bot_name, identifier, new_comment)
            self.assertIn("Creating new webhook comment", ' '.join(logs.output))
            gl_mergerequest.notes.create.assert_called()

    def test_get_mr_state_from_event(self):
        """Returns the state string value, if any, or None."""
        payload = {'object_attributes': {'state': 'opened'}}
        self.assertEqual(common.get_mr_state_from_event(payload), 'opened')
        payload = {'object_attributes': {}}
        self.assertEqual(common.get_mr_state_from_event(payload), None)
        payload = {'object_attributes': {}, 'merge_request': {'state': 'closed'}}
        self.assertEqual(common.get_mr_state_from_event(payload), 'closed')
        payload = {'object_attributes': None, 'merge_request': None}
        self.assertEqual(common.get_mr_state_from_event(payload), None)


class TestActiveBranch(TestCase):

    PAYLOAD = {'object_kind': None,
               'project': {'id': 12345, 'name': 'cool_project'},
               'merge_request': {'target_branch': '10.0'},
               'object_attributes': {'target_branch': '11.3'}
               }

    @mock.patch('webhook.common.is_branch_active')
    def test_unknown_object_kind(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        # No object_kind, returns None.
        self.assertEqual(common.is_event_target_branch_active(self.PAYLOAD), None)
        mock_is_branch_active.assert_not_called()

    @mock.patch('webhook.common.is_branch_active')
    def test_mr_event(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        payload = deepcopy(self.PAYLOAD)
        payload['object_kind'] = 'merge_request'
        self.assertTrue(common.is_event_target_branch_active(payload))
        mock_is_branch_active.assert_called_with(payload['project']['id'],
                                                 payload['object_attributes']['target_branch'])

    @mock.patch('webhook.common.is_branch_active')
    def test_note_event(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        # An MR-related note event.
        payload = deepcopy(self.PAYLOAD)
        payload['object_kind'] = 'note'
        payload['object_attributes']['noteable_type'] = 'MergeRequest'
        self.assertTrue(common.is_event_target_branch_active(payload))
        mock_is_branch_active.assert_called_with(payload['project']['id'],
                                                 payload['merge_request']['target_branch'])
        # A note event for something else.
        mock_is_branch_active.reset_mock()
        payload['object_attributes']['noteable_type'] = 'Issue'
        self.assertEqual(common.is_event_target_branch_active(payload), None)
        mock_is_branch_active.assert_not_called()

    @mock.patch('webhook.common.is_branch_active')
    def test_pipeline_event(self, mock_is_branch_active):
        """Should call is_branch_active with the expected parameters."""
        payload = deepcopy(self.PAYLOAD)
        payload['object_kind'] = 'pipeline'

        # an upstream triggered event
        self.assertTrue(common.is_event_target_branch_active(payload))
        mock_is_branch_active.assert_called_with(payload['project']['name'],
                                                 payload['merge_request']['target_branch'])
        # a downstream event with variables
        mock_is_branch_active.reset_mock()
        payload.pop('merge_request')
        mr_url = 'https://gitlab.com/group/upstream_project/-/merge_requests/321'
        payload['object_attributes'] = {'variables': [{'key': 'branch', 'value': '5'},
                                                      {'key': 'mr_url', 'value': mr_url}
                                                      ]
                                        }
        self.assertTrue(common.is_event_target_branch_active(payload))
        mock_is_branch_active.assert_called_with('upstream_project', '5')

        # some unhandled event?
        mock_is_branch_active.reset_mock()
        payload['object_attributes'].pop('variables')
        self.assertEqual(common.is_event_target_branch_active(payload), None)
        mock_is_branch_active.assert_not_called()
