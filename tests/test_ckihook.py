"""Webhook interaction tests."""
from copy import deepcopy
from unittest import TestCase
from unittest import mock

from webhook import ckihook
from webhook import defs

CHANGES = {'labels': {'previous': [{'title': f'{defs.CKI_KERNEL_RT_PREFIX}::Running'},
                                   {'title': f'{defs.CKI_KERNEL_PREFIX}::Running'}
                                   ],
                      'current': [{'title': f'{defs.CKI_KERNEL_RT_PREFIX}::Canceled'}]
                      }
           }


class TestPipeStatus(TestCase):
    """Tests for the PipeStatus enum."""

    def test_title(self):
        """Title() should capitalize() all Statuses except OK."""
        self.assertEqual(ckihook.PipelineStatus.FAILED.title(), 'Failed')
        self.assertEqual(ckihook.PipelineStatus.CREATED.title(), 'Running')
        self.assertEqual(ckihook.PipelineStatus.OK.title(), 'OK')


class TestPipelineResult(TestCase):
    """Tests for the PipelineResult dataclass."""

    def test_init_without_dict(self):
        """Object properties should be set."""
        # Type that label should be set for.
        pipe_input = {'name': 'merge_request_regular',
                      'status': ckihook.PipelineStatus.RUNNING,
                      }
        result = ckihook.PipelineResult(**pipe_input)
        self.assertEqual(result.name, pipe_input['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.RUNNING)
        self.assertEqual(result.label, f'{defs.CKI_KERNEL_PREFIX}::Running')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_LEGACY)

        # Type that label should NOT be set for.
        pipe_input = {'name': 'c9s_rhel9_compat_merge_request',
                      'status': ckihook.PipelineStatus.RUNNING,
                      }
        result = ckihook.PipelineResult(**pipe_input)
        self.assertEqual(result.name, pipe_input['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.RUNNING)
        self.assertEqual(result.label, '')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_SHADOW)

        # Unknown Type.
        pipe_input = {'name': 'what_is_this',
                      'status': ckihook.PipelineStatus.RUNNING,
                      }
        result = ckihook.PipelineResult(**pipe_input)
        self.assertEqual(result.name, pipe_input['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.RUNNING)
        self.assertEqual(result.label, '')
        self.assertEqual(result.type, ckihook.PipelineType.INVALID)

    def test_init_with_dict(self):
        """Object properties should be set."""
        # Sets label
        input_dict = {'status': 'pending',
                      'sourceJob': {'name': 'rhel8_realtime_check_merge_request'},
                      'jobs': {'nodes': [{'status': 'failed', 'stage': {'name': 'test'}},
                                         {'status': 'failed', 'stage': {'name': 'build'}},
                                         {'status': 'failed', 'stage': {'name': 'merge'}}
                                         ]
                               }
                      }
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.RUNNING)
        self.assertEqual(result.label, 'CKI_RT::Running')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_RT)
        # Sets failed label
        input_dict['status'] = 'failed'
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.FAILED)
        self.assertEqual(result.label, 'CKI_RT::Failed::test')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_RT)
        # Sets no label
        input_dict['sourceJob']['name'] = 'c9s_rhel9_compat_merge_request'
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.FAILED)
        self.assertEqual(result.label, '')
        self.assertEqual(result.type, ckihook.PipelineType.KERNEL_SHADOW)


class TestComputeCKI(TestCase):
    """Validate ComputeCKI object behaviour."""

    def test_ComputeCKI_init(self):
        """Sets the expected properties."""
        user = 'test_user'
        namespace = 'group/project'
        mr_id = 123
        result = ckihook.ComputeCKI(user, namespace, mr_id)
        self.assertEqual(result.event_user, user)
        self.assertEqual(result.namespace, namespace)
        self.assertEqual(result.mr_id, mr_id)

    @mock.patch('webhook.ckihook.parse_pipeline_query_results')
    def test_ComputeCKI_query(self, mock_parse_query):
        """Runs a query and returns the output of parse_pipeline_query_results()."""
        user = 'test_user'
        namespace = 'group/project'
        mr_id = 123
        mycki = ckihook.ComputeCKI(user, namespace, mr_id)
        mycki.execute_query = mock.Mock()
        query_params = {'namespace': namespace, 'mr_id': str(mr_id)}
        result = mycki.run_query()
        mycki.execute_query.assert_called_once_with(ckihook.PIPELINE_QUERY, query_params,
                                                    check_keys=ckihook.PIPELINE_QUERY_CHECK_KEYS,
                                                    check_user=user)
        mock_parse_query.assert_called_once_with(mycki.execute_query.return_value)
        self.assertEqual(result, mock_parse_query.return_value)


class TestHelpers(TestCase):
    """Test helper functions."""

    @mock.patch('webhook.ckihook.PipelineResult')
    def test_parse_pipeline_query_results_good(self, mock_PipelineResult):
        """Passes downstream nodes to generate PipelineResult objects and returns them as a list."""
        nodes = [1, 2]
        graphql = {'project': {'mergeRequest': {'headPipeline': {'downstream': {'nodes': nodes}}}}}
        results = ckihook.parse_pipeline_query_results(graphql)
        self.assertEqual(mock_PipelineResult.call_count, 2)
        self.assertEqual(mock_PipelineResult.call_args_list[0].kwargs['api_dict'], 1)
        self.assertEqual(mock_PipelineResult.call_args_list[1].kwargs['api_dict'], 2)
        self.assertEqual(len(results), 2)

    @mock.patch('webhook.ckihook.PipelineResult')
    def test_parse_pipeline_query_results_bad(self, mock_PipelineResult):
        """Returns an empty list when query results do not have the necessary details."""
        # Nada.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            self.assertEqual(ckihook.parse_pipeline_query_results([]), [])
            self.assertIn('Nothing to parse.', logs.output[-1])
            mock_PipelineResult.assert_not_called()

        # No MR.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            self.assertEqual(ckihook.parse_pipeline_query_results({'project': {}}), [])
            self.assertIn('Merge request does not exist', logs.output[-1])
            mock_PipelineResult.assert_not_called()

        # No head pipeline.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            graphql = {'project': {'mergeRequest': {'headPipeline': {}}}}
            self.assertEqual(ckihook.parse_pipeline_query_results(graphql), [])
            self.assertIn('MR does not have headPipeline set.', logs.output[-1])
            mock_PipelineResult.assert_not_called()

        # No downstream notes.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            graphql = {'project': {'mergeRequest': {'headPipeline': {'downstream': {'nodes': []}}}}}
            self.assertEqual(ckihook.parse_pipeline_query_results(graphql), [])
            self.assertIn('headPipeline does not have any downstream nodes set', logs.output[-1])
            mock_PipelineResult.assert_not_called()

    def test_get_failed_stage(self):
        """Returns a stage name if status is FAILED and a failed job is found, or None."""
        # Status is not FAILED.
        self.assertEqual(ckihook.get_failed_stage(ckihook.PipelineStatus.RUNNING, 1, 1), None)
        # No failed jobs
        stages = ['prepare', 'build', 'test']
        jobs = [{'stage': 'test', 'status': 'running'},
                {'stage': 'test', 'status': 'pending'},
                {'stage': 'build', 'status': 'success'},
                {'stage': 'prepare', 'status': 'success'}
                ]
        self.assertEqual(ckihook.get_failed_stage(ckihook.PipelineStatus.FAILED, stages, jobs),
                         None)
        # Test job failed.
        jobs.append({'stage': 'test', 'status': 'failed'})
        self.assertEqual(ckihook.get_failed_stage(ckihook.PipelineStatus.FAILED, stages, jobs),
                         'test')

    @mock.patch('webhook.ckihook.get_instance')
    @mock.patch('webhook.common.add_label_to_merge_request')
    def test_add_labels(self, mock_add_label, mock_get_instance):
        """Gets a gl_instance/project and calls add_label_to_merge_request."""
        namespace = 'group/project'
        mr_id = 123
        labels = ['CKI::Running', 'CKI_RT::OK']
        ckihook.add_labels(namespace, mr_id, labels)
        mock_get_instance.projects.get.called_once_with(namespace)
        mock_add_label.called_once_with(mock_get_instance,
                                        mock_get_instance.projects.get.return_value, mr_id, labels,
                                        remove_scoped=True)

    def test_cki_label_changed(self):
        """Returns True if any CKI labels are in the changes list."""
        changes = deepcopy(CHANGES)
        self.assertTrue(ckihook.cki_label_changed(changes))
        self.assertFalse(ckihook.cki_label_changed({}))

    @mock.patch('webhook.ckihook.ComputeCKI')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_compute_new_labels(self, mock_missing_check, mock_compute):
        """Returns a list of labels if any changed."""
        user = 'test_user'
        namespace = 'group/project'
        mr_id = 123

        # CKI changed so we compute new labels.
        mock_compute().run_query.return_value = [mock.Mock(label='CKI::OK'),
                                                 mock.Mock(label='CKI_RT::Failed::merge')]
        mock_missing_check.return_value = []
        results = ckihook.compute_new_labels(user, namespace, mr_id)
        mock_compute.assert_called_with(user, namespace, mr_id)
        mock_missing_check.assert_called_once_with(['CKI::OK', 'CKI_RT::Failed::merge'])
        self.assertEqual(results, ['CKI::OK', 'CKI_RT::Failed::merge'])

    def test_check_for_missing_labels(self):
        """Returns a list with the ::Missing CKI labels."""
        # Input list has neither, return both.
        missing_labels = [f'{defs.CKI_KERNEL_PREFIX}::Missing',
                          f'{defs.CKI_KERNEL_RT_PREFIX}::Missing']
        self.assertEqual(ckihook.check_for_missing_labels([]), missing_labels)

        # Input list has one, return the other.
        missing_labels = [f'{defs.CKI_KERNEL_RT_PREFIX}::Missing']
        self.assertEqual(ckihook.check_for_missing_labels([f'{defs.CKI_KERNEL_PREFIX}::Running']),
                         missing_labels)

        # Has both, return an empty list.
        self.assertEqual(ckihook.check_for_missing_labels([f'{defs.CKI_KERNEL_PREFIX}::Running',
                                                           f'{defs.CKI_KERNEL_RT_PREFIX}::OK']),
                         [])


class TestMRHandler(TestCase):
    """Tests for the MR event handler."""

    MR_PAYLOAD = {'user': {'username': 'test_user'},
                  'project': {'path_with_namespace': 'group/project'},
                  'object_attributes': {'iid': 123},
                  'labels': [{'title': 'hey'}],
                  'changes': {}
                  }

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_label_changed(self, mock_check_for_missing, mock_compute_new_labels,
                                            mock_cki_label_changed, mock_add_labels):
        """Calls add_labels with the results of cki_label_changed."""
        payload = deepcopy(self.MR_PAYLOAD)
        payload['changes'] = CHANGES
        msg = mock.Mock(payload=payload)
        ckihook.process_mr_event(msg)
        mock_cki_label_changed.assert_called_once_with(payload['changes'])
        mock_compute_new_labels.assert_called_once_with('test_user', 'group/project', 123)
        mock_add_labels.assert_called_once_with('group/project', 123,
                                                mock_compute_new_labels.return_value)
        mock_check_for_missing.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_check_for_missing(self, mock_check_for_missing,
                                                mock_compute_new_labels, mock_cki_label_changed,
                                                mock_add_labels):
        """No label changes, calls check_for_missing and add_labels."""
        payload = deepcopy(self.MR_PAYLOAD)
        msg = mock.Mock(payload=payload)
        mock_cki_label_changed.return_value = []
        ckihook.process_mr_event(msg)
        mock_cki_label_changed.assert_called_once_with({})
        mock_compute_new_labels.assert_not_called()
        mock_check_for_missing.assert_called_once_with(['hey'])
        mock_add_labels.assert_called_once_with('group/project', 123,
                                                mock_check_for_missing.return_value)

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_no_action(self, mock_check_for_missing, mock_compute_new_labels,
                                        mock_cki_label_changed, mock_add_labels):
        """No label changes and no missing CKI labels, nothing to do."""
        payload = deepcopy(self.MR_PAYLOAD)
        msg = mock.Mock(payload=payload)
        mock_cki_label_changed.return_value = []
        mock_check_for_missing.return_value = []
        ckihook.process_mr_event(msg)
        mock_cki_label_changed.assert_called_once_with({})
        mock_compute_new_labels.assert_not_called()
        mock_check_for_missing.assert_called_once_with(['hey'])
        mock_add_labels.assert_not_called()


class TestPipelineHandler(TestCase):
    """Tests for the Pipeline event handler."""

    PIPELINE_PAYLOAD = {'user': {'username': 'test_user'},
                        'project': {'path_with_namespace': 'group/project'},
                        'object_attributes': {'id': 12345,
                                              'status': 'running',
                                              'stages': ['prepare', 'merge', 'build', 'test'],
                                              'variables': []
                                              },
                        'builds': []
                        }

    VARIABLES = [{'key': 'mr_url', 'value': 'https://gitlab.com/group/project/-/merge_requests/10'},
                 {'key': 'trigger_job_name', 'value': 'merge_request_regular'}]

    @mock.patch('webhook.ckihook.PipelineResult', wraps=ckihook.PipelineResult)
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_no_vars(self, mock_add_labels, mock_PipelineResult):
        """Doesn't do anything."""
        # No mr_url, nothing to do.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_add_labels.assert_not_called()
            mock_PipelineResult.assert_not_called()

        # Has mr_url, but retrigger is 'true'
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            payload['object_attributes']['variables'].append({'key': 'retrigger', 'value': 'true'})
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_add_labels.assert_not_called()
            mock_PipelineResult.assert_not_called()

    @mock.patch('webhook.ckihook.PipelineResult', wraps=ckihook.PipelineResult)
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_sets_label(self, mock_add_labels, mock_PipelineResult):
        """Calls PipelineResult and sets a label."""
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            mock_PipelineResult.assert_called_once_with(name='merge_request_regular',
                                                        status=ckihook.PipelineStatus.RUNNING,
                                                        failed_stage=None
                                                        )
            self.assertIn('Setting label CKI::Running', logs.output[-1])
            mock_add_labels.assert_called_once_with('group/project', 10, ['CKI::Running'])

    @mock.patch('webhook.ckihook.PipelineResult', wraps=ckihook.PipelineResult)
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_failed(self, mock_add_labels, mock_PipelineResult):
        """Calls PipelineResult and sets a label."""
        # If the event status is Failed get the failed stage and set a failed label.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            payload['object_attributes']['status'] = 'failed'
            payload['builds'].append({'stage': 'build', 'status': 'failed'})
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            mock_PipelineResult.assert_called_once_with(name='merge_request_regular',
                                                        status=ckihook.PipelineStatus.FAILED,
                                                        failed_stage='build'
                                                        )
            self.assertIn('Setting label CKI::Failed::build', logs.output[-1])
            mock_add_labels.assert_called_once_with('group/project', 10, ['CKI::Failed::build'])

    @mock.patch('webhook.ckihook.PipelineResult', wraps=ckihook.PipelineResult)
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_failed_is_running(self, mock_add_labels, mock_PipelineResult):
        """Calls PipelineResult and sets a label."""
        # If the event status is Failed but there are no failed builds then set status to RUNNING.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            payload['object_attributes']['variables'] = deepcopy(self.VARIABLES)
            payload['object_attributes']['status'] = 'failed'
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            mock_PipelineResult.assert_called_once_with(name='merge_request_regular',
                                                        status=ckihook.PipelineStatus.RUNNING,
                                                        failed_stage=None
                                                        )
            self.assertIn('Setting label CKI::Running', logs.output[-1])
            mock_add_labels.assert_called_once_with('group/project', 10, ['CKI::Running'])

    @mock.patch('webhook.ckihook.PipelineResult', wraps=ckihook.PipelineResult)
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_no_label(self, mock_add_labels, mock_PipelineResult):
        """Calls PipelineResult and does not set a label.."""
        # An event for a pipeline that we don't associate with either CKI or CKI_RT labels.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(self.PIPELINE_PAYLOAD)
            variables = deepcopy(self.VARIABLES)
            variables[1]['value'] = 'c9s_rhel9_compat_merge_request'
            payload['object_attributes']['variables'] = variables
            msg = mock.Mock(payload=payload)
            ckihook.process_pipeline_event(msg)
            mock_PipelineResult.assert_called_once_with(name='c9s_rhel9_compat_merge_request',
                                                        status=ckihook.PipelineStatus.RUNNING,
                                                        failed_stage=None
                                                        )
            self.assertIn('No label to set for this pipeline', logs.output[-1])
            mock_add_labels.assert_not_called()


class TestNoteHandler(TestCase):
    """Tests for the Note event handler."""

    NOTE_PAYLOAD = {'user': {'username': 'test_user'},
                    'object_attributes': {'note': ''},
                    'project': {'path_with_namespace': 'group/project'},
                    'merge_request': {'iid': 123}
                    }

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.compute_new_labels')
    def test_process_note_event(self, mock_compute_new_labels, mock_add_labels):
        """Runs compute_new_labels and optionally add_labels if there is a proper request."""
        payload = deepcopy(self.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'request-cki-evaluation'
        mock_msg = mock.Mock(payload=payload)
        ckihook.process_note_event(mock_msg)
        mock_compute_new_labels.assert_called_once_with(payload['user']['username'],
                                                        payload['project']['path_with_namespace'],
                                                        payload['merge_request']['iid'])
        mock_add_labels.assert_called_once_with(payload['project']['path_with_namespace'],
                                                payload['merge_request']['iid'],
                                                mock_compute_new_labels.return_value)

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.compute_new_labels')
    def test_process_note_event_bad_note(self, mock_compute_new_labels, mock_add_labels):
        """Does nothing when force_webhook_evaluation returns False."""
        payload = deepcopy(self.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'a random comment'
        mock_msg = mock.Mock(payload=payload)
        ckihook.process_note_event(mock_msg)
        mock_compute_new_labels.assert_not_called()
        mock_add_labels.assert_not_called()
