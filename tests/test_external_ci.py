"""Tests for external CI module."""
import unittest
from unittest import mock

from tests import fakes
import webhook.common as common
import webhook.external_ci as external_ci


def init_fake_gitlab():
    """Initialize and return a fake gitlab.Gitlab instance."""
    glab = fakes.FakeGitLab()
    glab.add_group('group')
    glab.add_project('path/to/project')
    glab.add_project('outside-group')
    add_members_to = [glab.groups.get('group'),
                      glab.projects.get('path/to/project')]
    for g_or_p in add_members_to:
        g_or_p.members_all.add_member('testmember')
        g_or_p.members_all.add_member('anothermember')

    glab.user = fakes.FakeGitLabMember('good-bot')

    return glab


@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
class TestProcessMessage(unittest.TestCase):
    """Tests for common.process_message()."""

    HEADERS = {'message-type': 'gitlab'}

    @mock.patch('webhook.common.Message.gl_instance', mock.Mock())
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_unrecognized_data(self):
        """Verify unknown message types are ignored."""
        message = {}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')
        self.assertFalse(
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
        )

        message = {'object_kind': 'unknown'}
        self.assertFalse(
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
        )

    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_pipeline_call(self, mock_gl):
        """Verify correct handler is called for pipeline hook."""
        message = {'object_kind': 'pipeline',
                   'object_attributes': {
                       'id': 1,
                       'variables': [{'key': 'origin_path', 'value': 'none'}]
                   },
                   'commit': {'url': 'none'},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')

        with self.assertLogs('cki.webhook.external_ci', level='INFO') as logs:
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
            self.assertIn('No matching MR project for none', logs.output[-1])

    @mock.patch('webhook.common.Message.gl_instance', mock.MagicMock())
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_mr_hook_no_project(self):
        """Verify MR with unknown project is handled correctly."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'unknown'},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')

        with self.assertLogs('cki.webhook.external_ci', level='ERROR') as logs:
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
            self.assertIn('Missing config for', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('webhook.common.Message.gl_instance')
    def test_mr_hook_group_member(self, mock_gl, mock_config):
        """Check pipelines aren't triggered if the author is a group member."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'testmember'},
                   'state': 'opened'}
        mock_config.return_value = {'.members_of': 'group'}
        mock_gl.return_value.__enter__.return_value = init_fake_gitlab()
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
            self.assertIn('Found internal contributor', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('webhook.common.Message.gl_instance')
    def test_mr_hook_project_member(self, mock_gl, mock_config):
        """Test fallback with testing for a project member instead of group."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'anothermember'},
                   'state': 'opened'}
        mock_config.return_value = {'.members_of': 'path/to/project'}
        mock_gl.return_value.__enter__.return_value = init_fake_gitlab()
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
            self.assertIn('Found internal contributor', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('webhook.common.Message.gl_instance')
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_mr_hook_no_code_changes(self, mock_gl, mock_config):
        """Check we abort if the MR code was not modified."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'testmember'},
                   'state': 'opened',
                   'object_attributes': {'action': 'not-a-code-change'}}
        mock_config.return_value = {'.members_of': 'outside-group'}
        mock_gl.return_value.__enter__.return_value = init_fake_gitlab()
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
            self.assertIn('Not a code change, ignoring', logs.output[-1])


class TestMain(unittest.TestCase):
    """Sanity tests for external_ci.main()."""

    @mock.patch.dict('os.environ', {'CONFIG_PATH': 'filepath'})
    @mock.patch.dict('webhook.external_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    @mock.patch('webhook.external_ci.load_config', mock.Mock())
    @mock.patch('webhook.common.get_instance')
    def test_manual(self, mock_gitlab):
        """Verify manual request is correctly handled.

        This includes checking get_manual_hook_data() function.
        """
        args = ['--merge-request',
                'https://gitlab.example/path/to/project/-/merge_requests/11']
        fake_gitlab = init_fake_gitlab()
        fake_project = fake_gitlab.projects.get('path/to/project')
        fake_project.attributes = {
            'name': 'path/to/project',
            'http_url_to_repo': 'https://gitlab.example/path/to/project'
        }
        fake_project.add_mr(
            11,
            {'sha': 'last_commit',
             'author': {'username': 'testmember'},
             'target_branch': 'devel'},
            {'web_url': 'https://url'}
        )
        mock_gitlab.return_value = fake_gitlab

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            external_ci.main(args)
            self.assertIn('Found internal contributor', logs.output[-1])


class TestStageSuffix(unittest.TestCase):
    """Tests for external_ci.get_stage_suffix()."""

    def test_suffix(self):
        """Verify the function returns expected strings."""
        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success')]
        self.assertEqual('', external_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success'),
                fakes.FakeGitLabJob(125, 'stage2', 'skipped')]
        self.assertEqual('', external_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'success'),
                fakes.FakeGitLabJob(125, 'stage2', 'failed')]
        self.assertEqual('::stage2', external_ci.get_stage_suffix(jobs))

        jobs = [fakes.FakeGitLabJob(123, 'stage', 'failed'),
                fakes.FakeGitLabJob(125, 'stage2', 'success')]
        self.assertEqual('::stage', external_ci.get_stage_suffix(jobs))


@mock.patch('webhook.common.is_event_target_branch_active', mock.Mock(return_value=True))
class TestUpdatePipelineData(unittest.TestCase):
    """Tests for external_ci.update_pipeline_data()."""

    HEADERS = {'message-type': 'gitlab'}

    @mock.patch.dict('webhook.external_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    @mock.patch('webhook.common.Message.gl_instance')
    def test_no_project(self, mock_gitlab):
        """Verify correct behavior when no matching MR project is found."""
        message = {'object_kind': 'pipeline',
                   'object_attributes': {'id': 10, 'variables': []},
                   'user': {'username': 'testmember'},
                   'commit': {'url': 'link'}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')

        with self.assertLogs('cki.webhook.external_ci', level='INFO') as logs:
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
            self.assertIn('No matching MR project', logs.output[-1])

    @mock.patch.dict('webhook.external_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    @mock.patch('webhook.common.Message.gl_instance', mock.MagicMock())
    def test_retriggered(self):
        """Verify correct behavior when the pipeline is a retrigger."""
        message = {'object_kind': 'pipeline',
                   'object_attributes': {'id': 10, 'status': 'failed', 'variables': [
                       {'key': 'retrigger', 'value': 'true'},
                       {'key': 'origin_path', 'value': 'path/to/project'}
                   ]},
                   'user': {'username': 'testmember'},
                   'commit': {'url': 'link'},
                   'project': {'web_url': 'project url'}}
        args = common.get_arg_parser('EXTERNAL_CI').parse_args('')

        with self.assertLogs('cki.webhook.external_ci', level='INFO') as logs:
            common.process_message(args, external_ci.WEBHOOKS, 'key', message, self.HEADERS)
            self.assertIn('Handling a retriggered pipeline', logs.output[-2])


class TestHandleMR(unittest.TestCase):
    """Tests for external_ci.handle_mr()."""

    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.external_ci.already_commented')
    def test_no_config(self, mock_commented, mock_labeler):
        """Verify the function bails out if there is no pipeline project/branch config."""
        mr_data = {
            'object_attributes': {
                'iid': 11,
                'target_branch': 'target',
                'url': 'https://dummy-link-to.mr',
                'last_commit': {
                    'id': '0123abcd'
                },
                'target': {
                    'git_http_url': 'giturl'
                }
            },
            'project': {
                'path_with_namespace': 'path/to/project'
            }
        }
        project_config = {
            '.mr_project': 'path/to/project',
            '.members_of': 'path/to/project'
        }
        glab = init_fake_gitlab()
        glab.projects['path/to/project'].add_mr(11)
        mock_commented.return_value = True

        with self.assertLogs('cki.webhook.external_ci', level='INFO') as logs:
            external_ci.handle_mr(glab, project_config, mr_data)
            self.assertIn('No pipeline project or branch configured', logs.output[-1])

    @mock.patch('webhook.external_ci.cki_pipeline.trigger_multiple')
    @mock.patch('webhook.common.add_label_to_merge_request')
    @mock.patch('webhook.external_ci.already_commented')
    def test_handle_mr(self, mock_commented, mock_label, mock_trigger):
        """Verify the function doesn't do anything weird."""
        mr_data = {
            'object_attributes': {
                'iid': 11,
                'target_branch': 'target',
                'url': 'https://dummy-link-to.mr',
                'last_commit': {
                    'id': '0123abcd'
                },
                'target': {
                    'git_http_url': 'giturl'
                }
            },
            'project': {
                'path_with_namespace': 'path/to/project'
            }
        }
        project_config = {
            '.mr_project': 'path/to/project',
            '.members_of': 'path/to/project',
            '.cki_external_pipeline_branch': 'branch',
            '.cki_external_pipeline_project': 'project'
        }
        glab = init_fake_gitlab()
        glab.projects['path/to/project'].add_mr(11)
        mock_commented.return_value = True

        mock_trigger.return_value = [fakes.FakeGitLabPipeline(123, {'web_url': 'https://url'})]

        with self.assertLogs('cki.webhook.external_ci', level='INFO') as logs:
            external_ci.handle_mr(glab, project_config, mr_data)
            self.assertIn('STATUS COMMENT', logs.output[-1])
